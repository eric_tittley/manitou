/* Compile-time configuration */
/* The following is needed to trip VS Code's Intellisense */
#define HAVE_CONFIG
#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include <stdlib.h>

#ifdef DEBUG
#  include <stdio.h>

#  include <cmath>
#endif

#include <string>

/* For getopt */
#include <unistd.h>

/* For mkdir() */
#include <sys/stat.h>

#include "Manidoowag.h"
#include "Manitou.h"
#include "Parameters.h"
#include "RT_Precision.h"

extern "C" {
/* RT includes */
#include "RT.h"
#include "RT_Data.h"
#include "RT_Luminosity.h"
#include "RT_RatesT.h"
#include "RT_TimeScale.h"
}

/* No functions declared, then don't need to be extern "C" */
#include "RT_ConstantsT.h"
#include "RT_FileHeaderT.h"
#include "RT_SourceT.h"

/* rtsph includes */
#include "BVH.h"
#include "Rays.h"
#include "Spheres.h"

extern "C" {
/* Gadget2 includes */
#include "allvars.h"
#include "proto.h"
/* Declares:
 *  header Header info
 *  P      Particle data
 */
}

#if defined(CUDA_RTSPH) || defined(CUDA_RT)
#  include "Manitou_CUDA.h"
#  include "cudaMemory.h"
#  include "cudasupport.h"
#endif
#ifdef CUDA_RT
#  include "RT_CUDA.h"
// NCCL Header file
#  include "nccl.h"
// OMP Header file
#  include "omp.h"
#endif
#ifdef CUDA_RTSPH
#  include "LinearBVH.cuh"
#  include "Vec3.h"
#endif

enum CodeTimes {
  TreeBuild,
  CreatingRays,
  ColumnDensities,
  IonizationRates,
  Recombinations,
  ExtraCooling,
  UpdateHeatingRates,
  TimeStep,
  Updating,
  IO,
  EquilibriumValues,
  MemCopy,
  nCodeTimes
};
const char *to_string(CodeTimes ct) {
  switch (ct) {
    case TreeBuild:
      return "Tree build";
    case CreatingRays:
      return "Creating rays";
    case ColumnDensities:
      return "Column densities";
    case IonizationRates:
      return "Ionization rates";
    case Recombinations:
      return "Recombinations";
    case ExtraCooling:
      return "Extra cooling";
    case UpdateHeatingRates:
      return "Update heating rates";
    case TimeStep:
      return "Time step";
    case Updating:
      return "Updating";
    case IO:
      return "IO";
    case EquilibriumValues:
      return "Equilibrium values";
    case MemCopy:
      return "Device memory IO";
    default:
      return "Unknown";
  }
}

/* External functions for checking proper NCCL and GPU behaviour */
#define gpuErrchk(ans) \
  { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort = true) {
  if (code != cudaSuccess) {
    fprintf(stderr, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
    if (abort) exit(code);
  }
}

#define NCCLCHECK(cmd)                                                                      \
  do {                                                                                      \
    ncclResult_t r = cmd;                                                                   \
    if (r != ncclSuccess) {                                                                 \
      printf("Failed, NCCL error %s:%d '%s'\n", __FILE__, __LINE__, ncclGetErrorString(r)); \
      exit(EXIT_FAILURE);                                                                   \
    }                                                                                       \
  } while (0)

/* Start of programme */
int main(int argc, char **argv) {
  int ierr;
  int iteration_count = 0;

  /* check for and open timestep files */
  FILE *TimestepFile = fopen("timesteps.out", "w");
  if (TimestepFile == NULL) {
    printf("ERROR: %s: %i: Unable to open timesteps.out\n", __FILE__, __LINE__);
    exit(EXIT_FAILURE);
  }

  /* check for Restart File */
  char const *RestartOptionString = "r:";
  char *RestartFile = NULL;
  int optind;
  while ((optind = getopt(argc, argv, RestartOptionString)) != EOF) {
    switch (optind) {
      case 'r':
        RestartFile = optarg;
        break;
      default:
        exit(EXIT_FAILURE);
    }
  }

  /* check if the data dump folder exists */
  mkdir("save", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

  /* Initialize run parameters. */
  char const *const ParametersFile = "Manitou.params";
  ParametersT Parameters;
  ierr = readParameters(ParametersFile, &Parameters);
  if (ierr != EXIT_SUCCESS) {
    std::cout << "ERROR: " << __FILE__ << ": " << __LINE__;
    std::cout << ": Unable to read Parameters file.";
    std::cout << std::endl;
    exit(EXIT_FAILURE);
  }

/* Test 1 condition check */
#ifndef NO_COOLING
  if (Parameters.ProblemType == 1) {
    std::cout << "ERROR: If you want to run the test 1, you want to set NO_COOLING";
    std::cout << std::endl;
    exit(EXIT_FAILURE);
  }
#endif

  /* Read in the Constants file */
  char const *const ConstantsFile = "Constants.params";
  RT_ConstantsT Constants;
  ierr = RT_ReadConstantsFile(ConstantsFile, &Constants);
  if (ierr != EXIT_SUCCESS) {
    std::cout << "ERROR: " << __FILE__ << ": " << __LINE__;
    std::cout << ": Unable to read Constants file.";
    std::cout << std::endl;
    exit(EXIT_FAILURE);
  }

  /* Read in the output times, unless an every-nth-iteration has been specified */
  rt_float *OutputTimes = NULL;
  size_t nOutputTimes = 0;
  rt_float outputTimesUnit; /* To convert outputTimes[i] to s */
  if (Parameters.OutputIterationCount == 0) {
    nOutputTimes = readOutputTimes(Parameters.OutputTimesFile, outputTimesUnit, OutputTimes);
    if (nOutputTimes == 0) {
      std::cout << "ERROR: " << __FILE__ << ": " << __LINE__ << std::endl;
      std::cout << " Unable to read output times file: ";
      std::cout << Parameters.OutputTimesFile << std::endl;
      std::cout << "  Check file exists and is correctly formatted." << std::endl;
      exit(EXIT_FAILURE);
    }
  }

  /*Timing Variables*/
  double TimerArray[nCodeTimes];
  double const StartTime = getTimeInSeconds();
  double CheckPointTime;
  double TotalTime = 0.0;
#pragma omp parallel for
  for (size_t i = 0; i < nCodeTimes; ++i) {
    TimerArray[i] = 0.0;
  }

  char Dir[] = "./save";
  const size_t LABEL_STR_SIZE = 32;
  char RTDataFile[LABEL_STR_SIZE];
  rt_float dt_RT;
  /* RT_TimeScale dt_RT_sugg; */
  size_t iParticle;

  /* Load Data */
  ThisTask = 0;
  All.ICFormat = 1;
  RestartFlag = 1;
  /* read_file expects MPI, so we will need to fake an MPI environment */
  NTask = 1;                 /* Gadget2 global */
  All.PartAllocFactor = 1.0; /* The factor for allocating memory for particle data. */
  All.BufferSize = 1024.0;   /* The size (in MByte per processor) of a communication buffer. */
  if (!(CommBuffer = malloc(All.BufferSize * 1024 * 1024))) {
    printf("failed to allocate memory for `CommBuffer' (%i MB).\n", All.BufferSize);
    exit(EXIT_FAILURE);
  }
  int readTask = 0;
  int lastTask = 0;
  read_file(Parameters.GadgetFile, readTask, lastTask);
  free(CommBuffer);
  /* Now set:
   *  header.npart[6]
   *        .mass[6]
   *        .time
   *        .redshift
   *        .flag_sfr
   *        .flag_feedback
   *        .npartTotal[6]
   *        .flag_cooling
   *        .num_files
   *        .BoxSize
   *        .Omega0
   *        .OmegaLambda
   *        .HubbleParam
   *        .flag_stellarage
   *        .flag_metals
   *        .npartTotalHighWord[6]
   *        .flag_entropy_instead_u
   *  P.Pos[3]
   *   .Mass
   *   .Vel[3]
   *   .GravAccel[3]
   *   .Potential
   *   .
   *
   //XXX What is type?
   *   .Type
   *   .Vel[3]
   *   .ID
   *   .Mass
   *   .Entropy
   *   .Density** .rho
   *   .Hsml** .u
   * ** = used
   */

  if (Parameters.ProblemType != 0) {
    /* Test problem */
    if (header.redshift > 0.0) {
      printf("Setting redshift to 0\n");
      header.redshift = 0.0;
    }
  }

#ifdef CUDA_RT
  int devCount = 0;
  cudaGetDeviceCount(&devCount);
  int nDev = devCount;
  if (nDev > Parameters.nSources) {
    nDev = Parameters.nSources;
  }
  printf("Using %i CUDA devices\n",nDev);
#else
  const int nDev = 1;
#endif
  // Device Numbering Loop
  int *devs = (int *)malloc(sizeof(int) * nDev);
  for (int i = 0; i < nDev; ++i) {
    devs[i] = i;
  }
  printf("Devices numbered successfully\nUsing:\n");
  for (int i = 0; i < nDev; ++i) {
    printGpuInfo((unsigned int)devs[i]);
  }

  rt_float const ExpansionFactor = 1. / (header.redshift + 1.);

  size_t nParticles = (size_t)header.npart[0]; /* Gas only */

#if !defined(CUDA_RTSPH) || defined(SAH_BVH)
  /* Loading data into Martin's format */
  Spheres spheres;
  for (iParticle = 0; iParticle < nParticles; ++iParticle) {
    spheres.cx.push_back(P[iParticle].Pos[0]);
    spheres.cy.push_back(P[iParticle].Pos[1]);
    spheres.cz.push_back(P[iParticle].Pos[2]);
    spheres.h.push_back(SphP[iParticle].Hsml);
    spheres.invh2.push_back(1.0 / (SphP[iParticle].Hsml * SphP[iParticle].Hsml));
  }
  // Build Tree
  BVH tree;
  CheckPointTime = getTimeInSeconds();
  mkSAHBVH(spheres, tree);
#else

  /* Initialize NCCL communicator and individule device streams */
  ncclComm_t comms[nDev];
  // malloc for all
  cudaStream_t *s = (cudaStream_t *)malloc(sizeof(cudaStream_t) * nDev);

  if (nDev>1) {
    // individual streams
    printf("Creating Streams...");
    for (int i = 0; i < nDev; i++) {
      cudaSetDevice(devs[i]);
      cudaStreamCreate(s + i);
      gpuErrchk(cudaPeekAtLastError());
    }
    printf("done.\n");

    // initialize NCCL
    printf("Initializing NCCL: ncclCommInitAll\n");
    NCCLCHECK(ncclCommInitAll(comms, nDev, devs));

    // synchronizing on CUDA streams to wait for completion of NCCL operation
    for (int i = 0; i < nDev; ++i) {
      cudaSetDevice(devs[i]);
      cudaStreamSynchronize(s[i]);
    }
  }

  /* Generating spheres from particles for LinearBVH */
  printf("Generating spheres for trees...");
  std::vector<LinearBVH::Sphere> spheres(nParticles);
  for (iParticle = 0; iParticle < nParticles; ++iParticle) {
    spheres[iParticle].pos = vecFromArray(P[iParticle].Pos);
    spheres[iParticle].radius = SphP[iParticle].Hsml;
  }
  printf("done.\n");

  CheckPointTime = getTimeInSeconds();
  /* Generating Tree from spheres */
  printf("Generating tree list from spheres...");
  // the tree_list is made as a LinearBVH * pointer
  std::vector<LinearBVH *> tree_list;

  for (int i = 0; i < nDev; i++) {
    if (nDev>1) {
      cudaSetDevice(devs[i]);
      cudaDeviceSynchronize();
    }
    /* the next line creates the tree from the spheres
     * using the "new" constructor is important as it means the compiler will not
     * destroy it, so it can still exist outside the scope of this for loop
     */
    // extra print statements are for debugging
    LinearBVH *tree = new LinearBVH(spheres.size());
    printf("*tree made as new LinearBVH on device %d...", devs[i]);
    LinearBVH::construct_from_host_spheres(*tree, spheres.data(), spheres.size());
    printf("host constructed from spheres on device %d...", devs[i]);
    tree_list.push_back(tree);
  }
  printf("done.\n");

#endif

  TimerArray[TreeBuild] += getTimeInSeconds() - CheckPointTime;

  // mkOBVH(spheres, tree); /* rtsphxt */
  /* spheres.erase(); */ /* Not yet defined */
                         // from now on tree.spheres should be used

  /* Create the reverse map, to index into the RTSPH order from the input
   * (Gadget) order.
   */
#if !defined(CUDA_RTSPH) || defined(SAH_BVH)
  const uint32_t *tree_map = tree.map.data();
#else
  std::unique_ptr<uint32_t[]> tree_map_storage(new uint32_t[nParticles]);
  ERT_CUDA_CHECK(cudaMemcpy(
      tree_map_storage.get(), tree_list[0]->get_map(), sizeof(uint32_t) * nParticles, cudaMemcpyDeviceToHost));
  const uint32_t *tree_map = tree_map_storage.get();
#endif

  size_t *MapGadgetToRTSPH = new size_t[nParticles];
  for (iParticle = 0; iParticle < nParticles; ++iParticle) {
    MapGadgetToRTSPH[tree_map[iParticle]] = iParticle;
  }

  /* Set the problem-specific sources */
  RT_SourceT *Source = (RT_SourceT *)malloc(Parameters.nSources * sizeof(RT_SourceT));
  Manitou_SetSources(Parameters, header.BoxSize, Constants, Source);

  /* If Problem is test 1 or 2, and there are multiple sources, then this is a test of
   * multiGPU funcionality. */
  if( (Parameters.ProblemType==1 || Parameters.ProblemType==2) && Parameters.nSources>1 ) {
    MultipleSourcesFromSingle(Parameters.nSources, Source);
  }

  /* Initialize the source functions */
  for (size_t i = 0; i < Parameters.nSources; ++i) {
    RT_SetSourceFunction(Source + i); /* Pointer Arithmetic */
  }

  /* Report the number of ionizing photons per second */
  printf("Multiply the following flux by nSources=%lu\n", Parameters.nSources);
  RT_InitialiseSpectrum(0, Constants, &(Source[0]));
  printf("RT Spectrum Initialized.\n");

  /* Allocate memory for SPH-RT fields */
  RT_RatesBlockT Rates;
  ierr = RT_RatesBlock_Allocate(nParticles, &Rates);
  Rates.NumCells = nParticles;
  printf("RT RatesBlock Allocated.\n");

  /* Initialize constants */
  /* Number densities for H & He */
  double UnitMass_in_g = 1.989e+43 / (double)header.HubbleParam;                                              /* g */
  rt_float UnitLength_in_cm = 3.08568e+21 / (rt_float)header.HubbleParam / (1.0 + (rt_float)header.redshift); /* cm */
  double UnitMass = UnitMass_in_g / 1000.0;                                                                   /* kg */
  rt_float UnitLength = UnitLength_in_cm / 100.0;                                                             /* m */
  /* GadgetDensityUnit is 10^(43-3 - (3*18)) = 10^-14 so fits in single precision
   * but needs some assistance.
   *  Change to 10^(43-3 - 18 - 18 - 18) */
  rt_float GadgetDensityUnit =
      (rt_float)((UnitMass / (double)UnitLength) / (double)UnitLength / (double)UnitLength); /* kg/m^3) */
  double GasParticleMass;
  if (header.mass[0] > 0.0) {
    GasParticleMass = (double)header.mass[0] * UnitMass; /* kg */
  } else {
    GasParticleMass = (double)P[0].Mass * UnitMass; /* kg */
  }
  const rt_float density_to_n_H = (1.0 - Constants.Y) / (Constants.mass_p + Constants.mass_e) * GadgetDensityUnit;
  const rt_float density_to_n_He = 0.25 * Constants.Y / (Constants.mass_p + Constants.mass_e) * GadgetDensityUnit;
  /* NumberOfHydrogenAtomsPerParticle ~ 10^(40 - (-27)) = 10^67 so need double */
  double NumberOfHydrogenAtomsPerParticle =
      GasParticleMass * (double)(1.0 - Constants.Y) / (double)(Constants.mass_p + Constants.mass_e);
  double NumberOfHeliumAtomsPerParticle =
      GasParticleMass * (double)Constants.Y / (double)(4.0 * (Constants.mass_p + Constants.mass_e));

  /* Gadget's "u" == "Entropy" is Energy/Mass with units (km/s)^2 */
  /* GadgetUtoTFactor ~ 10^(6 -27 -(-23)) = 10^-2 */
  rt_float mu = 4.0 / (4.0 - 3.0 * Constants.Y);
  rt_float GadgetUtoTFactor = 1.e6 * (2. / 3.) * mu * Constants.mass_p / Constants.k_B;

  rt_float t = 0.0;

  /* Allocate memory RT data */
  RT_Data *Data_p = RT_Data_Allocate(nParticles);
  RT_Data Data = *Data_p;
  if (RestartFile == NULL) {
    /* Initialize Data (SPH particle) fields */
#pragma omp parallel for
    for (iParticle = 0; iParticle < nParticles; ++iParticle) {
      /* Density of the particle. [kg m^-3] */
      rt_float const NsphInvCubeRoot = 1. / CBRT(Parameters.Nsph);
      rt_float ParticleRadius = (rt_float)SphP[iParticle].Hsml * UnitLength * NsphInvCubeRoot; /* m */
      Data.dR[iParticle] = ParticleRadius;                                                     /* m */
      Data.Density[iParticle] = (rt_float)(GasParticleMass / (4. / 3. * M_PI * (double)ParticleRadius) /
                                           (double)ParticleRadius / (double)ParticleRadius); /* kg m^-3 */
      Data.n_H[iParticle] = density_to_n_H * (rt_float)SphP[iParticle].Density;              /* m^-3 */
      ;
      Data.n_He[iParticle] = density_to_n_He * (rt_float)SphP[iParticle].Density; /* Default [m^-3] */
      Data.f_H1[iParticle] = Parameters.InitialFractions[0];
      Data.f_H2[iParticle] = Parameters.InitialFractions[1];
      Data.f_He1[iParticle] = Parameters.InitialFractions[2];
      Data.f_He2[iParticle] = Parameters.InitialFractions[3];
      Data.f_He3[iParticle] = Parameters.InitialFractions[4];
      rt_float InitialTemperature = 0.0;
      switch (Parameters.ProblemType) {
        case 0: /* N sources placed in halos in mass order */
          InitialTemperature = GadgetUtoTFactor * (rt_float)SphP[iParticle].Entropy;
          break;
        case 1: /* Test 1 */
          InitialTemperature = Parameters.InitialTemperature;
          break;
        case 2: /* Test 2 */
          InitialTemperature = Parameters.InitialTemperature;
          break;
        case 3: /* Test 3, clump and medium have different temperatures. */
          /* HACK trigger on density. The medium n=2e-4 cm^-3, the clump has 0.04 cm^-3 */
          if (Data.Density[iParticle] < 5e-24) {
            InitialTemperature = 8000.0; /* K */
          } else {
            InitialTemperature = 40.0; /* K */
          }
          break;
        case 100:
          InitialTemperature = Parameters.InitialTemperature;
          break;
        default:
          printf("%s: %i: ERROR: Unknown ProblemType %i\n", __FILE__, __LINE__, (int)Parameters.ProblemType);
          exit(EXIT_FAILURE);
      }
      /* Entropy [J/kg (m^3/kg)^(gamma_ad-1) */
      Data.Entropy[iParticle] = RT_EntropyFromTemperature(Constants, InitialTemperature, iParticle, Data);
      if (Data.Entropy[iParticle] < 0.0) {
        std::cout << "ERROR: Entropy=" << Data.Entropy[iParticle] << std::endl;
        exit(EXIT_FAILURE);
      }
      Data.T[iParticle] = InitialTemperature; /* Temperature [K] */
      if (iParticle < 5) {
        printf("Entropy=%5.3e; Temperature=%5.3e\n", Data.Entropy[iParticle], Data.T[iParticle]);
      }
    }

  } else {
    /* Restart from a dump file */
    std::cout << "Attempting to restart from " << RestartFile << std::endl;
    RT_FileHeaderT FileHeader;
    size_t const NLos = RT_LoadData(RestartFile, &Data_p, &FileHeader);
    if (NLos == 0) {
      std::cout << "ERROR: Unable to read from " << RestartFile << std::endl;
      exit(EXIT_FAILURE);
    }
    Data = *Data_p;
    t = (rt_float)FileHeader.Time;
    printf("Restarting at t=%5.3e s\n", t);
  }

  /* Skip over output times which are in the past.
   * Parameters.t_start is now correct, even if a restart file was provided
   */
  size_t OutputTimeIndex = 0;
  size_t IterationCount = 0;
  if (OutputTimes != NULL) {
    skipMissedOutputTimes(Parameters.t_start / outputTimesUnit, /* Million years */
                          OutputTimes,
                          nOutputTimes,
                          &OutputTimeIndex);
    if (OutputTimeIndex < nOutputTimes) {
      printf("First output time: %5.3e Ma\n", OutputTimes[OutputTimeIndex] * outputTimesUnit / Constants.Ma);
    }
  }
  if ((OutputTimes == NULL && Parameters.OutputIterationCount == 0) || OutputTimeIndex == nOutputTimes) {
    std::cout << "*****************************************" << std::endl
              << "WARNING: No output files will be written!" << std::endl
              << "*****************************************" << std::endl;
  }

  /* Sanity check */
  double TotalMassInVolume = 0.0;
  switch (Parameters.ProblemType) {
    case 0: /* Manitou with N sources */
      TotalMassInVolume = 3.549e+43;
      break;

    case 1: /* Test 1 */
    case 2: /* Test 2 */
      /* Note: Assuming (13.2 kpc)^3 volume. Not the (6.6 kpc)^3 volume described
       *       in the test documentation because we put the source at the centre */
      TotalMassInVolume = 1.131e+38;
      break;

    case 3: /* Test 3 */
      TotalMassInVolume = 7.02483e+36;
      break;

    case 100:
      /* Note: Assuming (13.2 kpc)^3 volume. Not the (6.6 kpc)^3 volume described
       *       in the test documentation because we put all the sources at the centre */
      TotalMassInVolume = 1.131e+38;
      break;

    default:
      printf("%s: %i: ERROR: Unknown ProblemType %i\n", __FILE__, __LINE__, (int)Parameters.ProblemType);
      exit(EXIT_FAILURE);
  }
  double BoxVolume = (double)header.BoxSize * (double)header.BoxSize * (double)header.BoxSize * (double)UnitLength *
                     (double)UnitLength * (double)UnitLength;
  double MeanDensity = 0;
  for (iParticle = 0; iParticle < nParticles; ++iParticle) {
    MeanDensity += Data.Density[iParticle];
  }
  MeanDensity /= (double)nParticles;
  double Mean_nH = 0;
  for (iParticle = 0; iParticle < nParticles; ++iParticle) {
    Mean_nH += (double)Data.n_H[iParticle];
  }
  Mean_nH /= (double)nParticles;
  printf("Mean_nH=%5.3e; BoxVolume=%5.3e; BoxSize=%5.3e; UnitLength=%5.3e\n",
         Mean_nH,
         BoxVolume,
         header.BoxSize,
         UnitLength);
  printf("SANITY CHECK (works for constant density only)\n");
  printf(" MeanDensity * BoxVolume      = %5.3e kg\n", MeanDensity * BoxVolume);
  printf(" Mean_nH*m_H*BoxVolume/(1-Y)  = %5.3e kg\n", Mean_nH * 1.6737e-27 * BoxVolume / (1.0 - Constants.Y));
  printf(" nParticles * GasParticleMass = %5.3e kg\n", nParticles * GasParticleMass);
  printf(" Should be:                     %5.3e kg\n", TotalMassInVolume);

#ifdef __AVX__
  printf("Using AVX SIMD extensions.\n");
#elif defined __SSE4_2__
  printf("Using SSE4.2 SIMD extensions.\n");
#endif

  rt_float t_Ma;
  if (RestartFile == NULL) {
    /* Dump data */
    ierr = snprintf(RTDataFile, LABEL_STR_SIZE - 1, "%s/RTData_t=START", Dir);
    if (ierr > (int)LABEL_STR_SIZE) {
      printf("ERROR: %s: %i: Path too long %s\n", __FILE__, __LINE__, RTDataFile);
      exit(EXIT_FAILURE);
    }
    RT_FileHeaderT FileHeader;
    FileHeader.ExpansionFactor = (float)ExpansionFactor;
    FileHeader.Redshift = (float)header.redshift;
    FileHeader.Time = 0.0;
    RT_SaveData(RTDataFile, &Data_p, 1, FileHeader);
  }

  /* Allocate and initialize the flag to trigger calculation of the column
   * densities */
  unsigned int *RecalculateColumnDensityFlag = new unsigned int[nParticles];
#pragma omp parallel for
  for (iParticle = 0; iParticle < nParticles; ++iParticle) {
    RecalculateColumnDensityFlag[iParticle] = Parameters.RecalculateColumnDensityIterationCount;
  }

  /* Determine the start time */
  if (RestartFile == NULL) {
    t = Parameters.t_start; /* s */
  }

#ifdef CUDA_RTSPH
  // DeviceMemoryPointers DevMemP = initializeRtsphCuda(nParticles, nParticles, tree_list[0]);
#endif

#ifdef CUDA_RT

  /* Allocate RT Data on the GPU
   * then copy from the host to the device */
  // define an array of rt data structures
  std::vector<RT_Data *> data_list;
  printf("Allocating RT Data and copying to devices...");
  for (int i = 0; i < nDev; i++) {
    cudaSetDevice(devs[i]);
    RT_Data *Data_dev_i = new RT_Data();
    copyRTDataFromHostToDevice(Data, Data_dev_i);
    data_list.push_back(Data_dev_i);
  }
  printf("done.\n");

  /* Increase stack size per thread*/
  for (int i = 0; i < nDev; i++) {
    cudaSetDevice(devs[i]);
    size_t pValue;
    bool const TerminateOnCudaError = true;
    cudaError_t cudaError = cudaDeviceGetLimit(&pValue, cudaLimitStackSize);
    checkCudaAPICall(cudaError, __FILE__, __LINE__, TerminateOnCudaError);
    printf("On Device %d, Current LimitStackSize=%lu. Setting to 2048 bytes\n", i, pValue);
    cudaError = cudaDeviceSetLimit(cudaLimitStackSize, 2048);
    checkCudaAPICall(cudaError, __FILE__, __LINE__, TerminateOnCudaError);
  }

/* Check data is correct on devices if debugging */
#  ifdef DEBUG
  for (int i = 0; i < nDev; i++) {
    cudaSetDevice(devs[i]);
    RT_CUDA_RT_Data_CheckData(*data_list[i], __FILE__, __LINE__);
  }
#  endif

  /* Allocate Rates on GPU, a set for each particle */
  printf("Allocating Rates on GPU and setting for each particle, populate...");
  std::vector<RT_RatesBlockT> rates_list(nDev);
  for (int i = 0; i < nDev; i++) {
    cudaSetDevice(devs[i]);
    rates_list[i] = RT_CUDA_RatesBlock_Allocate(nParticles);
    rates_list[i].NumCells = nParticles;
  }
  printf("done.\n");

  /* Allocate and copy Constants to device */
  // Constants don't actually need to be copied. All of the values used by kernels
  // are passed by value on kernel launch.
  // RT_ConstantsT Constants_dev = Constants;
  // RT_CUDA_CopyConstantsToDevice(Constants, &Constants_dev);

#endif /* End if CUDA_RT */

  /* Initialize involatile (mostly) variables of the photoionization integral */
  /* Variables of the integration over frequency that don't change during
   * the integration through time. */
  printf("Allocating involatiles...");
  std::vector<RT_IntegralInvolatilesT> involatiles_list(nDev);
  for (int i = 0; i < nDev; i++) {
    if (Source[0].SourceType != SourceMonochromatic) {
      ierr = RT_IntegralInvolatiles_allocate(Constants, &involatiles_list[i]);
      assert(ierr == EXIT_SUCCESS);

      ierr = RT_IntegralInvolatiles_setFrequenciesWeightsAlphas(Constants, involatiles_list[i]);
      assert(ierr == EXIT_SUCCESS);

    } else { /* Monochromatic (Test 1) */
             /* Monochromatic.
              * NOTE: In Manitou's current form, we can't mix Monochromatic and
              *       multifrequency sources.
              * ALSO: The Monochromatic case should be folded into
              *       RT_IntegralInvolatiles_allocate(), just to save memory.
              *       Othewise, the same effect can be made by setting in Constants.params:
              *       NLevels 1
              *         1 */
      involatiles_list[i].nFrequencies = 1;
      involatiles_list[i].nSpecies = 3;
      involatiles_list[i].Frequencies = (rt_float *)malloc(sizeof(rt_float));
      involatiles_list[i].Frequencies[0] = Source[0].nu;
      involatiles_list[i].Weights = (rt_float *)malloc(sizeof(rt_float));
      involatiles_list[i].Weights[0] = 1.0;
      involatiles_list[i].Alphas = (rt_float *)malloc(3 * sizeof(rt_float));
      involatiles_list[i].Alphas[0] = H1_H2(Source[0].nu, Constants);
      involatiles_list[i].Alphas[1] = He1_He2(Source[0].nu, Constants);
      involatiles_list[i].Alphas[2] = He2_He3(Source[0].nu, Constants);
      involatiles_list[i].Luminosities = (rt_float *)malloc(sizeof(rt_float));
    }
  }
  printf("done\n");

#ifdef CUDA_RT
  /* Involatiles_list will contain 'Involatiles_dev' values which will contain pointers that point to device memory */
  printf("Allocating Involatiles to Device...");
  std::vector<RT_IntegralInvolatilesT> d_involatiles_list(nDev);
  for (int i = 0; i < nDev; i++) {
    cudaSetDevice(devs[i]);
    ierr = RT_CUDA_allocateIntegralInvolatilesOnDevice(involatiles_list[i], &d_involatiles_list[i]);
    gpuErrchk(cudaPeekAtLastError());
    if (ierr != EXIT_SUCCESS) {
      printf("ERROR: %s: %i: Unable to allocate device memory for Integral Involatiles.\n", __FILE__, __LINE__);
      printf("FAILED DEVICE NUMBER: %d\n", i);
      exit(EXIT_FAILURE);
    }
  }
  printf("done.\n");

#endif

  /* Main loop */
  unsigned char LastIterationFlag = FALSE;  // defined here so that it applies for all threads

#ifdef CUDA_RT  // TODO ADD THE NCCL REQUIREMENT HERE
  /* Set the number of sources per device */
  const int num_sources_per_device = (int)std::ceil((double)Parameters.nSources / (double)nDev);

  /* Data outputs to file are done using threads so need to set omp to be able to handle dynamic nested parallel
   * threads*/
  omp_set_dynamic(0);

/* Begin parallel threads for GPUs */
#  pragma omp parallel num_threads(nDev) default(shared)
  {
    /* We want each GPU to be controlled by a thread */
    int tid = omp_get_thread_num();
    printf("Got to thread %i of %i\n",tid,nDev);
    cudaSetDevice(devs[tid]);
    gpuErrchk(cudaPeekAtLastError());
    DeviceMemoryPointers DevMemP = initializeRtsphCuda(nParticles, nParticles, tree_list[tid]);
#endif

    while (!LastIterationFlag) {
/* Broadcast RT_Data from root to all ranks and initialize rates */
#ifdef CUDA_RT
      if (tid == 0) {
        printf("Broadcasting data...");
      }
      if (nDev > 1) {
        /* Which is correct: count = nParticles * 12 or count = data_list[tid]->NBytes/sizeof(rt_float) */
        /* The difference is that nParticles * 12 takes the first 12 of the 15 rt_float fields,
         * as the last three are derived */
        NCCLCHECK(ncclBroadcast((void *)data_list[0]->MemSpace,
                                (void *)data_list[tid]->MemSpace,
                                data_list[0]->NBytes,
                                ncclChar,
                                0,  // root
                                comms[tid],
                                s[tid]));
        cudaStreamSynchronize(s[tid]);
        if (tid == 0) {
          printf("done.\n");
        }
      }

      // #  pragma omp barrier  // barrier for debugging segmentation fault speed
      /* Initialize Rates */
      if (tid == 0) {
        printf("Zeroing Rates...");
      }
      //       cudaDeviceSynchronize();  // device synch for debugging segmentation fault speed
      // #  pragma omp barrier           // barrier for debugging segmentation fault speed

      printf("Thread %d on device %d has arrived at the ZEROING.\n", tid, devs[tid]);
      printf("Zeroing rates...");
      zeroRates(rates_list[tid]); /* (Asynchronous) */
      printf("done\n");

      // #  pragma omp barrier  // make sure all devs have initialized rates speed

      if (tid == 0) {
        printf("done.\n");
      }

      printf("Thread %d on device %d has zeroed the rates.\n", tid, devs[tid]);
#else
      RT_Rates_ZeroRates(Rates);
#endif

      printf("Updating NumParticlesToUpdateColumnDensities...\n");
      size_t NumParticlesToUpdateColumnDensities = 0;
#pragma omp parallel for reduction(+ : NumParticlesToUpdateColumnDensities)
      /* Loop over particles to calculate column density updates*/
      for (iParticle = 0; iParticle < nParticles; ++iParticle) {
        if (RecalculateColumnDensityFlag[iParticle] >= Parameters.RecalculateColumnDensityIterationCount) {
          NumParticlesToUpdateColumnDensities++;
        }
      }
      printf("Thread %i will be finding column densities to %lu of %lu particles.\n",
             tid,
             NumParticlesToUpdateColumnDensities,
             nParticles);

      /* Create the lists of particles for which we need updated column densities.
       * Note: The logic for multiple sources works:
       *       1) All column densities are set on the first pass.
       *       2) Outside the loop over all sources, the list of particles for
       *          which column densities are required is created.
       *       3) Within the loop over all sources, for each source the column
       *          densities are updated for particles in the list.
       *       4) For each source, the RT is done. If the column density to a
       *          source is less then the threshold, it is tagged to enforce an
       *          update to the column density.
       *       The logic is only flawed in that for multiple sources, within
       *       the loop over sources, column densities will be updated for
       *       all particles in the update list, which will include particles
       *       with large column density for this source, but small column
       *       density for another source.  This is an error on the side of
       *       caution.
       */
      size_t *ParticlesToUpdateColumnDensities = new size_t[NumParticlesToUpdateColumnDensities];
#if 0
      /* This is a race condition on RecalculateColumnDensityFlag with more than one device.
         See also:  calculateSourceParticlePairRates_CUDA.cu, where I've blocked the update there. */
      size_t Placeholder = 0;
      for (iParticle = 0; iParticle < nParticles; ++iParticle) {
        if (RecalculateColumnDensityFlag[iParticle] >= Parameters.RecalculateColumnDensityIterationCount) {
          ParticlesToUpdateColumnDensities[Placeholder] = iParticle;
          RecalculateColumnDensityFlag[iParticle] = 0;
          Placeholder++;
        } else {
          RecalculateColumnDensityFlag[iParticle]++;
        }
      }
#else
      /* Hardwire all particles to update */
      printf("Hardwiring all particles to update...");
      for (iParticle = 0; iParticle < nParticles; ++iParticle) {
        ParticlesToUpdateColumnDensities[iParticle] = iParticle;
      }
      printf("done\n");
#endif

#ifdef CUDA_RTSPH
      printf("Sending Particles to update data to device...");
      std::unique_ptr<size_t[]> RtsphToUpdate(new size_t[NumParticlesToUpdateColumnDensities]);
      for (size_t i = 0; i < NumParticlesToUpdateColumnDensities; ++i) {
        RtsphToUpdate[i] = MapGadgetToRTSPH[ParticlesToUpdateColumnDensities[i]];
      }
      auto RtsphToUpdate_dev = cuda_malloc<size_t>(NumParticlesToUpdateColumnDensities);
      auto cerr = cudaMemcpy(RtsphToUpdate_dev.get(),
                             RtsphToUpdate.get(),
                             sizeof(size_t) * NumParticlesToUpdateColumnDensities,
                             cudaMemcpyHostToDevice);
      checkCudaAPICall(cerr, __FILE__, __LINE__, true);

      auto ParticlesToUpdateColumnDensities_dev = cuda_malloc<size_t>(NumParticlesToUpdateColumnDensities);
      cerr = cudaMemcpy(ParticlesToUpdateColumnDensities_dev.get(),
                        ParticlesToUpdateColumnDensities,
                        sizeof(size_t) * NumParticlesToUpdateColumnDensities,
                        cudaMemcpyHostToDevice);
      checkCudaAPICall(cerr, __FILE__, __LINE__, true);
      printf("done\n");
#endif

#ifdef CUDA_RT
      /* Loop over sources */
      /* We want each GPU to only loop over a subset of the sources defined using threadId */
      size_t source_start = tid * num_sources_per_device;
      size_t source_end = (tid + 1) * num_sources_per_device;
      // make sure the last device doesn't loop over non-existent sources
      if (source_end > Parameters.nSources) source_end = Parameters.nSources;
      // printf("Thread %d on dev %d looping over sources %lu to %lu\n", tid, devs[tid], source_start, source_end);
      // #  pragma omp barrier // debug speed test
      // if (source_start != source_end) {  // testing to see if this stops the idle thread from causing problems
      for (size_t iSource = source_start; iSource < source_end; iSource++) {
        CheckPointTime = getTimeInSeconds();
        // printf("Thread %d on dev %d processing source %lu\n", tid, devs[tid], iSource);
#else
    /* Loop over each source */
    // XXX ideally give all rays at once
    for (iSource = 0; iSource < Parameters.nSources; iSource++) {
      CheckPointTime = getTimeInSeconds();
#endif  // End the CUDA_RT ifdef for defining source ranges
#ifdef CUDA_RTSPH
#  ifdef SAH_BVH
        printf("Generate RT Source rays...");
        ierr = genRtSourceRays(Source[iSource],
                               *DevMemP.tree,
                               RtsphToUpdate_dev.get(),
                               NumParticlesToUpdateColumnDensities,
                               *DevMemP.rays,
                               header.BoxSize);
        printf("done.\n");
#  else
        printf("Generate RT Source rays on device %d...", devs[tid]);
        ierr = genRtSourceRays(Source[iSource],
                               *tree_list[tid],
                               RtsphToUpdate_dev.get(),
                               NumParticlesToUpdateColumnDensities,
                               *DevMemP.rays,
                               header.BoxSize);
        printf("done.\n");
#  endif

        if (ierr != EXIT_SUCCESS) {
          printf("ERROR genRTSourceRays Failed!");
        }
#else

      Rays rays;
      if (Parameters.ProblemType == 3) {
        /* Test 3, which requires plane wave */
        for (size_t i = 0; i < NumParticlesToUpdateColumnDensities; ++i) {
          iParticle = MapGadgetToRTSPH[ParticlesToUpdateColumnDensities[i]];
          Vector3 RayStart(0.0, tree.spheres.cy[iParticle], tree.spheres.cz[iParticle]);
          Vector3 RayFinish(tree.spheres.cx[iParticle], tree.spheres.cy[iParticle], tree.spheres.cz[iParticle]);
          rays.setFromPoints(RayStart, RayFinish, iParticle);
        }
      } else {
        /* Not plane wave */
        Vector3 RayStart(Source[iSource].r[0], Source[iSource].r[1], Source[iSource].r[2]);
        // Create rays from the source to all particles
        for (size_t i = 0; i < NumParticlesToUpdateColumnDensities; ++i) {
          iParticle = MapGadgetToRTSPH[ParticlesToUpdateColumnDensities[i]];
          Vector3 RayFinish(tree.spheres.cx[iParticle], tree.spheres.cy[iParticle], tree.spheres.cz[iParticle]);
          rays.setFromPoints(RayStart, RayFinish, iParticle);
        }
      }
#endif
        TimerArray[CreatingRays] += getTimeInSeconds() - CheckPointTime;

        /* Find column densities to each particle from this source */
        CheckPointTime = getTimeInSeconds();
#ifndef CUDA_RTSPH
        ierr = columnDensities(ParticlesToUpdateColumnDensities,
                               NumberOfHydrogenAtomsPerParticle,
                               NumberOfHeliumAtomsPerParticle,
                               UnitLength,
                               &tree,
                               &rays,
                               Data);
#else
      printf("Thread %d on device %d is calculating column densities...", tid, devs[tid]);
      ierr = columnDensities_CUDA(ParticlesToUpdateColumnDensities_dev.get(),
                                  NumberOfHydrogenAtomsPerParticle,
                                  NumberOfHeliumAtomsPerParticle,
                                  UnitLength,
                                  NumParticlesToUpdateColumnDensities,
                                  nParticles,
                                  DevMemP,
#  ifndef SAH_BVH
                                  *tree_list[tid],
#  endif
                                  *data_list[tid]);
      printf("Thread %d done.\n", tid);
      cudaDeviceSynchronize();
#endif
        if (ierr == EXIT_FAILURE) {
          printf("ERROR: %s: %i: Failed to calculate Column Densities.\n", __FILE__, __LINE__);
          (void)fflush(stdout);
          exit(EXIT_FAILURE);
        }
        TimerArray[ColumnDensities] += getTimeInSeconds() - CheckPointTime;

#ifdef DEBUG
        for (iParticle = 0; iParticle < nParticles; ++iParticle) {
          if (!std::isfinite(Data.column_H1[iParticle]) || !std::isfinite(Data.column_He1[iParticle]) ||
              !std::isfinite(Data.column_He2[iParticle])) {
            printf("ERROR: %s: %i: i=%i; N_H1_cum=%f; N_He1_cum=%f; N_He2_cum=%f;",
                   __FILE__,
                   __LINE__,
                   (int)iParticle,
                   Data.column_H1[iParticle],
                   Data.column_He1[iParticle],
                   Data.column_He2[iParticle]);
            exit(EXIT_FAILURE);
          }
        }
#endif

        /* Set the distance from the source to each particle */
        printf("Set the distance from source to each particle...");
#ifdef CUDA_RT
        setSourceToParticleDistance(
#  ifdef SAH_BVH
            *DevMemP.tree,
            DevMemP.tree_map.get(),
#  else
            *tree_list[tid],
            tree_list[tid]->get_map(),
#  endif
            Source[iSource],
            header.BoxSize,
            UnitLength,
            *data_list[tid]);
        cudaDeviceSynchronize();
#else
      if (Parameters.ProblemType == 3) {
        /* Test 3, which requires plane wave */
#  pragma omp parallel for
        for (iParticle = 0; iParticle < nParticles; ++iParticle) {
          Data.R[iParticle] = P[iParticle].Pos[0] * UnitLength;
        }
      } else {
#  pragma omp parallel for
        for (iParticle = 0; iParticle < nParticles; ++iParticle) {
          /* The distance to the source [m] */
          Data.R[iParticle] =
              SQRT((P[iParticle].Pos[0] - Source[iSource].r[0]) * (P[iParticle].Pos[0] - Source[iSource].r[0]) +
                   (P[iParticle].Pos[1] - Source[iSource].r[1]) * (P[iParticle].Pos[1] - Source[iSource].r[1]) +
                   (P[iParticle].Pos[2] - Source[iSource].r[2]) * (P[iParticle].Pos[2] - Source[iSource].r[2])) *
              UnitLength;
        }
      }
#endif  // end of ifdef CUDA_RT
        printf("done\n");

        printf("Setting Luminosity in Involatiles...");
        ierr = RT_IntegralInvolatiles_setLuminosity(
            Source[iSource], Constants, (rt_float)header.redshift, involatiles_list[tid]);
        if (ierr != EXIT_SUCCESS) {
          printf("ERROR: %s: %i: Failed to set Luminosity.\n", __FILE__, __LINE__);
          exit(EXIT_FAILURE);
        }
        printf("done\n");

        /* Find contribution to photoionization rates for each particle from this source */
        CheckPointTime = getTimeInSeconds();
#ifdef CUDA_RT
        printf("Copying Involatiles to device...");
        ierr = RT_CUDA_copyIntegralInvolatilesToDevice(involatiles_list[tid], d_involatiles_list[tid]);
        if (ierr != EXIT_SUCCESS) {
          printf("ERROR: %s: %i: Failed to copy Integral Involatiles to device.\n", __FILE__, __LINE__);
          exit(EXIT_FAILURE);
        }
        printf("done\n");

        // If using non-cuda RTSPH, column densities aren't on the device yet.
#  ifndef CUDA_RTSPH
        copyColumnDensitiesToDevice(Data, data_list[tid]);
        cudaDeviceSynchronize();
#  endif
        printf("Calculating the source-particle pair rates...");
        /* Calculate the source-particle pair rates. */
        calculateSourceParticlePairRates_CUDA(*data_list[tid],
                                              Parameters,
                                              Constants,
                                              ExpansionFactor,
                                              Source[iSource],
                                              d_involatiles_list[tid],
                                              /* Incremented */ rates_list[tid],
                                              /*     Updated */ RecalculateColumnDensityFlag);
        cudaDeviceSynchronize();
        printf("done\n");
#else

#  ifdef CUDA_RTSPH
      printf("Copy Column Densities from device %d...", devs[tid]);
      copyColumnDensitiesFromDevice(Data, *data_list[tid]);
      printf("done.\n");
#  endif

      ierr = calculateSourceParticlePairRates(Data,
                                              Parameters,
                                              Constants,
                                              (rt_float)header.redshift,
                                              Source[iSource],
                                              involatiles_list[tid],
                                              /* Incremented */ Rates,
                                              /*     Updated */ RecalculateColumnDensityFlag);
#endif
        TimerArray[IonizationRates] += getTimeInSeconds() - CheckPointTime;
      } /* End of loop over all sources */

#ifdef CUDA_RT
      // } /* End of if (source_start != source_end) test loop*/
#  pragma omp barrier
#endif

      printf("Deleting ParticlesToUpdateColumnDensities...");
      delete ParticlesToUpdateColumnDensities;
      printf("done\n");

      /* Update the overall heating rates
       * This must be done on each device as it will then be reduced
       */
      CheckPointTime = getTimeInSeconds();
#ifdef CUDA_RT
      printf("Update cumulative heating rate...");
      updateCumulativeHeatingRate(rates_list[tid]);
      cudaDeviceSynchronize();
      printf("done\n");
#endif

#ifdef CUDA_RT
      /* Call reductions to sum up data on master rank */
      if (nDev > 1) {
        printf("Thread %d on Device %d has reached the reductions.\n", tid, devs[tid]);
        if (tid == 0) {
          printf("Calling Reductions...");
        }
        NCCLCHECK(ncclReduce((const void *)rates_list[tid].I_H1,
                             (void *)rates_list[tid].I_H1,
                             nParticles,
                             ncclFloat,
                             ncclSum,
                             0,  // root
                             comms[tid],
                             s[tid]));
        NCCLCHECK(ncclReduce((const void *)rates_list[tid].I_He1,
                             (void *)rates_list[tid].I_He1,
                             nParticles,
                             ncclFloat,
                             ncclSum,
                             0,  // root
                             comms[tid],
                             s[tid]));
        NCCLCHECK(ncclReduce((const void *)rates_list[tid].I_He2,
                             (void *)rates_list[tid].I_He2,
                             nParticles,
                             ncclFloat,
                             ncclSum,
                             0,  // root
                             comms[tid],
                             s[tid]));
        NCCLCHECK(ncclReduce((const void *)rates_list[tid].Gamma_HI,
                             (void *)rates_list[tid].Gamma_HI,
                             nParticles,
                             ncclFloat,
                             ncclSum,
                             0,  // root
                             comms[tid],
                             s[tid]));
        NCCLCHECK(ncclReduce((const void *)rates_list[tid].G,
                             (void *)rates_list[tid].G,
                             nParticles,
                             ncclFloat,
                             ncclSum,
                             0,  // root
                             comms[tid],
                             s[tid]));
        if (tid == 0 && nDev > 1) {
          printf("done.\n");
        }
        // synchronizing on CUDA streams to wait for completion of NCCL operation
        cudaStreamSynchronize(s[tid]);
      }
/* Only the master device should perform the following operations */
// master does not have implicit barriers
#  pragma omp master
      {
#endif

        /* Now we have photoionization rates for each particle */
        /* Note, cooling rates can be done on the master rank as they are much simpler */
        /* Calculate the Recombination Cooling Rates */
        CheckPointTime = getTimeInSeconds();
#ifdef CUDA_RT
        printf("Calculate Recombination Rates Including Cooling...");
        calculateRecombinationRatesInclCooling(ExpansionFactor, Constants, *data_list[tid], rates_list[tid]);
        cudaDeviceSynchronize();
        printf("done.\n");
#else
        RT_CalculateRecombinationRatesInclCooling(ExpansionFactor, Constants, Data, Rates);
#endif
        TimerArray[Recombinations] += getTimeInSeconds() - CheckPointTime;

#ifndef NO_COOLING
        /* Extra cooling terms */
        CheckPointTime = getTimeInSeconds();
#  ifdef CUDA_RT
        RT_CUDA_CoolingRates(ExpansionFactor, Constants, *data_list[tid], rates_list[tid]);
        cudaDeviceSynchronize();
#  else
        RT_CoolingRatesExtra(ExpansionFactor, Constants, Data, Rates);
#  endif
        TimerArray[ExtraCooling] += getTimeInSeconds() - CheckPointTime;
#endif

#ifndef CUDA_RT
        printf("Update Cumulative Cooling Rates...");
        RT_UpdateCumulativeHeatingCoolingRate(Rates);
        printf("done\n");
#endif
        TimerArray[UpdateHeatingRates] += getTimeInSeconds() - CheckPointTime;

        /* Set the equilibrium nHI and nHII values (valid only in the highly ionised
         * regime) */
        CheckPointTime = getTimeInSeconds();
#ifdef CUDA_RT
        printf("Set Hydrogen Equilibrium Values...");
        RT_CUDA_setHydrogenEquilibriumValues(Constants, *data_list[tid], rates_list[tid]);
        cudaDeviceSynchronize();
        printf("done.\n");
#else
        ierr = RT_SetHydrogenFractionEquilibriumValues(Rates, Data);
        if (ierr == EXIT_FAILURE) {
          printf("ERROR: %s: %i: Failed to calculate equilibrium values.\n", __FILE__, __LINE__);
          (void)fflush(stdout);
          exit(EXIT_FAILURE);
        }
#endif
        TimerArray[EquilibriumValues] += getTimeInSeconds() - CheckPointTime;

        /* Find the largest timestep that allows us to safely integrate through dt */
        CheckPointTime = getTimeInSeconds();
        rt_float NextOutputTime;
        if (NULL != OutputTimes) {
          /* NextOutputTime should be set to the next in the list of output times */
          size_t const NumSkipped =
              skipMissedOutputTimes(t / outputTimesUnit, OutputTimes, nOutputTimes, &OutputTimeIndex);
          if (NumSkipped > 0) {
            printf("Skipping %lu Output Times. Next Output time is %5.3e Ma (%5.3e s)\n",
                   NumSkipped,
                   OutputTimes[OutputTimeIndex] * (outputTimesUnit / Constants.Ma),
                   OutputTimes[OutputTimeIndex] * outputTimesUnit);
          }
          NextOutputTime = FMIN(OutputTimes[OutputTimeIndex] * outputTimesUnit, Parameters.t_stop);
        } else {
          NextOutputTime = Parameters.t_stop;
        }

#ifdef CUDA_RT
        printf("Find Next Timestep...");
        findNextTimestep(nParticles,
                         NextOutputTime,
                         t,
                         *data_list[tid],
                         rates_list[tid],
                         Constants,
                         /* Output */
                         dt_RT,
                         LastIterationFlag);
        cudaDeviceSynchronize();
        printf("done.\n");
#else
    RT_FindNextTimestep(nParticles,
                        NextOutputTime,
                        t,
                        Data,
                        Rates,
                        Constants,
                        /* Output */
                        &dt_RT,
                        &LastIterationFlag);
#endif
        TimerArray[TimeStep] += getTimeInSeconds() - CheckPointTime;

        /* Correct if LastIterationFlag was incorrectly set.
         * TODO: LastIterationFlag should be removed from *findNextTimestep. */
        if (nOutputTimes > 0) {
          if (OutputTimeIndex < nOutputTimes - 1) {
            LastIterationFlag = FALSE;
          } else {
            /* If the time at the end of this step is greater than or equal to the last
             * output time, then this is the last iteration. Otherwise we are approaching
             * the last output time but haven't exceeded it. */
            if ((t + dt_RT) >= (OutputTimes[OutputTimeIndex] * outputTimesUnit)) {
              LastIterationFlag = TRUE;
            } else {
              LastIterationFlag = FALSE;
            }
          }
        }
        /* t_stop is a hard upper limit */
        if ((t + dt_RT) >= Parameters.t_stop) LastIterationFlag = TRUE;

        CheckPointTime = getTimeInSeconds();
        /* Update the particle values through dt_RT */
#ifdef CUDA_RT
        updateParticles(dt_RT,
                        header.redshift,
                        Constants,
                        rates_list[tid],
                        /* Updated */ *data_list[tid]);
        cudaDeviceSynchronize();
#else
        RT_UpdateParticles(dt_RT,
                           header.redshift,
                           Constants,
                           Rates,
                           /* Updated */ Data);
#endif
        TimerArray[Updating] += getTimeInSeconds() - CheckPointTime;

        if (Parameters.ProblemType == 1) {
/* Test 1 is ISOTHERMAL */
#ifdef CUDA_RT
          resetEntropyAndTemperature_CUDA(Parameters.InitialTemperature, Constants, *data_list[tid]);
          cudaDeviceSynchronize();
#else
          resetEntropyAndTemperature(Parameters.InitialTemperature, Constants, Data);
#endif
        }

        t += dt_RT;
        printf("t = %5.3e s ( %5.3e Ma); dt = %5.3e s ( %5.3e Ma); t_stop = %5.3e s ( %5.3e Ma)\n",
               t,
               t / Constants.Ma,
               dt_RT,
               dt_RT / Constants.Ma,
               Parameters.t_stop,
               Parameters.t_stop / Constants.Ma);

        fprintf(TimestepFile, "%5d %.5f %.5f\n", ++iteration_count, t / Constants.Ma, dt_RT / Constants.Ma);
        (void)fflush(TimestepFile);

        CheckPointTime = getTimeInSeconds();

#ifdef CUDA_RT
        /* Copy device data to host */
        copyRTDataFromDeviceToHost(*data_list[tid], Data);
#endif
        TimerArray[MemCopy] += getTimeInSeconds() - CheckPointTime;

        CheckPointTime = getTimeInSeconds();
        /* Test if we should dump the data */
        t_Ma = t / Constants.Ma; /* Million years */
        bool DumpRTData = false;
        rt_float const t_outputTimeUnits = t / outputTimesUnit;
        if (NULL != OutputTimes) {
          if (isOutputTime(t_outputTimeUnits, OutputTimes, nOutputTimes, OutputTimeIndex)) {
            DumpRTData = true;
            OutputTimeIndex++;
          }
        } else if (Parameters.OutputIterationCount != 0 && 0 == (IterationCount % Parameters.OutputIterationCount)) {
          /* Every nth iteration was requested */
          DumpRTData = true;
        }

        /* Dump data if necessary */
        if (DumpRTData) {
          ierr = snprintf(RTDataFile, LABEL_STR_SIZE - 1, "%s/RTData_t=%07.3f", Dir, t_outputTimeUnits);
          if (ierr > (int)LABEL_STR_SIZE) {
            printf("ERROR: %s: %i: Path too long %s\n", __FILE__, __LINE__, RTDataFile);
            exit(EXIT_FAILURE);
          }
          RT_FileHeaderT FileHeader;
          FileHeader.ExpansionFactor = (float)ExpansionFactor;
          FileHeader.Redshift = (float)header.redshift;
          FileHeader.Time = (float)t;
#ifdef CUDA_RTSPH
          copyColumnDensitiesFromDevice(Data, *data_list[tid]);
#endif
          RT_SaveData(RTDataFile, &Data_p, 1, FileHeader);

          saveDeviceRatesToFile(*data_list[tid], rates_list[tid], t, outputTimesUnit, Dir);
        }
        TimerArray[IO] += getTimeInSeconds() - CheckPointTime;

        IterationCount++;
        double LastIterationTime = getTimeInSeconds() - StartTime - TotalTime;
        TotalTime = getTimeInSeconds() - StartTime;
        std::cout << "Iteration walltime = " << LastIterationTime << " s;";

        /* give a current runtime output */
        int const TotalSeconds = static_cast<int>(TotalTime);
        int NumDays = TotalSeconds / 60 / 60 / 24;
        int NumHours = (TotalSeconds / 60 / 60) % 24;
        int NumMins = (TotalSeconds / 60) % 60;
        int NumSecs = TotalSeconds % 60;
        printf(" Cumulative Walltime = %dd %dh %dm %ds\n", NumDays, NumHours, NumMins, NumSecs);

        /* Check if we are approaching the walltime limit and restart if necessary */
        if ((Parameters.WallTimeLimit != 0) &&
            ((unsigned int)floor(TotalTime + LastIterationTime) > Parameters.WallTimeLimit)) {
          LastIterationFlag = TRUE;
          std::cout << "Terminating on WallTimeLimit" << std::endl;
          if (Parameters.RestartFlag == 1) {
            /* Issue command to restart */
            std::cout << "Attempting restart with command:" << Parameters.RestartCommand << std::endl;
            /* It is important that command gets backgrounded or it backgrounds itself */
            ierr = system(Parameters.RestartCommand);
            std::cout << "Restart command returned " << ierr << std::endl;
          }
        }
        printf("\n"); /* make boundary between iterations more obvious */
        (void)fflush(stdout);

#ifdef CUDA_RT
      }                // omp master
#  pragma omp barrier  // pragma omp master does not have an implicit barrier
#endif

    } /* end of loop over time */
#ifdef CUDA_RT
    printf("Thread %d executed successfully.\n", tid);
  }                     // omp parallel
#  pragma omp taskwait  // wait for child tasks to finish

#endif
  double SumKnownTimes = 0.0;
  for (size_t i = 0; i < nCodeTimes; ++i) {
    SumKnownTimes += TimerArray[i];
  }
  printf("%-22s = %8.3lf s\n", "Walltime", TotalTime);
  printf("%-22s = %4zu\n", "Iteration count", IterationCount);
  printf("%-22s = %8.3lf s\n", "Time per iteration", TotalTime / IterationCount);
  for (int i = 0; i < nCodeTimes; ++i) {
    printf("%-22s = %8.3lf s (%6.3lf %%)\n", to_string(CodeTimes(i)), TimerArray[i], 100. * TimerArray[i] / TotalTime);
  }
  double OtherTimes = TotalTime - SumKnownTimes;
  printf("%-22s = %8.3lf s (%6.3lf %%)\n", "Other", OtherTimes, 100. * OtherTimes / TotalTime);

  /* Dump final data */
  t_Ma = t / Constants.Ma; /* Million years */
  ierr = snprintf(RTDataFile, LABEL_STR_SIZE - 1, "%s/RTData_t=%07.3f", Dir, t_Ma);
  if (ierr > (int)LABEL_STR_SIZE) {
    printf("ERROR: %s: %i: Path too long %s\n", __FILE__, __LINE__, RTDataFile);
    exit(EXIT_FAILURE);
  }
  RT_FileHeaderT FileHeader;
  FileHeader.ExpansionFactor = (float)ExpansionFactor;
  FileHeader.Redshift = (float)header.redshift;
  FileHeader.Time = t;
#ifdef CUDA_RTSPH
  copyColumnDensitiesFromDevice(Data, *data_list[0]);
#endif
  RT_SaveData(RTDataFile, &Data_p, 1, FileHeader);

  /* Free malloc'ed memory */
  RT_RatesBlock_Free(Rates);
  RT_Data_Free(Data_p);

#ifdef CUDA_RT
  RT_CUDA_freeIntegralInvolatilesOnDevice(d_involatiles_list[0]);
#endif
  RT_IntegralInvolatiles_free(involatiles_list[0]);

  // Free the CUDA device enumeration
  free(devs);
  // Free the CUDA Stream space
  free(s);

  exit(EXIT_SUCCESS);
}
