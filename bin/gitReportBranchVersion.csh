#!/bin/tcsh -f

# Report the source of the code, the branch, and the last commit on which it
# is drawn
set Source=`git config --get remote.origin.url`
set Branch=`git branch | awk '{print $2;}'`
set Commit=`git show | head -1 | awk '{print $2;}'`

echo $Source $Branch $Commit

# And report differences between the source code and the commit it is based on.
echo DIFFS:
git diff
