#ifndef _CONFIG_H_
#define _CONFIG_H_

#ifndef DEBUG
#  define DEBUG
#endif
#define VERBOSE 0

// #define PROFILE
// #undef DEBUG

/* If any of the CUDA_* are defined, set CUDA = 1 in make.config */
#define CUDA_ALL
// #define CUDA_RT
// #define CUDA_RTSPH
/* Define CUDA_FAST_MATH to force the code to use fast and low-accuracy
 * device intrisics in functions that include RT_CUDA_Precision.h.
 * Not clear if his is also forced by simply using -use_fast_math. */
// #define CUDA_FAST_MATH

#define GAUSSIAN_QUADRATURE
#define ADAPTIVE_TIMESCALE
// #define NO_COOLING
#define CONSTANT_DENSITY
#define USE_B_RECOMBINATION_CASE_ONLY
#define ENFORCE_MINIMUM_CMB_TEMPERATURE 10.0

/* Disable He1 timescale, which gets messed up when one of the
 * other species == n_He */
#define RT_TIMESCALE_DISABLE_HE1_TIMESCALE

/* Uncomment the following for double-precision RT calculations */
// #define RT_DOUBLE

/* The following should be parameters read in */

#define N_Levels_1 4
#define N_Levels_2 4
#define N_Levels_3 8

#ifdef CUDA_ALL
#  define CUDA_RT
#  define CUDA_RTSPH
#endif

/* Track ionization rates.  Just information, no change to results. */
#define TRACK_IONIZATION_RATE

/* There are two ways to build a tree.  The original way designed by 
 * Martin Ruefenacht as part of his RTSPH package and is selected with
 * SAH_BVH. The "default" way is Peter Bell's LinearBVH  method.
 * I *think* Peter's way is the one that doesn't work with CUDA-12 
 * Martin's way used to be selected with CUDA_RTSPH which was used to
 * select offloading RTSPH to the GPU. 
 * What is needed is a clearer way to select GPU offloading of the 
 * column-density calculation and a separate way to select which tree
 * to build */
// #define SAH_BVH

#endif /* _CONFIG_H_ */
