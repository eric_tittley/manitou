%-------------------------------------------------------------------------
%----  This file contains the input parameters needed at run-time for 
%       simulations. It is based on and closely resembles the GADGET-3
%       parameterfile (format of which and parsing routines written by 
%       Volker Springel [volker.springel@h-its.org]). It has been updated
%       with new naming conventions and additional variables as needed by 
%       Phil Hopkins [phopkins@caltech.edu] for GIZMO.
%-------------------------------------------------------------------------

%----  Relevant files (filenames and directories)
InitCondFile  Test6.gadget
OutputDir     output

%---- File formats (input and output)
ICFormat    1  % 1=unformatted (gadget) binary, 3=hdf5, 4=cluster
SnapFormat  3  % 1=unformatted (gadget) binary, 3=hdf5


%---- Output parameters 
RestartFile                 restart 
SnapshotFileBase            snapshot 
OutputListOn                1  % =1 to use list in "OutputListFilename" 
OutputListFilename          output_times.txt  % list of times (in code units) for snaps
NumFilesPerSnapshot         1
NumFilesWrittenInParallel   1  % must be < N_processors & power of 2

%---- Output frequency 
TimeOfFirstSnapshot     0.0  % time (code units) of first snapshot

%---- CPU run-time and checkpointing time-limits
TimeLimitCPU            1000000  % in seconds
CpuTimeBetRestartFile   864000   % in seconds [10 days, to disable restart]

%---- Desired simulation beginning and end times (in code units) for run
TimeBegin   0.0    % Beginning of the simulation
TimeMax     25.56780412614237e-03 % End of the simulation

%---- Maximum and minimum timesteps allowed
MaxSizeTimestep         2.0e-5  % in code units, set for your problem
MinSizeTimestep         1.0e-14 % set this very low, or get the wrong answer


%---- System of units
UnitLength_in_cm            3.085678e21     % 1.0 kpc/h
UnitMass_in_g               1.989e43          % 1.0e10 solar masses/h
UnitVelocity_in_cm_per_s    1.0e5           % 1 km/sec
GravityConstantInternal     0                % calculated by code if =0

%---- Cosmological parameters
ComovingIntegrationOn   0    % is it cosmological? (yes=1, no=0)
BoxSize                 1.6  % in code units
Omega0                  0.0  % =0 for non-cosmological
OmegaLambda             0.0  % =0 for non-cosmological
OmegaBaryon             0.0  % =0 for non-cosmological
HubbleParam             1.0  % little 'h'; =1 for non-cosmological runs


%----- Memory allocation
MaxMemSize          55000   % sets maximum MPI process memory use in MByte
PartAllocFactor     5.0     % memory load allowed for better cpu balance
BufferSize          200     % in MByte

%---- Rebuild domains when >this fraction of particles active
TreeDomainUpdateFrequency   0.005    % 0.0005-0.05, dept on core+particle number


%---- (Optional) Initial hydro temperature & temperature floor (in Kelvin)
InitGasTemp     100.0       % set by IC file if =0
MinGasTemp      10.0        % don't set <10 in explicit feedback runs, otherwise 0

%---- Hydro reconstruction (kernel) parameters
DesNumNgb               32      % domain-reconstruction kernel number: 32 standard, 60-114 for quintic
MaxHsml                 1.0e10  % minimum gas kernel length (some very large value to prevent errors)
MinGasHsmlFractional    0.0     % minimum kernel length relative to gas force softening (<= 1)


%---- Gravitational softening lengths 
%----- Softening lengths per particle type. If ADAPTIVE_GRAVSOFT is set, these  
%-------- are the minimum softening allowed for each type ------- 
%-------- (units are co-moving for cosmological integrations)
SofteningGas    5.0e-4   % gas (particle type=0) (in co-moving code units)
SofteningHalo   0.001    % dark matter/collisionless particles (type=1)
SofteningDisk   0.150    % collisionless particles (type=2)
SofteningBulge  0.500    % collisionless particles (type=3)
SofteningStars  0.001    % stars spawned from gas (type=4)
SofteningBndry  0.001    % black holes (if active), or collisionless (type=5)
%---- if these are set in cosmo runs, SofteningX switches from comoving to physical
%------- units when the comoving value exceeds the choice here
%------- (these are ignored, and *only* the above are used, for non-cosmo runs)
SofteningGasMaxPhys     0.0005  % e.g. switch to 0.5pc physical below z=1
SofteningHaloMaxPhys    0.010
SofteningDiskMaxPhys    0.075 
SofteningBulgeMaxPhys   0.250 
SofteningStarsMaxPhys   0.0005 
SofteningBndryMaxPhys   0.0005 
