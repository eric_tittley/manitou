#include <RT_SourceT.h>
#include <cudasupport.h>

#include "Vec3.h"
#include "generateRays.h"

#ifdef SAH_BVH
#  include <D_BVH.cuh>
#else
#  include <LinearBVH.cuh>
#endif
#include <D_Rays.cuh>
#include <cmath>
#include <float3_util.cuh>

namespace {
#ifdef SAH_BVH
using SphereType = cudaTextureObject_t;

__device__ float3 getSpherePos(SphereType spheres, uint32_t isphere) {
  const float4 sphere_data = tex1Dfetch<float4>(spheres, isphere);
  return make_float3(sphere_data.x, sphere_data.y, sphere_data.z);
}

__host__ SphereType getSpheres(const TreeType &tree) {
  cudaTextureObject_t spheres;
  ERT_CUDA_CHECK(cudaMemcpy(&spheres, &tree.spheres, sizeof(spheres), cudaMemcpyDeviceToHost));
  return spheres;
}
#else
using SphereType = const LinearBVH::Sphere *;

__device__ float3 getSpherePos(SphereType spheres, uint32_t isphere) { return spheres[isphere].pos; }

__host__ SphereType getSpheres(const TreeType &tree) { return tree.get_spheres(); }

#endif

}  // namespace

__global__ void genPlaneRays(PlaneSource source, SphereType spheres, const size_t *ids, size_t num_rays, D_Rays &rays) {
  const size_t istart = blockIdx.x * blockDim.x + threadIdx.x;
  const size_t span = gridDim.x * blockDim.x;

  // distance from global origin to surface of plane
  const size_t d = dot(source.origin, source.direction);

  for (size_t i = istart; i < num_rays; i += span) {
    auto iparticle = ids[i];
    const float3 ray_end = getSpherePos(spheres, iparticle);
    const float tmax = dot(ray_end, source.direction) - d;
    const float3 ray_start = ray_end - tmax * source.direction;

    rays.ox[i] = ray_start.x;
    rays.oy[i] = ray_start.y;
    rays.oz[i] = ray_start.z;

    rays.tmax[i] = tmax;

    rays.dx[i] = source.direction.x;
    rays.dy[i] = source.direction.y;
    rays.dz[i] = source.direction.z;

    rays.ivdx[i] = 1.f / source.direction.x;
    rays.ivdy[i] = 1.f / source.direction.y;
    rays.ivdz[i] = 1.f / source.direction.z;

    rays.id[i] = iparticle;
  }
}

__global__ void genPointRays(PointSource source, SphereType spheres, const size_t *ids, size_t num_rays, D_Rays &rays) {
  const size_t istart = blockIdx.x * blockDim.x + threadIdx.x;
  const size_t span = gridDim.x * blockDim.x;
  const float3 ray_start = source.origin;

  // Create rays from the source to all particles
  for (size_t i = istart; i < num_rays; i += span) {
    size_t iparticle = ids[i];
    const float3 ray_end = getSpherePos(spheres, iparticle);
    float3 dr = ray_end - ray_start;
    float tmax = std::sqrt(dot(dr, dr));
    dr /= tmax;

    rays.ox[i] = ray_start.x;
    rays.oy[i] = ray_start.y;
    rays.oz[i] = ray_start.z;

    rays.tmax[i] = tmax;

    rays.dx[i] = dr.x;
    rays.dy[i] = dr.y;
    rays.dz[i] = dr.z;

    rays.ivdx[i] = 1.f / dr.x;
    rays.ivdy[i] = 1.f / dr.y;
    rays.ivdz[i] = 1.f / dr.z;

    rays.id[i] = iparticle;
  }
}

__host__ int genRtSourceRays(const RT_SourceT &src,
                             const TreeType &tree,
                             const size_t *ids,
                             size_t num_rays,
                             D_Rays &rays,
                             float box_size) {
  const int grid_size = 128, block_size = 512;
  switch (src.AttenuationGeometry) {
    case PlaneWave: {
      float3 dir = vecFromArray(src.r);
      // Slightly outside the box so particles aren't missed by rounding
      float3 origin =
          box_size * make_float3(dir.x >= 0.f ? -0.1f : 1.1f, dir.y >= 0.f ? -0.1f : 1.1f, dir.z >= 0.f ? -0.1f : 1.1f);
      genPlaneRays<<<grid_size, block_size>>>(PlaneSource{origin, dir}, getSpheres(tree), ids, num_rays, rays);
      break;
    }
    case SphericalWave: {
      genPointRays<<<grid_size, block_size>>>(PointSource{vecFromArray(src.r)}, getSpheres(tree), ids, num_rays, rays);
      break;
    }
    default: {
      printf("ERROR: Unsupported source geometry %d", static_cast<int>(src.AttenuationGeometry));
      return EXIT_FAILURE;
    }
  }
  return EXIT_SUCCESS;
}
