#ifndef _MANITOU_VARS_
#define _MANITOU_VARS_
#include "Manitou.h"
#include "Manitou_CUDA.h"
#include "dynamicSources.h"

// The host RT_Data structure needs to be global as it is used for both
// computing and for I/O
extern RT_Data* _Data_p;
extern RT_Data _Data;

extern RT_RatesBlockT _Rates_dev;

extern RT_ConstantsT _Constants;
extern RT_ConstantsT _Constants_dev;

extern ParametersT _Parameters;

extern DynamicSources _DynamicSources;
extern std::vector<RT_SourceT> _Source;
#ifdef MANITOU_FOF_SOURCES
extern float* _source_table;
#endif

extern float _current_time;

extern unsigned int* _RecalculateColumnDensityFlag;

enum CodeTimes {
  CumulativeTime,
  CreatingSpheres,
  CreatingTree,
  CreatingRays,
  ColumnDensities,
  IonizationRates,
  Recombinations,
  ExtraCooling,
  UpdateHeatingRates,
  TimeStep,
  Updating,
  EquilibriumValues,
  MemCopy,
  UpdatingFromGizmo,
  UpdatingToGizmo,
  IO,
  nCodeTimes
};

extern double* _TimerArray;
extern double _StartTime;
extern double _TotalTime;
#endif
