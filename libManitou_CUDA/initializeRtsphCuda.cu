#include "DeviceMemoryPointers.cuh"
#include "Manitou_CUDA.h"
#ifdef SAH_BVH
#  include "BVH.h"
#else
#  include "LinearBVH.cuh"
#endif

DeviceMemoryPointers initializeRtsphCuda(size_t const nParticles,
                                         size_t const nRays,
#ifdef SAH_BVH
                                         BVH* tree) {
#else
                                         LinearBVH* tree) {
#endif
  /* Create structure, accessed via pointer. It will not be destroyed. */
  DeviceMemoryPointers DevMemP{0};
  /* Initialize memory on the CUDA device.
   * Allocate space for float3s, to be used if there is Helium.
   * This extra space will be wasted if there isn't any Helium,
   * but shouldn't otherwise cause problems. */
  DevMemP.q = cuda_malloc<float3>(nParticles);
  DevMemP.weights = cuda_malloc<float3>(nRays);
#ifdef SAH_BVH
  DevMemP.tree_map = cuda_malloc<uint32_t>(nParticles);
  cudaMemcpy(DevMemP.tree_map.get(), tree->map.data(), nParticles * sizeof(uint32_t), cudaMemcpyHostToDevice);
  DevMemP.tree.reset(copyTreeToDevice(tree));
#endif
  DevMemP.rays.reset(allocateRaysOnDevice(nRays));
  return DevMemP;
}
