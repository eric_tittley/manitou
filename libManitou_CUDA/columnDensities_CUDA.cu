/* Compile-time configuration */
#include <stdio.h>
#include <stdlib.h>

#include "config.h"
#ifdef DEBUG
#  include <cassert>
#  include <cmath>
#endif
#include <memory>

#include "BVH.h"
#include "D_BVH.cuh"
#include "D_Rays.cuh"
#include "DeviceMemoryPointers.cuh"
#include "Epsilon.h"
#include "IntegrateAlongRay.h"
#include "Manitou_CUDA.h"
#include "RT_Data.h"
#include "Ray.h"
#include "Weight.cuh"
#include "columnDensities.h"
#include "cuda_profiler_api.h"
#include "cudasupport.h"

#ifdef SAH_BVH
#  include "D_BVH.cuh"
#else
#  include "LinearBVH.cuh"
#endif

template <typename T>
struct pointers3 {
  T *x, *y, *z;
};

template <typename T>
__global__ void reorderQuantityToIntegrate(const T *in, size_t num, const uint32_t *map, T *out) {
  size_t const istart = blockIdx.x * blockDim.x + threadIdx.x;
  size_t const span = gridDim.x * blockDim.x;  // nBlocks * nThreadsPerBlock

  for (size_t i = istart; i < num; i += span) {
    out[i] = in[map[i]];
  }
}

__global__ void reorderQuantityToIntegrate(const pointers3<rt_float> in, size_t num, const uint32_t *map, float3 *out) {
  size_t const istart = blockIdx.x * blockDim.x + threadIdx.x;
  size_t const span = gridDim.x * blockDim.x;  // nBlocks * nThreadsPerBlock

  for (size_t i = istart; i < num; i += span) {
    auto idx = map[i];
    out[i].x = in.x[idx];
    out[i].y = in.y[idx];
    out[i].z = in.z[idx];
  }
}

__global__ void copyOutColumnDensities(const rt_float *tmp_cd,
                                       size_t nParticlesToUpdate,
                                       const size_t *ParticlesToUpdateColumnDensities,
                                       rt_float *dest,
                                       rt_float scale_factor) {
  size_t const istart = blockIdx.x * blockDim.x + threadIdx.x;
  size_t const span = gridDim.x * blockDim.x;  // nBlocks * nThreadsPerBlock

  for (size_t iParticle = istart; iParticle < nParticlesToUpdate; iParticle += span) {
    dest[ParticlesToUpdateColumnDensities[iParticle]] = tmp_cd[iParticle] * scale_factor;

#ifdef DEBUG
    size_t const particleID = ParticlesToUpdateColumnDensities[iParticle];
    if (!std::isfinite(dest[particleID]) || dest[particleID] < -FLOAT_MIN) {
      printf(
          "ERROR: %s: %i: Destination[%i]=%5.3e"
          " TemporaryStorageOfColumnDensities=%5.3e; scale_factor=%5.3e\n",
          __FILE__,
          __LINE__,
          (int)iParticle,
          dest[particleID],
          tmp_cd[iParticle],
          scale_factor);
      assert(false);
    }
#endif
  }
}

__global__ void copyOutColumnDensities(const float3 *tmp_cd,
                                       size_t nParticlesToUpdate,
                                       const size_t *ParticlesToUpdateColumnDensities,
                                       pointers3<rt_float> dest,
                                       float3 scale_factor) {
  size_t const istart = blockIdx.x * blockDim.x + threadIdx.x;
  size_t const span = gridDim.x * blockDim.x;  // nBlocks * nThreadsPerBlock

  for (size_t iParticle = istart; iParticle < nParticlesToUpdate; iParticle += span) {
    const auto idx = ParticlesToUpdateColumnDensities[iParticle];
    dest.x[idx] = tmp_cd[iParticle].x * scale_factor.x;
    dest.y[idx] = tmp_cd[iParticle].y * scale_factor.y;
    dest.z[idx] = tmp_cd[iParticle].z * scale_factor.z;

#ifdef DEBUG
    for (int iSpecies = 0; iSpecies < 3; ++iSpecies) {
      size_t const particleID = ParticlesToUpdateColumnDensities[iParticle];
      if (!std::isfinite(dest.x[particleID]) || dest.x[particleID] < -FLOAT_MIN || !std::isfinite(dest.x[particleID]) ||
          dest.x[particleID] < -FLOAT_MIN || !std::isfinite(dest.x[particleID]) || dest.x[particleID] < -FLOAT_MIN) {
        printf(
            "ERROR: %s: %d: Destination[%d]=[%5.3e %5.3e %5.3e];"
            " TemporaryStorageOfColumnDensities=[%5.3e %5.3e %5.3e];"
            " scale_factor=[%5.3e %5.3e %5.3e]\n",
            __FILE__,
            __LINE__,
            (int)iParticle,
            dest.x[particleID],
            dest.y[particleID],
            dest.z[particleID],
            tmp_cd[iParticle].x,
            tmp_cd[iParticle].y,
            tmp_cd[iParticle].z,
            scale_factor.x,
            scale_factor.y,
            scale_factor.z);
        assert(false);
      }
    }
#endif
  }
}

int cD_CUDA_ProcessSpecies(size_t const nParticlesTotal,
                           size_t const *const ParticlesToUpdateColumnDensities_dev,
                           double const NumberOfAtomsPerParticle,
                           rt_float const UnitLength,
                           const size_t nParticlesToUpdate,
                           DeviceMemoryPointers const &DevMemP,
#ifndef SAH_BVH
                           LinearBVH const &tree,
#endif
                           Weights const &sph_weights,
                           rt_float const *const QuantityToIntegrate_dev,
                           rt_float *const Destination_dev) {

  const int grid_size = 128, block_size = 512;

#ifdef SAH_BVH
  const uint32_t *tree_map = DevMemP.tree_map.get();
#else
  const uint32_t *tree_map = tree.get_map();
#endif

  /* if q is set to a mass (kg), a mass column density is returned (kg/(units of h)^2) */
  reorderQuantityToIntegrate<<<grid_size, block_size>>>(
      QuantityToIntegrate_dev, nParticlesTotal, tree_map, (rt_float *)DevMemP.q.get());

  /* Calculate on the GPU the integrals of q along the rays. */
  launchFindIntersectsKernel(DevMemP.rays.get(),
#ifdef SAH_BVH
                             DevMemP.tree.get(),
#else
                             &tree,
#endif
                             sph_weights,
                             (float *)DevMemP.weights.get(),
                             (float *)DevMemP.q.get(),
                             nParticlesToUpdate);

  rt_float const ScaleFactor = NumberOfAtomsPerParticle / (double)UnitLength / (double)UnitLength;
  // printf("%p; %lu; %p; %p; %5.3e\n",
  //        DevMemP.weights.get(),
  //        nParticlesToUpdate,
  //        ParticlesToUpdateColumnDensities_dev,
  //        Destination_dev,
  //        ScaleFactor);

  copyOutColumnDensities<<<grid_size, block_size>>>((rt_float *)DevMemP.weights.get(),
                                                    nParticlesToUpdate,
                                                    ParticlesToUpdateColumnDensities_dev,
                                                    Destination_dev,
                                                    ScaleFactor);

  auto cerr = cudaDeviceSynchronize();
  checkCudaAPICall(cerr, __FILE__, __LINE__, true);

  return EXIT_SUCCESS;
}

int cD_CUDA_ProcessSpecies3(size_t const nParticlesTotal,
                            size_t const *const ParticlesToUpdateColumnDensities_dev,
                            double const NumberOfAtomsPerParticle[3],
                            rt_float const UnitLength,
                            size_t const nParticlesToUpdate,
                            DeviceMemoryPointers const &DevMemP,
#ifndef SAH_BVH
                            LinearBVH const &tree,
#endif
                            Weights const &sph_weights,
                            pointers3<rt_float> const QuantityToIntegrate_dev,
                            pointers3<rt_float> const Destination_dev) {
  const int grid_size = 128, block_size = 512;

#ifdef SAH_BVH
  const uint32_t *tree_map = DevMemP.tree_map.get();
#else
  const uint32_t *tree_map = tree.get_map();
#endif

  /* if q is set to a mass (kg), a mass column density is returned (kg/(units of h)^2) */
  reorderQuantityToIntegrate<<<grid_size, block_size>>>(
      QuantityToIntegrate_dev, nParticlesTotal, tree_map, (float3 *)DevMemP.q.get());

  /* Calculate on the GPU the integrals of q along the rays. */
  launchFindIntersectsKernel3(DevMemP.rays.get(),
#ifdef SAH_BVH
                              DevMemP.tree.get(),
#else
                              &tree,
#endif
                              sph_weights,
                              DevMemP.weights.get(),
                              DevMemP.q.get(),
                              nParticlesToUpdate);

  float3 ScaleFactor = make_float3(NumberOfAtomsPerParticle[0] / (double)UnitLength / (double)UnitLength,
                                   NumberOfAtomsPerParticle[1] / (double)UnitLength / (double)UnitLength,
                                   NumberOfAtomsPerParticle[2] / (double)UnitLength / (double)UnitLength);

  copyOutColumnDensities<<<grid_size, block_size>>>((float3 *)DevMemP.weights.get(),
                                                    nParticlesToUpdate,
                                                    ParticlesToUpdateColumnDensities_dev,
                                                    Destination_dev,
                                                    ScaleFactor);

  auto cerr = cudaDeviceSynchronize();
  checkCudaAPICall(cerr, __FILE__, __LINE__, true);

  return EXIT_SUCCESS;
}

struct ProfilerGuard {
  ProfilerGuard() { cudaProfilerStart(); }
  ~ProfilerGuard() { cudaProfilerStop(); }
};

int columnDensities_CUDA(size_t const *const ParticlesToUpdateColumnDensities_dev,
                         double const NumberOfHydrogenAtomsPerParticle,
                         double const NumberOfHeliumAtomsPerParticle,
                         rt_float const UnitLength,
                         size_t nRays,
                         size_t nParticles,
                         DeviceMemoryPointers const &DevMemP,
#ifndef SAH_BVH
                         const LinearBVH &tree,
#endif
                         RT_Data const Data_dev) {
#ifdef PROFILE
  ProfilerGuard profiler;
#endif
#if 1
  /* Set weights to 0 */
  cudaMemset(DevMemP.weights.get(), 0, nRays * sizeof(float));

  // static Weights sph_weights; /*  static is not thread safe */
  Weights sph_weights;

  int ierr;
  if (NumberOfHeliumAtomsPerParticle > 0.0) {
    /* If there is any Helium... */

    double NumberOfAtomsPerParticle[3];
    NumberOfAtomsPerParticle[0] = NumberOfHydrogenAtomsPerParticle;
    NumberOfAtomsPerParticle[1] = NumberOfHeliumAtomsPerParticle;
    NumberOfAtomsPerParticle[2] = NumberOfHeliumAtomsPerParticle;

    pointers3<rt_float> QuantityToIntegrate;
    QuantityToIntegrate.x = Data_dev.f_H1;
    QuantityToIntegrate.y = Data_dev.f_He1;
    QuantityToIntegrate.z = Data_dev.f_He2;

    pointers3<rt_float> Destination;
    Destination.x = Data_dev.column_H1;
    Destination.y = Data_dev.column_He1;
    Destination.z = Data_dev.column_He2;

    ierr = cD_CUDA_ProcessSpecies3(nParticles,
                                   ParticlesToUpdateColumnDensities_dev,
                                   NumberOfAtomsPerParticle,
                                   UnitLength,
                                   nRays,
                                   DevMemP,
#  ifndef SAH_BVH
                                   tree,
#  endif
                                   sph_weights,
                                   QuantityToIntegrate,
                                   Destination);

  } else {
    /* No Helium */
    ierr = cD_CUDA_ProcessSpecies(nParticles,
                                  ParticlesToUpdateColumnDensities_dev,
                                  NumberOfHydrogenAtomsPerParticle,
                                  UnitLength,
                                  nRays,
                                  DevMemP,
#  ifndef SAH_BVH
                                  tree,
#  endif
                                  sph_weights,
                                  Data_dev.f_H1,
                                  Data_dev.column_H1);
  }
  cudaDeviceSynchronize();
  if (ierr != EXIT_SUCCESS) {
    printf("ERROR: %s: %i: Unable to integrate rays.\n", __FILE__, __LINE__);
    return EXIT_FAILURE;
  }
#endif /* #if 0 */
  return EXIT_SUCCESS;
}
