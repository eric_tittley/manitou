#include <cmath>
#include <cstdlib>

#include "Manitou.h"
#include "Manitou_CUDA.h"

#ifdef SAH_BVH
#  include "D_BVH.cuh"
#else
#  include "LinearBVH.cuh"
#endif

#include "DeviceMemoryPointers.cuh"
#include "cudasupport.h"

namespace {
#ifdef SAH_BVH
using SphereType = cudaTextureObject_t;

__device__ float3 getSpherePos(SphereType spheres, uint32_t isphere) {
  const float4 sphere_data = tex1Dfetch<float4>(spheres, isphere);
  return make_float3(sphere_data.x, sphere_data.y, sphere_data.z);
}

__host__ SphereType getSpheres(const TreeType &tree) {
  cudaTextureObject_t spheres;
  ERT_CUDA_CHECK(cudaMemcpy(&spheres, &tree.spheres, sizeof(spheres), cudaMemcpyDeviceToHost));
  return spheres;
}
#else
using SphereType = const LinearBVH::Sphere *;

__device__ float3 getSpherePos(SphereType spheres, uint32_t isphere) { return spheres[isphere].pos; }

__host__ SphereType getSpheres(const TreeType &tree) { return tree.get_spheres(); }
#endif
}  // namespace

/* Spherical wave source. */
__global__ void setSWSTPD_kernel(SphereType spheres,
                                 uint32_t const num_spheres,
                                 uint32_t const *const map,
                                 float const r0,
                                 float const r1,
                                 float const r2,
                                 rt_float const UnitLength,
                                 rt_float *const R) {
  size_t const istart = blockIdx.x * blockDim.x + threadIdx.x;
  size_t const span = gridDim.x * blockDim.x;  // nBlocks * nThreadsPerBlock

  for (size_t iParticle = istart; iParticle < num_spheres; iParticle += span) {
    /* Set the distance from the source to each particle */
    /* The distance to the source [m] */
    float3 sphereData = getSpherePos(spheres, iParticle);
    R[map[iParticle]] =
        (rt_float)sqrtf((sphereData.x - r0) * (sphereData.x - r0) + (sphereData.y - r1) * (sphereData.y - r1) +
                        (sphereData.z - r2) * (sphereData.z - r2)) *
        UnitLength;
  }
}

/* Plane wave source. */
__global__ void setPWSTPD_kernel(SphereType spheres,
                                 uint32_t const num_spheres,
                                 uint32_t const *const map,
                                 int const dir0,
                                 int const dir1,
                                 int const dir2,
                                 rt_float const BoxSize,
                                 rt_float const UnitLength,
                                 rt_float *const R) {
  size_t const istart = blockIdx.x * blockDim.x + threadIdx.x;
  size_t const span = gridDim.x * blockDim.x;  // nBlocks * nThreadsPerBlock

  /* Set the distance from the source to each particle */

  if (dir0 != 0) {
    /* Rays along x axis. +ve x means origin at x = 0 plane; -ve x means origin
     * at x = BoxSize plane.
     */
    float origin_x = dir0 > 0 ? 0. : BoxSize;
    for (size_t iParticle = istart; iParticle < num_spheres; iParticle += span) {
      float3 sphereData = getSpherePos(spheres, iParticle);
      R[map[iParticle]] = (rt_float)(abs(sphereData.x - origin_x) * UnitLength);
    }
  } else if (dir1 != 0) {
    /* Rays along y axis. +ve y means origin at y = 0 plane; -ve y means origin
     * at y = BoxSize plane.
     */
    float origin_y = dir1 > 0 ? 0. : BoxSize;
    for (size_t iParticle = istart; iParticle < num_spheres; iParticle += span) {
      float3 sphereData = getSpherePos(spheres, iParticle);
      R[map[iParticle]] = (rt_float)(abs(sphereData.y - origin_y) * UnitLength);
    }
  } else if (dir2 != 0) {
    /* Rays along z axis. +ve z means origin at z = 0 plane; -ve z means origin
     * at z = BoxSize plane.
     */
    float origin_z = dir2 > 0 ? 0. : BoxSize;
    for (size_t iParticle = istart; iParticle < num_spheres; iParticle += span) {
      float3 sphereData = getSpherePos(spheres, iParticle);
      R[map[iParticle]] = (rt_float)(abs(sphereData.z - origin_z) * UnitLength);
    }
  }
}

void setSourceToParticleDistance(const TreeType &tree,
                                 uint32_t const *tree_map,
                                 RT_SourceT const &Source,
                                 /* Same units as h_particles. */
                                 rt_float const BoxSize,
                                 rt_float const UnitLength,
                                 RT_Data Data_dev) {
  /* Create a new variable with a copy of the contents pointed to by the pointer. */

  /* Good time to remind ourselves that:
   *  DevMemP (DeviceMemoryPointers *) has as a
   *  member tree (BVH *)
   *  tree has as a member spheres (Spheres)
   *  spheres has as members:
   *   cx (std::vector<float>)
   *   cy (std::vector<float>)
   *   cz (std::vector<float>)
   *   h  (std::vector<float>)
   *  The pointer, tree *, points to Device Memory. So the host does not have
   *  access to tree->spheres. Hence we need to pass tree * to the kernel.
   */

  int gridSize;   // The actual grid size needed, based on input size
  int blockSize;  // The launch configurator returned block size

  bool const TerminateOnCudaError = true;

  if (Source.AttenuationGeometry == SphericalWave || Source.AttenuationGeometry == RaySphericalWave) {
    setGridAndBlockSize(Data_dev.NumCells, (void *)setSWSTPD_kernel, &gridSize, &blockSize);
    setSWSTPD_kernel<<<gridSize, blockSize>>>(getSpheres(tree),
                                              Data_dev.NumCells,
                                              tree_map,
                                              (float)Source.r[0],
                                              (float)Source.r[1],
                                              (float)Source.r[2],
                                              UnitLength,
                                              Data_dev.R);
    checkCudaKernelLaunch(__FILE__, __LINE__, TerminateOnCudaError);
  } else if (Source.AttenuationGeometry == PlaneWave || Source.AttenuationGeometry == RayPlaneWave) {
    setGridAndBlockSize(Data_dev.NumCells, (void *)setPWSTPD_kernel, &gridSize, &blockSize);
    setPWSTPD_kernel<<<gridSize, blockSize>>>(getSpheres(tree),
                                              Data_dev.NumCells,
                                              tree_map,
                                              (int)Source.r[0],
                                              (int)Source.r[1],
                                              (int)Source.r[2],
                                              BoxSize,
                                              UnitLength,
                                              Data_dev.R);
    checkCudaKernelLaunch(__FILE__, __LINE__, TerminateOnCudaError);
  }

  auto cerr = cudaDeviceSynchronize();
  checkCudaAPICall(cerr, __FILE__, __LINE__, TerminateOnCudaError);
}
