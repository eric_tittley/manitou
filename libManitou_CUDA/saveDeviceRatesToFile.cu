#include "Manitou.h"

extern "C" {
/* RT includes */
#include "RT.h"
}
#include <thrust/host_vector.h>

#include "RT_CUDA.h"
#include "cudasupport.h"

int saveDeviceRatesToFile(RT_Data const &Data_host,
                          RT_RatesBlockT Rates_dev,
                          rt_float t,
                          rt_float outputTimesUnit,
                          char *Dir) {
  /* * Save Rates to file * */

  size_t const LABEL_STR_SIZE = 32;
  char RTFile[LABEL_STR_SIZE];
  int ierr;

  /*   Allocate host memory for Rates   */
  RT_RatesBlockT Rates_host;
  Rates_host.NumCells = Data_host.NumCells;
  ierr = RT_RatesBlock_Allocate(Data_host.NumCells, &Rates_host);
  if (ierr != EXIT_SUCCESS) {
    printf("ERROR: %s: %i: Failed to allocate host memory for Rates\n", __FILE__, __LINE__);
    return EXIT_FAILURE;
  }

  /*   Copy Rates from device to host   */
  RT_CUDA_CopyRatesDeviceToHost(Rates_dev, Rates_host);

  /*   Write Rates to file              */
  rt_float const t_outputTimeUnits = t / outputTimesUnit;
  ierr = snprintf(RTFile, LABEL_STR_SIZE - 1, "%s/Rates_t=%07.3f", Dir, t_outputTimeUnits);
  if (ierr > (int)LABEL_STR_SIZE) {
    printf("ERROR: %s: %i: Path too long %s\n", __FILE__, __LINE__, RTFile);
    return EXIT_FAILURE;
  }
  FILE *fid = fopen(RTFile, "w");
  if (fid == NULL) {
    printf("ERROR: %s: %i: Unable to open file %s\n", __FILE__, __LINE__, RTFile);
    ReportFileError(RTFile);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
  ierr = RT_RatesBlock_Write(Rates_host, fid);
  if (ierr != EXIT_SUCCESS) {
    printf("ERROR: %s: %i: Failed to write Rates to file %s\n", __FILE__, __LINE__, RTFile);
    return EXIT_FAILURE;
  }

  /* Close the file */
  ierr = fclose(fid);
  if (ierr != 0) {
    printf("ERROR: %s: Unable to close file %s\n", __FILE__, RTFile);
    ReportFileError(RTFile);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }

  /*   Free local Rates memory          */
  RT_RatesBlock_Free(Rates_host);

  return EXIT_SUCCESS;
}