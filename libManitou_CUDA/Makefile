ifndef MAKE_CONFIG
 MAKE_CONFIG = ../make.config
endif
include $(MAKE_CONFIG)
# Expected definitions:
#  ROOT_DIR
#  INC_RT, INC_RT_CUDA, INC_RTSPH, INC_CUDA

INCS = -I$(ROOT_DIR) \
         $(INC_RT) \
         $(INC_RT_CUDA) \
         $(INC_RTSPH) \
         $(INC_CUDA) \
         $(INC_NCCL) \
         $(INC_LIBCUDASUPPORT) \
         $(INC_LIBMANIDOOWAG)

ifeq ($(GIZMO_BINDINGS),1)
 INCS += $(INC_GIZMO) \
         $(INC_MPI) \
         $(INC_HDF5)
endif

SRC_CPP = \
	copyColumnDensitiesToDevice.cpp \
	copyRTDataFromDeviceToHost.cpp \
	copyRTDataFromHostToDevice.cpp \

SRC_CUDA = calculateSourceParticlePairRates_CUDA.cu \
           columnDensities_CUDA.cu \
           initializeRtsphCuda.cu \
           resetEntropyAndTemperature_CUDA.cu \
           saveDeviceRatesToFile.cu \
           setSourceToParticleDistance.cu \
           generateRays.cu

ifeq ($(GIZMO_BINDINGS),1)
SRC_CUDA += gizmoStartup.cu \
            gizmoRun.cu \
            gizmoSnapshot.cu \
            gizmoSetSources.cu
SRC_CPP += dynamicSources.cpp

endif

OBJS_CPP = $(SRC_CPP:.cpp=.o)
OBJS_CUDA = $(SRC_CUDA:.cu=.o)
OBJS = $(OBJS_CPP) $(OBJS_CUDA)
LIBRARY   = libManitou_CUDA.a

LIB_RT_CUDA = $(ROOT_DIR)/RT_CUDA/libRT_CUDA.a
LIB_MANIDOOWAG = $(ROOT_DIR)/libManidoowag/libManidoowag.a

all: $(LIBRARY)

$(LIBRARY): $(OBJS) link.o
	$(AR) $(AR_FLAGS) r $(LIBRARY) $(OBJS) link.o

link.o: $(OBJS_CUDA)
	-rm -fr tmp
	mkdir tmp
	cd tmp; $(AR) $(AR_FLAGS) x $(LIB_RT_CUDA); $(AR) $(AR_FLAGS) x $(LIB_MANIDOOWAG); rm link.o
	$(NVCC) $(NVCC_ARCH) -dlink -o link.o $(OBJS_CUDA) tmp/*.o
	rm -fr tmp

clean:
	-rm -f *.o
	-rm -f *~

distclean: clean
	-rm $(LIBRARY)

rebuild: distclean $(LIBRARY)

indent:
	indent *.cpp
	indent *.cu

.SUFFIXES: .o .c .cpp .cu

.c.o:
	$(CC) $(CFLAGS) $(DEFS) $(INCS) -c $<

.cpp.o:
	$(CXX) $(CXXFLAGS) $(DEFS) $(INCS) -c $<

.cu.o:
	$(NVCC) $(NVCCFLAGS) $(DEFS) $(INCS) -dc $<
