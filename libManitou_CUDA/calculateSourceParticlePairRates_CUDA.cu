#include "Manitou_CUDA.h"
#include "RT_CUDA.h"
#include "cudasupport.h"

// Shared memory for IntegralInvolatiles
extern __shared__ rt_float shared[];

static __device__ RT_IntegralInvolatilesT copyInvolatilesToSM(RT_IntegralInvolatilesT const Involatiles) {
  RT_IntegralInvolatilesT Involatiles_SM;
  Involatiles_SM.nFrequencies = Involatiles.nFrequencies;
  Involatiles_SM.nSpecies = Involatiles.nSpecies;
  Involatiles_SM.Frequencies = &shared[0 * Involatiles.nFrequencies];
  Involatiles_SM.Weights = &shared[1 * Involatiles.nFrequencies];
  Involatiles_SM.Luminosities = &shared[2 * Involatiles.nFrequencies];
  // NB: Alphas is nSpecies * nFrequencies, so we use (3+nSpecies) *
  // nFrequencies total SM
  Involatiles_SM.Alphas = &shared[3 * Involatiles.nFrequencies];

  for (int i = threadIdx.x; i < Involatiles.nFrequencies; i += blockDim.x) {
    Involatiles_SM.Frequencies[i] = Involatiles.Frequencies[i];
    Involatiles_SM.Weights[i] = Involatiles.Weights[i];
    Involatiles_SM.Luminosities[i] = Involatiles.Luminosities[i];
    for (int j = 0; j < Involatiles.nSpecies; ++j) {
      const auto idx = i * Involatiles.nSpecies + j;
      Involatiles_SM.Alphas[idx] = Involatiles.Alphas[idx];
    }
  }
  __syncthreads();
  return Involatiles_SM;
}

__global__ void cSppR_kernel(RT_Data const Data,
                             rt_float const ExpansionFactor,
                             RT_SourceT Source,
                             RT_ConstantsT const Constants,
                             RT_IntegralInvolatilesT const Involatiles,
                             /* out */
                             RT_RatesBlockT Rates) {
  size_t const nParticles = Data.NumCells;

  size_t const istart = blockIdx.x * blockDim.x + threadIdx.x;
  size_t const span = gridDim.x * blockDim.x;  // nBlocks * nThreadsPerBlock

  RT_IntegralInvolatilesT const Involatiles_SM = copyInvolatilesToSM(Involatiles);

  for (size_t cellindex = istart; cellindex < nParticles; cellindex += span) {
    /* The species number densities. m^-3 */
    rt_float n_H1 = Data.n_H[cellindex] * Data.f_H1[cellindex];
    rt_float n_He1 = Data.n_He[cellindex] * Data.f_He1[cellindex];
    rt_float n_He2 = Data.n_He[cellindex] * Data.f_He2[cellindex];

    /* The column density through this particle (number density)*(size)  m^-2 */
    rt_float meanPathLengthThroughParticle = rt_float(4. / 3.) * Data.dR[cellindex];
    rt_float N_H1 = n_H1 * meanPathLengthThroughParticle;
    rt_float N_He1 = n_He1 * meanPathLengthThroughParticle;
    rt_float N_He2 = n_He2 * meanPathLengthThroughParticle;

    rt_float const DistanceFromSourceMpc = Data.R[cellindex] / Constants.Mpc;
    rt_float const ParticleRadiusMpc = Data.dR[cellindex] / Constants.Mpc; /* Not used */
#if 1
    rt_float const DistanceThroughElementMpc = meanPathLengthThroughParticle / Constants.Mpc;
#else
    rt_float DistanceThroughElementMpc;
    /* The first is the correct long-field form. The corrections that follow are justified
     * by handwaving and fitting I(r) curves, but are poorly justified. */
    if (DistanceFromSourceMpc >= ParticleRadiusMpc) {
      DistanceThroughElementMpc = meanPathLengthThroughParticle / Constants.Mpc;
    } else {
      DistanceThroughElementMpc = ParticleRadiusMpc - RT_FLT_C(0.3) * DistanceFromSourceMpc;
    }
#endif

    if (DistanceFromSourceMpc <= NPP_MINABS_32F) {
      printf("%s: %i: DistanceFromSourceMpc = %f\n", __FILE__, __LINE__, DistanceFromSourceMpc);
    }
    /* The contributions to the photoionization rates for this source */
    auto Rates_local = RT_CUDA_PhotoionizationRates(N_H1,
                                                    N_He1,
                                                    N_He2,
                                                    Data.column_H1[cellindex],
                                                    Data.column_He1[cellindex],
                                                    Data.column_He2[cellindex],
                                                    ExpansionFactor,
                                                    &Constants,
                                                    &Source,
                                                    DistanceFromSourceMpc,
                                                    ParticleRadiusMpc, /* Not used */
                                                    DistanceThroughElementMpc,
                                                    &Involatiles_SM);

    Rates.I_H1[cellindex] += Rates_local.I_H1;
    Rates.I_He1[cellindex] += Rates_local.I_He1;
    Rates.I_He2[cellindex] += Rates_local.I_He2;
    Rates.G_H1[cellindex] += Rates_local.G_H1;
    Rates.G_He1[cellindex] += Rates_local.G_He1;
    Rates.G_He2[cellindex] += Rates_local.G_He2;
    Rates.Gamma_HI[cellindex] += Rates_local.Gamma_HI;
  }
}

__global__ void setRecalculateColumnDensityFlag(RT_Data const Data,
                                                ParametersT const Parameters,
                                                /*    Updated */ unsigned int *const RecalculateColumnDensityFlag) {
  size_t const nParticles = Data.NumCells;

  size_t const istart = blockIdx.x * blockDim.x + threadIdx.x;
  size_t const span = gridDim.x * blockDim.x;  // nBlocks * nThreadsPerBlock

  for (size_t cellindex = istart; cellindex < nParticles; cellindex += span) {
    if (Data.column_H1[cellindex] < Parameters.columnDensityThreshold_H1 &&
        Data.column_He1[cellindex] < Parameters.columnDensityThreshold_He1 &&
        Data.column_He2[cellindex] < Parameters.columnDensityThreshold_He2) {
      /* Always recalculate column densities in this regime.
       * The 1 is a marker that it must be held. */
      RecalculateColumnDensityFlag[cellindex] = Parameters.RecalculateColumnDensityIterationCount + 1;
    }
  }
}

void calculateSourceParticlePairRates_CUDA(RT_Data const Data_dev,
                                           ParametersT const Parameters,
                                           RT_ConstantsT const Constants_dev,
                                           rt_float const ExpansionFactor,
                                           RT_SourceT Source,
                                           RT_IntegralInvolatilesT const Involatiles_dev,
                                           /* Incremented */ RT_RatesBlockT Rates_dev,
                                           /*    Updated */ unsigned int *const RecalculateColumnDensityFlag) {
  int gridSize;   // The actual grid size needed, based on input size
  int blockSize;  // The launch configurator returned block size
  setGridAndBlockSize(Data_dev.NumCells, (void *)cSppR_kernel, &gridSize, &blockSize);

#ifdef DEBUG
  RT_CUDA_RT_Data_CheckData(Data_dev, __FILE__, __LINE__);
#endif

  // Overlap RecalculateColumnDensity copies with the rates kernel
  cudaStream_t spprStream, flagStream;
  ERT_CUDA_CHECK(cudaStreamCreate(&spprStream));
  ERT_CUDA_CHECK(cudaStreamCreate(&flagStream));

  size_t const NumBytes = sizeof(unsigned int) * Data_dev.NumCells;
  unsigned int *RecalculateColumnDensityFlag_dev;
  ERT_CUDA_CHECK(cudaMalloc((void **)&RecalculateColumnDensityFlag_dev, NumBytes));

  const auto memSize = (3 + Involatiles_dev.nSpecies) * Involatiles_dev.nFrequencies * sizeof(rt_float);
  cSppR_kernel<<<gridSize, blockSize, memSize, spprStream>>>(Data_dev,
                                                             ExpansionFactor,
                                                             Source,
                                                             Constants_dev,
                                                             Involatiles_dev,
                                                             /* out */
                                                             Rates_dev);
#if 0
 /* Update RecalculateColumnDensityFlag */
 ERT_CUDA_CHECK(cudaMemcpyAsync(RecalculateColumnDensityFlag_dev,
                                RecalculateColumnDensityFlag, NumBytes,
                                cudaMemcpyHostToDevice, flagStream));

 setGridAndBlockSize(Data_dev.NumCells,
             (void *)setRecalculateColumnDensityFlag,
                    &gridSize,
                    &blockSize);
 setRecalculateColumnDensityFlag<<<gridSize, blockSize, 0, flagStream>>>
                               (Data_dev,
                                Parameters,
              /*    Updated */  RecalculateColumnDensityFlag_dev);

 ERT_CUDA_CHECK(cudaMemcpyAsync(RecalculateColumnDensityFlag,
                                RecalculateColumnDensityFlag_dev, NumBytes,
                                cudaMemcpyDeviceToHost, flagStream));

#endif /* #if 0 */
  ERT_CUDA_CHECK(cudaStreamDestroy(flagStream));
  ERT_CUDA_CHECK(cudaStreamDestroy(spprStream));
  ERT_CUDA_CHECK(cudaFree(RecalculateColumnDensityFlag_dev));
  ERT_CUDA_CHECK(cudaDeviceSynchronize());
}
