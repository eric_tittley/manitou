#include <cuda_runtime.h>

#include <cassert>

#include "Manitou_CUDA.h"
#include "cudasupport.h"

static void copyColumnDensities(RT_Data const Data_to, RT_Data const Data_from, cudaMemcpyKind Kind) {
  assert(Data_from.NumCells == Data_to.NumCells);
  size_t const NumBytes = sizeof(rt_float) * Data_to.NumCells;

  ERT_CUDA_CHECK(cudaMemcpy(Data_to.column_H1, Data_from.column_H1, NumBytes, Kind));
  ERT_CUDA_CHECK(cudaMemcpy(Data_to.column_He1, Data_from.column_He1, NumBytes, Kind));
  ERT_CUDA_CHECK(cudaMemcpy(Data_to.column_He2, Data_from.column_He2, NumBytes, Kind));
}

void copyColumnDensitiesToDevice(RT_Data const Data_host, RT_Data const Data_dev) {
  copyColumnDensities(Data_dev, Data_host, cudaMemcpyHostToDevice);
}

void copyColumnDensitiesFromDevice(RT_Data const Data_host, RT_Data const Data_dev) {
  copyColumnDensities(Data_host, Data_dev, cudaMemcpyDeviceToHost);
}
