#pragma once

extern "C" {
#include "RT_Precision.h"
#include "RT_SourceT.h"
}

#include <allvars.h>

#include <vector>

#include "Vec3.h"

struct DynamicSources {
  struct SourceInfo {
    float3 initial_position, velocity;
  };

  std::vector<SourceInfo> particles;
  integertime epoch;
};

/** Update RT source positions from dynamic sources */
void updateSourcePositions(const DynamicSources &dyn_sources, std::vector<RT_SourceT> &rt_sources, integertime time);
