#include "Manidoowag.h"
#include "Manitou.h"
#include "Manitou_CUDA.h"
#include "ERT_gizmo.h"
#include "RT_CUDA.h"
#include "Vec3.h"
#include "cudaMemory.h"
#include "cudasupport.h"
#ifdef __cplusplus
extern "C" {
#endif
#include "RT_FileHeaderT.h"
#ifdef __cplusplus
}
#endif

#ifndef SAH_BVH
#  include "LinearBVH.cuh"
#  include "Vec3.h"
#endif

/* RT includes */
#include "RT.h"
#include "RT_Data.h"
#include "RT_Luminosity.h"
#include "allvars.h"
#include "manitou_vars.h"
extern "C" {
#include "proto.h"
}

// NCCL Header file
#include "nccl.h"

// OMP Header file
#include <stdlib.h>

#include <atomic>
#include <memory>

#include "NCCLCHECK.h"
#include "gpuErrchk.h"
#include "omp.h"

/* Why do these need to be global?
 * Because ert_write_snapshot() doesn't take any arguments.
 * If we checked that it was time to dump data here,
 * then it wouldn't need to be */
RT_Data *_Data_p;
RT_Data _Data;

void reportMemUsage();

std::vector<size_t> find_particles_to_update() {
printf("Got to find_particles_to_update\n");
  const size_t nParticles = (size_t)All.TotN_gas;
  size_t NumToUpdate = 0;

#pragma omp parallel for reduction(+ : NumToUpdate)
  for (size_t iParticle = 0; iParticle < nParticles; ++iParticle) {
    if (_RecalculateColumnDensityFlag[iParticle] >= _Parameters.RecalculateColumnDensityIterationCount) {
      NumToUpdate++;
    }
  }

  std::vector<size_t> ToUpdate(NumToUpdate);
  std::atomic<size_t> Head(0);
#pragma omp parallel for
  for (size_t iParticle = 0; iParticle < nParticles; ++iParticle) {
    if (_RecalculateColumnDensityFlag[iParticle] >= _Parameters.RecalculateColumnDensityIterationCount) {
      ToUpdate[Head++] = iParticle;
      //_RecalculateColumnDensityFlag[iParticle]=0;
    } else {
      _RecalculateColumnDensityFlag[iParticle]++;
    }
  }
printf("Returning from find_particles_to_update\n");
  return ToUpdate;
}

// Number of RT steps we aim to perform for each tree built
// Increase to amortise the expensive tree build times at the cost of accuracy
// Note: Just a rough estimate based on the previous timestep, not a fixed step size!
#ifndef MANITOU_MULTISTEP
#  define MANITOU_MULTISTEP 4
#endif

static float last_dt_RT = 0.;

static bool should_skip_update() {
  const auto dt_code = (((integertime)1 << All.HighestActiveTimeBin)) * All.Timebase_interval;
  const auto dt_phys = dt_code * UNIT_TIME_IN_CGS;

  const auto est_timesteps = std::ceil(dt_phys / last_dt_RT);
  bool skip = !Flag_FullStep && (est_timesteps * 1.5 < MANITOU_MULTISTEP);
  if (skip) {
    printf("Manitou: skipping GIZMO timestep of estimated %ld RT steps, aiming for %ld\n",
           std::lround(est_timesteps),
           static_cast<long>(MANITOU_MULTISTEP));
  }
  return skip;
}

void ert_parent_routine() {
  printf("Got to ert_parent_routine [Manitou]...\n");
  if (should_skip_update()) {
    return;
  }

  checkCUDAErrors("Failed before entering manitou", __FILE__, __LINE__);

#ifdef MANITOU_FOF_SOURCES
  int const nSources = _Source.size();
#else
  int const nSources = _Parameters.nSources;
#endif
  if (nSources == 0) {
    printf("No sources.  Returning from radiative transfer\n");
    return;
  }
  printf("Computing radiative transfer from %i sources.\n", nSources);

  // Longest possible gizmo timestep (seconds at current redshift)
  const rt_float max_timestep = TIMEBASE * All.Timebase_interval * UNIT_TIME_IN_CGS;

  int ierr;

  double tStart = getTimeInSeconds();

  double CheckPointTime;
  double TimerArray[nCodeTimes];
  for (size_t i = 0; i < nCodeTimes; ++i) {
    TimerArray[i] = 0.0;
  }

  /* Increase the stack size per thread */
  size_t pValue;
  bool const TerminateOnCudaError = true;
  cudaError_t cudaError = cudaDeviceGetLimit(&pValue, cudaLimitStackSize);
  checkCudaAPICall(cudaError, __FILE__, __LINE__, TerminateOnCudaError);
  printf("Current LimitStackSize=%lu. Setting to 2048 bytes\n", pValue);
  cudaError = cudaDeviceSetLimit(cudaLimitStackSize, 2048);
  checkCudaAPICall(cudaError, __FILE__, __LINE__, TerminateOnCudaError);

#ifdef PROGRESSIVE_MaxSizeTimestep
  printf("Increasing MaxSizeTimestep from %5.3e to %5.3e\n",
         All.MaxSizeTimestep,
         All.MaxSizeTimestep * PROGRESSIVE_MaxSizeTimestep);
  All.MaxSizeTimestep *= PROGRESSIVE_MaxSizeTimestep;
#endif

  rt_float UnitLength = UNIT_LENGTH_IN_CGS / 100;
  double UnitMass = UNIT_MASS_IN_CGS / 1000;
  if (All.ComovingIntegrationOn) {
    /* Scale by the expansion factor */
    UnitLength *= All.Time;
  }
  rt_float GizmoDensityUnit =
      (rt_float)((UnitMass / (double)UnitLength) / (double)UnitLength) / (double)UnitLength; /* kg/m^3) */

  double GasParticleMass = P[0].Mass * UnitMass; /* kg */
  const rt_float density_to_n_H = (1.0 - _Constants.Y) / (_Constants.mass_p + _Constants.mass_e) * GizmoDensityUnit;
  const rt_float density_to_n_He = 0.25 * _Constants.Y / (_Constants.mass_p + _Constants.mass_e) * GizmoDensityUnit;

  /* NumberOfHydrogenAtomsPerParticle ~ 10^(40 - (-27)) = 10^-67 so need double */
  double NumberOfHydrogenAtomsPerParticle =
      GasParticleMass * (double)(1.0 - _Constants.Y) / (double)(_Constants.mass_p + _Constants.mass_e);
  double NumberOfHeliumAtomsPerParticle =
      GasParticleMass * (double)_Constants.Y / (double)(4.0 * (_Constants.mass_p + _Constants.mass_e));
  printf("done.\n");

  size_t const nParticles = (size_t)All.TotN_gas;

  /* Loading particle data into format required to build tree */
#ifdef SAH_BVH
  Spheres spheres(nParticles);
#else
  std::vector<LinearBVH::Sphere> spheres(nParticles);
#endif
  CheckPointTime = getTimeInSeconds();
  {
    // Some of the first N_gas particles may be non-gas particles
    // e.g. stars converted by GALSF
    size_t iGas = 0;
    for (size_t iParticle = 0; iParticle < N_gas; ++iParticle) {
      if (P[iParticle].Type == 0) {
#ifdef SAH_BVH
        spheres.cx[iGas] = P[iParticle].Pos[0];
        spheres.cy[iGas] = P[iParticle].Pos[1];
        spheres.cz[iGas] = P[iParticle].Pos[2];
        spheres.h[iGas] = P[iParticle].Hsml;
        spheres.invh2[iGas] = 1.0 / (P[iParticle].Hsml * P[iParticle].Hsml);
#else
        auto *Pos = P[iParticle].Pos;
        spheres[iGas].pos = make_float3(Pos[0], Pos[1], Pos[2]);
        spheres[iGas].radius = P[iParticle].Hsml;
#endif
        ++iGas;
      }
    }
    // Sanity check
    if (iGas != nParticles) {
      printf("iGas=%lu; nParticles=%lu\n", iGas, nParticles);
      gizmo_terminate("Manitou: Unexpected number of gas particles, All.TotN_gas invalid");
    }
  }
  TimerArray[CreatingSpheres] += getTimeInSeconds() - CheckPointTime;
  CheckPointTime = getTimeInSeconds();

  // Count up the GPU devices
  int devCount = 0;
  cudaGetDeviceCount(&devCount);
  int nDev = devCount;
  if (nDev > nSources) {
    nDev = nSources;
  }

  // Device Numbering Loop
  int *devs = (int *)malloc(sizeof(int) * nDev);
  for (int i = 0; i < nDev; i++) {
    devs[i] = i;
  }
  printf("GPU devices numbered successfully, nDev = %i\n", nDev);

  // Initialize NCCL communicator and individual device streams
  ncclComm_t comms[nDev];
  cudaStream_t *s;
  if (nDev > 1) {
    // malloc for all
    s = (cudaStream_t *)malloc(sizeof(cudaStream_t) * nDev);

    // individual streams
    printf("Creating Streams...");
    for (int i = 0; i < nDev; i++) {
      cudaSetDevice(devs[i]);
      cudaStreamCreate(s + i);
      gpuErrchk(cudaPeekAtLastError());
    }
    printf("done.\n");

    // initialize NCCL
    printf("Initializing NCCL: ncclCommInitAll\n");
    NCCLCHECK(ncclCommInitAll(comms, nDev, devs));

    // synchronizing on CUDA streams to wait for completion of NCCL operation
    for (int i = 0; i < nDev; ++i) {
      cudaSetDevice(devs[i]);
      cudaStreamSynchronize(s[i]);
    }

  } /* End if more than one device */

  // Build Tree
#ifdef SAH_BVH
  BVH tree;
  mkSAHBVH(spheres, tree);
#else
  // the tree_list is made as a LinearBVH * pointer
  std::vector<LinearBVH *> tree_list;
  for (int i = 0; i < nDev; i++) {
    cudaSetDevice(devs[i]);
    cudaDeviceSynchronize();
    /* the next line creates the tree from the spheres
     * using the "new" constructor is important as it means the compiler will not
     * destroy it, so it can still exist outside the scope of this for loop
     */
    // extra print statements are for debugging
    LinearBVH *tree = new LinearBVH(spheres.size());
    printf("*tree made as new LinearBVH on device %d...", devs[i]);
    LinearBVH::construct_from_host_spheres(*tree, spheres.data(), spheres.size());
    printf("host constructed from spheres on device %d...", devs[i]);
    tree_list.push_back(tree);
  }
  /* auto tree = LinearBVH::construct_from_host_spheres(spheres.data(), nParticles); */
#endif
  printf("done.\n");

  /* Create the reverse map, to index into the RTSPH order from the input
   * (Gadget) order.
   */
#ifdef SAH_BVH
  const uint32_t *tree_map = tree.map.data();
#else
  std::unique_ptr<uint32_t[]> tree_map_storage(new uint32_t[nParticles]);
  ERT_CUDA_CHECK(cudaMemcpy(
      tree_map_storage.get(), tree_list[0]->get_map(), sizeof(uint32_t) * nParticles, cudaMemcpyDeviceToHost));
  /*
   * ERT_CUDA_CHECK(
   *   cudaMemcpy(tree_map_storage.get(), tree.get_map(), sizeof(uint32_t) * nParticles, cudaMemcpyDeviceToHost));
   */
  const uint32_t *tree_map = tree_map_storage.get();
#endif
  std::unique_ptr<unsigned int[]> MapGadgetToRTSPH(new unsigned int[nParticles]);
  for (unsigned int iParticle = 0; iParticle < nParticles; ++iParticle) {
    MapGadgetToRTSPH[tree_map[iParticle]] = iParticle;
  }

  checkCUDAErrors("Failed before tree copy", __FILE__, __LINE__);
#ifdef SAH_BVH
  DeviceMemoryPointers DevMemP = initializeRtsphCuda(nParticles, nParticles, &tree);
#endif
  cudaDeviceSynchronize();
  checkCUDAErrors("Tree copy failed", __FILE__, __LINE__);
  printf("Copied new tree to device.\n");
  (void)fflush(stdout);

  TimerArray[CreatingTree] = getTimeInSeconds() - CheckPointTime;

  CheckPointTime = getTimeInSeconds();

  // Allocate memory for RT_Data
  /* It might be beneficial if nParticles be a multiple of 16 (512 bytes) so
   * the arrays are aligned on 512 byte boundaries on the GPU */
  printf("Allocate RT_Data for %lu particles...",nParticles);
  _Data_p = RT_Data_Allocate(nParticles);
  _Data = *_Data_p;
  _Data.NumCells = nParticles;
  printf("done.\n");

  // Pin _Data to improve cudaMemcpy throughput
  PinMemoryGuard pin(_Data.MemSpace, _Data.NBytes);

  printf("Extract Gizmo data to RT_Data\n");
#pragma omp parallel for shared(N_gas,P,SphP)
  for (size_t i = 0; i < N_gas; ++i) {
    if (P[i].Type != 0) continue;
    if (std::abs(SphP[i].HI + SphP[i].HII - 1.) > 1e-5) {
      printf("SphP[%lu].HI=%5.3e; HII=%5.3e  Renormalizing\n",i,SphP[i].HI,SphP[i].HII);
      //gizmo_terminate("Manitou: Hydrogen species fractions not normalised");
      /* Force a normalization.
       * Note, this is should always be Big = Big - Small. Avoid Small = Big - Big, which will just 
       * create numerical artefacts.  However, it seems on restarts we don't have HII. */
      SphP[i].HII = 1.0 - SphP[i].HI;
    }
  }
  for (size_t i = 0; i < N_gas; ++i) {
    if (P[i].Type != 0) continue;
    if (std::abs(SphP[i].HeI + SphP[i].HeII + SphP[i].HeIII - 1.) > 1e-5) {
      printf("SphP[%lu].HeI=%5.3e; HeII=%5.3e; HeIII=%5.3e  Renormalizing\n",i,SphP[i].HeI,SphP[i].HeII,SphP[i].HeIII);
      //gizmo_terminate("Manitou: Helium species fractions not normalised");
      /* Force a normalization.
       * Note, this is should always be Big = Big - Small. Avoid Small = Big - Big, which will just 
       * create numerical artefacts.  However, it seems on restarts we don't have HeIII. */
      SphP[i].HeIII = 1.0 - SphP[i].HeI - SphP[i].HeII;
    }
  }

  for (size_t iParticle = 0, iGas = 0; iParticle < N_gas; iParticle++) {
    if (P[iParticle].Type != 0) {
      continue;
    }
    // update fractions first (needed for entropy calculations)
    _Data.f_H1[iGas] = (rt_float)SphP[iParticle].HI;
    _Data.f_H2[iGas] = (rt_float)SphP[iParticle].HII;
    _Data.f_He1[iGas] = (rt_float)SphP[iParticle].HeI;
    _Data.f_He2[iGas] = (rt_float)SphP[iParticle].HeII;
    _Data.f_He3[iGas] = (rt_float)SphP[iParticle].HeIII;

    _Data.dR[iGas] = UnitLength * P[iParticle].Hsml / P[iParticle].NumNgb; /* m */             // dR
    _Data.Density[iGas] = GizmoDensityUnit * (rt_float)SphP[iParticle].Density; /* kg m^-3 */  // density
    _Data.n_H[iGas] = density_to_n_H * (rt_float)SphP[iParticle].Density; /* m^-3 */           // n_H
    _Data.n_He[iGas] = density_to_n_He * (rt_float)SphP[iParticle].Density; /* m^-3 */         // n_He

    /* Update the neutral & ionized number densities. */
    rt_float const n_H2 = _Data.n_H[iGas] * _Data.f_H2[iGas];
    rt_float const n_He2 = _Data.n_He[iGas] * _Data.f_He2[iGas];
    rt_float const n_He3 = _Data.n_He[iGas] * _Data.f_He3[iGas];

    /* Electron density */
    rt_float const n_e = n_H2 + n_He2 + (2. * n_He3);

    /* Total number density */
    rt_float const n = _Data.n_H[iGas] + _Data.n_He[iGas] + n_e;

    rt_float const mu = (_Data.n_H[iGas] + 4. * _Data.n_He[iGas]) / n;

    rt_float GadgetUtoTFactor = 1.e6 * (2. / 3.) * mu * _Constants.mass_p / _Constants.k_B;
    rt_float temperature = GadgetUtoTFactor * (rt_float)SphP[iParticle].InternalEnergy;
    _Data.T[iGas] = temperature; /* Temperature [K] */
    // _Data.Entropy[iGas] = RT_EntropyFromTemperature(_Constants, temperature, iGas, _Data);
    _Data.Entropy[iGas] = (_Constants.k_B * temperature / _Constants.mass_p) /
                          (mu * std::pow(_Data.Density[iGas], _Constants.gamma_ad - RT_FLT_C(1.)));

    ++iGas;
  }
#ifdef DEBUG
  printf("Checking RT_Data...");
  RT_Data_CheckData(_Data,__FILE__,__LINE__);
  printf("done.\n");
#endif  

  CheckPointTime = getTimeInSeconds();
  if (ert_DumpFlag == 1) {
    if(ThisTask == 0 && All.Time != 0.0) {
      printf("Writing RT snapshot...");
      ert_write_snapshot(ert_snapshot_num);
      printf("done.\n");
    }
  }
  TimerArray[IO] += getTimeInSeconds() - CheckPointTime;

#ifdef TRACK_IONIZATION_RATE
  double NumberOfIonizedHydrogenAtoms_start = 0.0;
  for (size_t i = 0; i < nParticles; ++i) {
    NumberOfIonizedHydrogenAtoms_start += (double)_Data.f_H2[i];
  }
  NumberOfIonizedHydrogenAtoms_start *= NumberOfHydrogenAtomsPerParticle;
#endif

#ifdef DEBUG
  /* Sanity Check */
  double TotalMassInVolume = 0.0;
  bool PerformSanityCheck = true;
  switch (_Parameters.ProblemType) {
    case 5: /* Test 2 with hydrodynamical response */
      /* Note: Assuming (30 kpc)^3 volume. Not the (15 kpc)^3 volume described
       *       in the test documentation because we put the source at the centre */
      TotalMassInVolume = 1.3276e+39;
      break;
    default:
      printf("%s: %i: WARNING: Unknown ProblemType for Sanity Check: %i\n",
             __FILE__,
             __LINE__,
             (int)_Parameters.ProblemType);
      PerformSanityCheck = false;
  }
  if (PerformSanityCheck) {
    double BoxVolume = (double)All.BoxSize * (double)All.BoxSize * (double)All.BoxSize * (double)UnitLength *
                       (double)UnitLength * (double)UnitLength;
    double MeanDensity = 0.0;
    for (size_t iParticle = 0; iParticle < nParticles; ++iParticle) {
      MeanDensity += _Data.Density[iParticle];
    }
    MeanDensity /= (double)nParticles;
    double Mean_nH = 0.0;
    for (size_t iParticle = 0; iParticle < nParticles; ++iParticle) {
      Mean_nH += (double)_Data.n_H[iParticle];
    }
    Mean_nH /= (double)nParticles;
    double SumOfVolumes = 0.0;
    for (size_t iParticle = 0; iParticle < nParticles; ++iParticle) {
      SumOfVolumes += (double)_Data.dR[iParticle] * (double)_Data.dR[iParticle] * (double)_Data.dR[iParticle];
    }
    SumOfVolumes *= (4.0 / 3.0) * M_PI;

    printf("Mean_nH=%9.3e; BoxVolume=%9.3e; BoxSize=%9.3e; UnitLength=%9.3e\n",
           Mean_nH,
           BoxVolume,
           All.BoxSize,
           UnitLength);
    printf("SANITY CHECK (works for constant density only)\n");
    printf(" MeanDensity * BoxVolume      = %9.3e kg\n", MeanDensity * BoxVolume);
    printf(" Mean_nH*m_H*BoxVolume/(1-Y)  = %9.3e kg\n", Mean_nH * 1.6737e-27 * BoxVolume / (1.0 - _Constants.Y));
    printf(" nParticles * GasParticleMass = %9.3e kg\n", nParticles * GasParticleMass);
    printf("  Should be:                    %9.3e kg\n", TotalMassInVolume);
    printf(" (Sum of volumes)/(Total volume) = %9.3e\n", SumOfVolumes / BoxVolume);
    printf("  Should be                           %f\n", 1.0);
  }
  /* END OF SANITY CHECK */
#endif

  /* Allocate RT Data on the GPU
   * then copy from the host to the device */
  // Define an array of rt data structures
  printf("Allocating device RT Data and copying to devices...\n");
  std::vector<RT_Data *> data_list;
  for (int i = 0; i < nDev; i++) {
    cudaSetDevice(devs[i]);
    RT_Data *Data_dev_i = new RT_Data();
    printf("_Data.NumCells=%lu, NBytes=%lu\n, &R=%p; &dR=%p",_Data.NumCells,_Data.NBytes,_Data.R,_Data.dR);
    copyRTDataFromHostToDevice(_Data, Data_dev_i);
    data_list.push_back(Data_dev_i);
    printf("%s: Checking device %d copy of the RT data...",__FILE__,i);
    printf("Data_dev_i.NumCells=%lu, NBytes=%lu\n, &R=%p; &dR=%p",Data_dev_i->NumCells,Data_dev_i->NBytes,Data_dev_i->R,Data_dev_i->dR);
    RT_CUDA_RT_Data_CheckData(*Data_dev_i, __FILE__, __LINE__);
    printf("done.\n");
  }
  printf("done.\n");

  /* Allocate Rates on GPU, a set for each particle */
  printf("Allocating Rates on GPU and setting for each particle, populate...");
  std::vector<RT_RatesBlockT> rates_list(nDev);
  for (int i = 0; i < nDev; i++) {
    cudaSetDevice(devs[i]);
    rates_list[i] = RT_CUDA_RatesBlock_Allocate(nParticles);
    rates_list[i].NumCells = nParticles;
  }
  printf("done.\n");

  TimerArray[UpdatingFromGizmo] = getTimeInSeconds() - CheckPointTime;

  auto ParticlesToUpdateColumnDensities = find_particles_to_update();

  /* ??? Doesn't this need to be done for each CUDA device? */
  auto ParticlesToUpdate_dev = cuda_malloc<size_t>(ParticlesToUpdateColumnDensities.size());
  auto cerr = cudaMemcpy(ParticlesToUpdate_dev.get(),
                         ParticlesToUpdateColumnDensities.data(),
                         ParticlesToUpdateColumnDensities.size() * sizeof(size_t),
                         cudaMemcpyHostToDevice);
  checkCudaAPICall(cerr, __FILE__, __LINE__, true);

  std::unique_ptr<size_t[]> RtsphToUpdate(new size_t[ParticlesToUpdateColumnDensities.size()]);
  for (size_t i = 0; i < ParticlesToUpdateColumnDensities.size(); ++i) {
    RtsphToUpdate[i] = MapGadgetToRTSPH[ParticlesToUpdateColumnDensities[i]];
  }
  /* ??? Doesn't this need to be done for each CUDA device? */
  auto RtsphToUpdate_dev = cuda_malloc<size_t>(ParticlesToUpdateColumnDensities.size());
  cerr = cudaMemcpy(RtsphToUpdate_dev.get(),
                    RtsphToUpdate.get(),
                    sizeof(size_t) * ParticlesToUpdateColumnDensities.size(),
                    cudaMemcpyHostToDevice);
  checkCudaAPICall(cerr, __FILE__, __LINE__, true);

  printf("Will be finding column densities to %zu of %zu particles.\n",
         ParticlesToUpdateColumnDensities.size(),
         nParticles);

  RT_IntegralInvolatilesT Involatiles;
  /* Variables of the integration over frequency that don't change during
   * the integration through time. */
  ierr = RT_IntegralInvolatiles_allocate(_Constants, &Involatiles);
  if (ierr != EXIT_SUCCESS) {
    printf("ERROR: %s: %i: Unable to allocate memory for Involatiles\n", __FILE__, __LINE__);
    exit(EXIT_FAILURE);
  }

  ierr = RT_IntegralInvolatiles_setFrequenciesWeightsAlphas(_Constants, Involatiles);
  if (ierr != EXIT_SUCCESS) {
    printf("ERROR: %s: %i: Unable to set Involatile Frequencies, Weights, and Alphas\n", __FILE__, __LINE__);
    exit(EXIT_FAILURE);
  }

  /* Involatiles_dev will contain pointers that point to device memory */
  RT_IntegralInvolatilesT Involatiles_dev;
  /* ??? Doesn't this need to be done for each CUDA device? */
  ierr = RT_CUDA_allocateIntegralInvolatilesOnDevice(Involatiles, &Involatiles_dev);
  if (ierr != EXIT_SUCCESS) {
    printf("ERROR: %s: %i: Unable to allocate device memory for Integral Involatiles.\n", __FILE__, __LINE__);
    exit(EXIT_FAILURE);
  }

  rt_float const t_start = _current_time;  // Stored globally from end of last call to ert_parent_routine
  rt_float t = t_start;  // Keep track of current time as we loop in this call to ert_parent_routine
  rt_float t_stop, redshift, ExpansionFactor;
  switch (All.ComovingIntegrationOn) {
    case 0:
      t_stop = All.Time * UNIT_TIME_IN_CGS;  // current gizmo time in seconds
      redshift = 0;                          // Need to update this to come from the header
      ExpansionFactor = 1.0;
      break;
    case 1:
      t_stop = cosmological_time(All.Time, _Constants);
      redshift = 1. / All.Time - 1.0;
      ExpansionFactor = All.Time;
      break;
    default:
      std::cerr << __FILE__ ": " << __LINE__ << ": "
                << "ERROR: unknown value of ComovingIntegrationOn:" << All.ComovingIntegrationOn << std::endl;
      return;
  }
  printf("t_start %5.3e s (%5.3e Code Units), t_stop %5.3e s ( %5.3e Ma), dt %5.3e Ma\n",
         t_start,
         t_start / UNIT_TIME_IN_CGS,
         t_stop,
         t_stop / _Constants.Ma,
         (t_stop - t_start) / _Constants.Ma);

  (void)fflush(stdout);
  /* Main loop */
  unsigned char LastIterationFlag = FALSE;

  /* Set the number of sources per device */
  const int num_sources_per_device = (int)std::ceil((double)nSources / (double)nDev);

  /* Data outputs to file are done using threads so need to set omp to be able to handle dynamic nested parallel
   * threads*/
  omp_set_dynamic(0);

  /* Begin parallel threads for GPUs */
#pragma omp parallel num_threads(nDev) default(shared)
  {
    /* We want each GPU to be controlled by a thread */
    int tid = omp_get_thread_num();
    cudaSetDevice(devs[tid]);
    gpuErrchk(cudaPeekAtLastError());
    DeviceMemoryPointers DevMemP = initializeRtsphCuda(nParticles, nParticles, tree_list[tid]);

    while (!LastIterationFlag) {
      /* Broadcast RT_Data from root to all ranks and initialize rates */
      // if (tid == 0) {
      // printf("Broadcasting data...");
      /* Which is correct: count = nParticles * 12 or count = data_list[tid]->NBytes/sizeof(rt_float) */
      /* The difference is that nParticles * 12 takes the first 12 of the 15 rt_float fields, as the last three are
       * derived */
      // }
      if (nDev > 1) {
        NCCLCHECK(ncclBroadcast((void *)data_list[0]->MemSpace,
                                (void *)data_list[tid]->MemSpace,
                                data_list[0]->NBytes / sizeof(rt_float),
                                ncclFloat,
                                0,  // root
                                comms[tid],
                                s[tid]));
        cudaStreamSynchronize(s[tid]);
      }

      zeroRates(rates_list[tid]);

#if 0
      size_t NumParticlesToUpdateColumnDensities = 0;
#  pragma omp parallel for reduction(+ : NumParticlesToUpdateColumnDensities)
      /* Loop over particles to calculate column density updates*/
      for (int iParticle = 0; iParticle < nParticles; ++iParticle) {
        if (RecalculateColumnDensityFlag[iParticle] >= _Parameters.RecalculateColumnDensityIterationCount) {
          NumParticlesToUpdateColumnDensities++;
        }
      }

      size_t *ParticlesToUpdateColumnDensities = new size_t[NumParticlesToUpdateColumnDensities];
      /* This is a race condition on RecalculateColumnDensityFlag with more than one device.
         See also:  calculateSourceParticlePairRates_CUDA.cu, where I've blocked the update there. */
      size_t Placeholder = 0;
      for (iParticle = 0; iParticle < nParticles; ++iParticle) {
        if (RecalculateColumnDensityFlag[iParticle] >= Parameters.RecalculateColumnDensityIterationCount) {
          ParticlesToUpdateColumnDensities[Placeholder] = iParticle;
          RecalculateColumnDensityFlag[iParticle] = 0;
          Placeholder++;
        } else {
          RecalculateColumnDensityFlag[iParticle]++;
        }
      }
#else
      /* Hardwire all particles to update */
      size_t *ParticlesToUpdateColumnDensities = new size_t[nParticles];
      for (int iParticle = 0; iParticle < nParticles; ++iParticle) {
        ParticlesToUpdateColumnDensities[iParticle] = iParticle;
      }
      size_t NumParticlesToUpdateColumnDensities = nParticles;
#endif

      /* Loop over sources */
      /* We want each GPU to only loop over a subset of the sources defined using threadId */
      size_t source_start = tid * num_sources_per_device;
      size_t source_end = (tid + 1) * num_sources_per_device;
      // Ensure the last device doesn't loop over non-existent sources
      if (source_end > nSources) source_end = nSources;
      // printf("Thread %d on dev %d looping over sources %lu to %lu\n", tid, devs[tid], source_start, source_end);
      // #  pragma omp barrier // debug speed test
      // if (source_start != source_end) {  // testing to see if this stops the idle thread from causing problems
      for (size_t iSource = source_start; iSource < source_end; iSource++) {
        /*
         *  for (int iSrc = 0; iSrc < _Source.size(); ++iSrc) {
         */
        const auto &Src = _Source[iSource];
        CheckPointTime = getTimeInSeconds();

#ifdef SAH_BVH
        ierr = genRtSourceRays(Src,
                               *DevMemP.tree,
                               RtsphToUpdate_dev.get(),
                               ParticlesToUpdateColumnDensities.size(),
                               *DevMemP.rays,
                               header.BoxSize);
#else
        ierr = genRtSourceRays(Src,
                               *tree_list[tid],
                               RtsphToUpdate_dev.get(),
                               NumParticlesToUpdateColumnDensities,
                               *DevMemP.rays,
                               header.BoxSize);

        /*
         *    ierr = genRtSourceRays(
         *        Src, tree, RtsphToUpdate_dev.get(), ParticlesToUpdateColumnDensities.size(), *DevMemP.rays,
         * header.BoxSize);
         */
#endif
        if (ierr != EXIT_SUCCESS) {
          gizmo_terminate("Manitou: Failed to generate rays");
        }

        cerr = cudaDeviceSynchronize();
        checkCudaAPICall(cerr, __FILE__, __LINE__, true);

        TimerArray[CreatingRays] += getTimeInSeconds() - CheckPointTime;
        CheckPointTime = getTimeInSeconds();

        ierr = columnDensities_CUDA(ParticlesToUpdate_dev.get(),
                                    NumberOfHydrogenAtomsPerParticle,
                                    NumberOfHeliumAtomsPerParticle,
                                    UnitLength,
                                    nParticles,
                                    nParticles,
                                    DevMemP,
#ifndef SAH_BVH
                                    *tree_list[tid],
#endif
                                    *data_list[tid]);
        if (ierr == EXIT_FAILURE) {
          printf("ERROR: %s: %i: Failed to calculate Column Densities.\n", __FILE__, __LINE__);
          (void)fflush(stdout);
          exit(EXIT_FAILURE);
        }

        TimerArray[ColumnDensities] += getTimeInSeconds() - CheckPointTime;

        /* Set the distance from the source to each particle */
        setSourceToParticleDistance(
#ifdef SAH_BVH
            *DevMemP.tree,
            DevMemP.tree_map.get(),
#else
            *tree_list[tid],
            tree_list[tid]->get_map(),
#endif
            Src,
            All.BoxSize,
            UnitLength,
            *data_list[tid]);
        cudaDeviceSynchronize();

        ierr = RT_IntegralInvolatiles_setLuminosity(Src, _Constants, redshift, Involatiles);
        if (ierr != EXIT_SUCCESS) {
          printf("ERROR: %s: %i: Failed to set Luminosity.\n", __FILE__, __LINE__);
          exit(EXIT_FAILURE);
        }

        CheckPointTime = getTimeInSeconds();
        /* ??? Need to copy to each device ? */
        ierr = RT_CUDA_copyIntegralInvolatilesToDevice(Involatiles, Involatiles_dev);
        if (ierr != EXIT_SUCCESS) {
          printf("ERROR: %s: %i: Failed to copy Integral Involatiles to device.\n", __FILE__, __LINE__);
          exit(EXIT_FAILURE);
        }

        calculateSourceParticlePairRates_CUDA(*data_list[tid],
                                              _Parameters,
                                              _Constants_dev,
                                              ExpansionFactor,
                                              Src,
                                              Involatiles_dev,
                                              rates_list[tid],
                                              _RecalculateColumnDensityFlag);
        cudaDeviceSynchronize();
        TimerArray[IonizationRates] += getTimeInSeconds() - CheckPointTime;
      }  // End of loop over all sources
#pragma omp barrier

      delete ParticlesToUpdateColumnDensities;

      /* Update the overall heating rates
       * This must be done on each device as it will then be reduced
       */
      CheckPointTime = getTimeInSeconds();
      updateCumulativeHeatingRate(rates_list[tid]);
      cudaDeviceSynchronize();

      /* Call reductions to sum up data on master rank */
      if (nDev > 1) {
        // printf("Thread %d on Device %d has reached the reductions.\n", tid, devs[tid]);
        // if (tid == 0) {
        //   printf("Calling Reductions...");
        // }
        NCCLCHECK(ncclReduce((const void *)rates_list[tid].I_H1,
                             (void *)rates_list[tid].I_H1,
                             nParticles,
                             ncclFloat,
                             ncclSum,
                             0,  // root
                             comms[tid],
                             s[tid]));
        NCCLCHECK(ncclReduce((const void *)rates_list[tid].I_He1,
                             (void *)rates_list[tid].I_He1,
                             nParticles,
                             ncclFloat,
                             ncclSum,
                             0,  // root
                             comms[tid],
                             s[tid]));
        NCCLCHECK(ncclReduce((const void *)rates_list[tid].I_He2,
                             (void *)rates_list[tid].I_He2,
                             nParticles,
                             ncclFloat,
                             ncclSum,
                             0,  // root
                             comms[tid],
                             s[tid]));
        NCCLCHECK(ncclReduce((const void *)rates_list[tid].Gamma_HI,
                             (void *)rates_list[tid].Gamma_HI,
                             nParticles,
                             ncclFloat,
                             ncclSum,
                             0,  // root
                             comms[tid],
                             s[tid]));
        NCCLCHECK(ncclReduce((const void *)rates_list[tid].G,
                             (void *)rates_list[tid].G,
                             nParticles,
                             ncclFloat,
                             ncclSum,
                             0,  // root
                             comms[tid],
                             s[tid]));
        // synchronoizing on CUDA streams to wait for completion of NCCL operation
        cudaStreamSynchronize(s[tid]);
      }
      // if (tid == 0 && nDev > 1) {
      //   printf("done.\n");
      // }
/* Only the master device should perform the following operations */
// master does not have implicit barriers
#pragma omp master
      {
        CheckPointTime = getTimeInSeconds();
        /* Now we have photoionization rates for each particle */
        /* Calculate the Recombination Cooling Rates */
        calculateRecombinationRatesInclCooling(ExpansionFactor, _Constants_dev, *data_list[tid], rates_list[tid]);
        cudaDeviceSynchronize();
        TimerArray[Recombinations] += getTimeInSeconds() - CheckPointTime;

#ifndef NO_COOLING
        CheckPointTime = getTimeInSeconds();
        /* Extra cooling terms */
        RT_CUDA_CoolingRates(ExpansionFactor, _Constants_dev, *data_list[tid], rates_list[tid]);
        cudaDeviceSynchronize();
        TimerArray[ExtraCooling] += getTimeInSeconds() - CheckPointTime;
#endif

        CheckPointTime = getTimeInSeconds();
        /* Set the equilibrium values for HI & HII */
        RT_CUDA_setHydrogenEquilibriumValues(_Constants, *data_list[tid], rates_list[tid]);
        cudaDeviceSynchronize();
        TimerArray[EquilibriumValues] += getTimeInSeconds() - CheckPointTime;

        CheckPointTime = getTimeInSeconds();
        /* Find the next RT timestep */
        rt_float dt_RT;
        findNextTimestep(nParticles,
                         max_timestep,
                         0.,
                         *data_list[tid],
                         rates_list[tid],
                         _Constants,
                         /* Output */
                         dt_RT,
                         LastIterationFlag);
        cudaDeviceSynchronize();
        TimerArray[TimeStep] += getTimeInSeconds() - CheckPointTime;

        last_dt_RT = dt_RT;

        /* t_stop is a hard upper limit */
        if ((t + dt_RT) >= t_stop) {
          dt_RT = t_stop - t;
          t = t_stop;
          LastIterationFlag = true;
        } else {
          t += dt_RT;
          LastIterationFlag = false;
        }

        printf("t = %5.3e s ( %5.3e Ma); dt = %5.3e s ( %5.3e Ma); t_stop = %5.3e s ( %5.3e Ma)\n",
               t,
               t / _Constants.Ma,
               dt_RT,
               dt_RT / _Constants.Ma,
               t_stop,
               t_stop / _Constants.Ma);

        CheckPointTime = getTimeInSeconds();
        /* Update particle fractions and entropies */
        updateParticles(dt_RT, redshift, _Constants_dev, rates_list[tid], *data_list[tid]);
        cudaDeviceSynchronize();
        TimerArray[Updating] += getTimeInSeconds() - CheckPointTime;

      }              // omp master
#pragma omp barrier  // pragma omp master does not have an implicit barrier

    }  // End loop over time
    printf("Thread %d executed successfully.\n", tid);
  }                   // omp parallel
#pragma omp taskwait  // wait for child tasks to finish

  /* Copy device data to host */
  CheckPointTime = getTimeInSeconds();
  cudaMemcpy(_Data.MemSpace, data_list[0]->MemSpace, _Data.NBytes, cudaMemcpyDeviceToHost);
  checkCUDAErrors("Memcpy failed", __FILE__, __LINE__);
  TimerArray[MemCopy] += getTimeInSeconds() - CheckPointTime;

  CheckPointTime = getTimeInSeconds();
  rt_float numNeutral = 0;
  printf("Updating GIZMO Particle Energies...");
  for (size_t iParticle = 0, iGas = 0; iParticle < N_gas; iParticle++) {
    if (P[iParticle].Type != 0) {
      continue;
    }
    /* Update the neutral & ionised number densities. */
    rt_float const n_H2 = _Data.n_H[iGas] * _Data.f_H2[iGas];
    rt_float const n_He2 = _Data.n_He[iGas] * _Data.f_He2[iGas];
    rt_float const n_He3 = _Data.n_He[iGas] * _Data.f_He3[iGas];

    /* Electron density */
    rt_float const n_e = n_H2 + n_He2 + (2. * n_He3);

    /* Total number density */
    rt_float const n = _Data.n_H[iGas] + _Data.n_He[iGas] + n_e;

    rt_float const mu = (_Data.n_H[iGas] + 4. * _Data.n_He[iGas]) / n;

    rt_float GadgetUtoTFactor = 1.e6 * (2. / 3.) * mu * _Constants.mass_p / _Constants.k_B;

    // rt_float newT = RT_TemperatureFromEntropy(_Constants, _Data.Entropy[iGas], iGas, _Data);
    rt_float const newT = mu * (_Constants.mass_p / _Constants.k_B) * _Data.Entropy[iGas] *
                          std::pow(_Constants.Mpc, RT_FLT_C(1.0) - _Constants.gamma_ad) *
                          std::pow(_Data.Density[iGas] * _Constants.Mpc, _Constants.gamma_ad - RT_FLT_C(1.0));
    rt_float unew = (1.0 / GadgetUtoTFactor) * newT;

    SphP[iParticle].InternalEnergy = unew;
    SphP[iParticle].InternalEnergyPred = unew;
    SphP[iParticle].Pressure = get_pressure(iParticle);
    SphP[iParticle].DtInternalEnergy = 0;
    SphP[iParticle].HI = _Data.f_H1[iGas];
    SphP[iParticle].HII = _Data.f_H2[iGas];
    SphP[iParticle].HeI = _Data.f_He1[iGas];
    SphP[iParticle].HeII = _Data.f_He2[iGas];
    SphP[iParticle].HeIII = _Data.f_He3[iGas];

    numNeutral += _Data.f_H1[iGas];

    SphP[iParticle].Taranis_Temperature = _Data.T[iGas];

    ++iGas;
  }
  printf("done.\n");
  TimerArray[UpdatingToGizmo] += getTimeInSeconds() - CheckPointTime;

  double TotalTime = getTimeInSeconds() - tStart;
  TimerArray[CumulativeTime] += TotalTime;
  for (size_t i = 0; i < nCodeTimes; ++i) {
    _TimerArray[i] += TimerArray[i];
  }

  double SumKnownTimes = 0.0;
  for (size_t i = 1; i < nCodeTimes; ++i) {
    SumKnownTimes += TimerArray[i];
  }
  std::cout << "Walltime              = " << TotalTime << "s" << std::endl;
  std::cout << "Creating spheres      = " << 100.0 * TimerArray[CreatingSpheres] / TotalTime << " %" << std::endl;
  std::cout << "Tree Creation time    = " << 100.0 * TimerArray[CreatingTree] / TotalTime << " %" << std::endl;
  std::cout << "Updating From Gizmo   = " << 100.0 * TimerArray[UpdatingFromGizmo] / TotalTime << " %" << std::endl;
  std::cout << "Ray Creation time     = " << 100.0 * TimerArray[CreatingRays] / TotalTime << " %" << std::endl;
  std::cout << "Column Densities time = " << 100.0 * TimerArray[ColumnDensities] / TotalTime << " %" << std::endl;
  std::cout << "Ionization Rates      = " << 100.0 * TimerArray[IonizationRates] / TotalTime << " %" << std::endl;
  std::cout << "Recombinations        = " << 100.0 * TimerArray[Recombinations] / TotalTime << " %" << std::endl;
  std::cout << "Extra Cooling         = " << 100.0 * TimerArray[ExtraCooling] / TotalTime << " %" << std::endl;
  std::cout << "Update Heating Rates  = " << 100.0 * TimerArray[UpdateHeatingRates] / TotalTime << " %" << std::endl;
  std::cout << "TimeStep              = " << 100.0 * TimerArray[TimeStep] / TotalTime << " %" << std::endl;
  std::cout << "Updating              = " << 100.0 * TimerArray[Updating] / TotalTime << " %" << std::endl;
  std::cout << "Setting Equil. values = " << 100.0 * TimerArray[EquilibriumValues] / TotalTime << " %" << std::endl;
  std::cout << "Device Memory IO      = " << 100.0 * TimerArray[MemCopy] / TotalTime << " %" << std::endl;
  std::cout << "Updating To Gizmo     = " << 100.0 * TimerArray[UpdatingToGizmo] / TotalTime << " %" << std::endl;
  std::cout << "Updating To Gizmo     = " << 100.0 * TimerArray[IO] / TotalTime << " %" << std::endl;
  std::cout << "Other                 = " << 100.0 * (TotalTime - SumKnownTimes) / TotalTime << " %" << std::endl;

  _current_time = t_stop;

#ifdef TRACK_IONIZATION_RATE
  double NumberOfIonizedHydrogenAtoms_end = 0.0;
  for (int i = 0; i < nParticles; ++i) {
    NumberOfIonizedHydrogenAtoms_end += (double)_Data.f_H2[i];
  }
  NumberOfIonizedHydrogenAtoms_end *= NumberOfHydrogenAtomsPerParticle;
  double const IonizationRate =
      (NumberOfIonizedHydrogenAtoms_end - NumberOfIonizedHydrogenAtoms_start) / (t_stop - t_start);
  printf("Ionization rate = %5.3e ; NOIHA=%5.3e; NOHAPP=%5.3e\n",
         IonizationRate,
         NumberOfIonizedHydrogenAtoms_end,
         NumberOfHydrogenAtomsPerParticle);
#endif

  /* Free space on devices */
  for (int i = 0; i < nDev; i++) {
    ERT_CUDA_CHECK(cudaFree((*data_list[i]).MemSpace));
    delete data_list[i];
    RT_CUDA_RatesBlock_Free(rates_list[i]);
    delete tree_list[i];
  }
  /* ??? These should be in a loop over all devices? */
  RT_CUDA_freeIntegralInvolatilesOnDevice(Involatiles_dev);
  RT_IntegralInvolatiles_free(Involatiles);
  // delete tree_map_storage;
  // delete MapGadgetToRTSPH;
  // delete ParticlesToUpdateColumnDensities;
  // ERT_CUDA_CHECK( cudaFree( ParticlesToUpdate_dev.get() ) );
  // ERT_CUDA_CHECK( cudaFree( RtsphToUpdate_dev.get() ) );

  /* Free space on host */
  if (nDev > 1) {
    free(s);
  }
  free(devs);

  printf("Exiting ert_parent_routine [Manitou] with no CUDA errors.\n");
}

void reportMemUsage() {
  // show memory usage of GPU for debugging
  size_t free_byte;
  size_t total_byte;
  cudaError_t cuda_status;
  cuda_status = cudaMemGetInfo(&free_byte, &total_byte);
  if (cudaSuccess != cuda_status) {
    printf("Error: cudaMemGetInfo fails, %s \n", cudaGetErrorString(cuda_status));
    exit(1);
  }
  double free_db = (double)free_byte;
  double total_db = (double)total_byte;
  double used_db = total_db - free_db;
  printf("GPU memory usage: used = %f, free = %f MB, total = %f MB\n",
         used_db / 1024.0 / 1024.0,
         free_db / 1024.0 / 1024.0,
         total_db / 1024.0 / 1024.0);
}
