#pragma once

#include <cudasupport.h>
#include <vector_functions.h>
#include <vector_types.h>

ERT_HOST_DEVICE inline float3 operator+(float3 lhs, float3 rhs) {
  return make_float3(lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z);
}

ERT_HOST_DEVICE inline float3 operator-(float3 lhs, float3 rhs) {
  return make_float3(lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z);
}

ERT_HOST_DEVICE inline float3 operator*(float scalar, float3 vec) {
  return make_float3(scalar * vec.x, scalar * vec.y, scalar * vec.z);
}

ERT_HOST_DEVICE inline float3 operator*(float3 vec, float scalar) { return scalar * vec; }

ERT_HOST_DEVICE inline float3 vecFromArray(const float arr[3]) { return make_float3(arr[0], arr[1], arr[2]); }

ERT_HOST_DEVICE inline float3 operator/(float3 vec, float scalar) {
  return make_float3(vec.x / scalar, vec.y / scalar, vec.z / scalar);
}

ERT_HOST_DEVICE inline float3& operator/=(float3& vec, float scalar) {
  vec = vec / scalar;
  return vec;
}

ERT_HOST_DEVICE inline float3& operator+=(float3& lhs, float3 rhs) {
  lhs.x += rhs.x;
  lhs.y += rhs.y;
  lhs.z += rhs.z;
  return lhs;
}

ERT_HOST_DEVICE inline float3& operator-=(float3& lhs, float3 rhs) {
  lhs.x -= rhs.x;
  lhs.y -= rhs.y;
  lhs.z -= rhs.z;
  return lhs;
}
