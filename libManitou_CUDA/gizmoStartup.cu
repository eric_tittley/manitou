#include <algorithm>

#include "DeviceMemoryPointers.cuh"
#include "Manidoowag.h"
#include "Manitou.h"
#include "Manitou_CUDA.h"
#include "ERT_gizmo.h"
#include "RT.h"
#include "RT_CUDA.h"
#include "RT_Data.h"
#include "RT_Luminosity.h"
#include "allvars.h"
#include "cudasupport.h"
#include "manitou_vars.h"

/* Here we will perfom everything that needs to be done once.
 * Load constants file ect...
 *
 * Test 2 source can be reused for test 5, same with 3/6
 */
RT_RatesBlockT _Rates_dev;

RT_ConstantsT _Constants;
RT_ConstantsT _Constants_dev;

ParametersT _Parameters;

DynamicSources _DynamicSources;
std::vector<RT_SourceT> _Source;
#ifdef MANITOU_FOF_SOURCES
float *_source_table;
#endif

float _current_time;

double *_TimerArray;
double _StartTime;
double _TotalTime;

unsigned int *_RecalculateColumnDensityFlag;
rt_float _numNeutral;

void ert_startup_routine() {
  printf("Got to ert_startup_routine [Manitou]\n");
  /* Initialize run parameters. */
  printf("Reading Parameter File...");
  char const *const ParametersFile = "Manitou.params";
  int ierr = readParameters(ParametersFile, &_Parameters);
  if (ierr != EXIT_SUCCESS) {
    std::cout << "ERROR: " << __FILE__ << ": " << __LINE__;
    std::cout << ": Unable to read Parameters file.";
    std::cout << std::endl;
  }
  printf("done.\n");

  // Load Constants File
  printf("Load Constants file...");
  char const *const ConstantsFile = "Constants.params";
  ierr = RT_ReadConstantsFile(ConstantsFile, &_Constants);
  if (ierr != EXIT_SUCCESS) {
    std::cout << "ERROR: " << __FILE__ << ": " << __LINE__;
    std::cout << ": Unable to read Constants file.";
    std::cout << std::endl;
  }
  printf("done.\n");

  // Set the start time
  switch (All.ComovingIntegrationOn) {
    case 0:
      _current_time = All.Time * UNIT_TIME_IN_CGS;  // current gizmo time in seconds
      break;
    case 1:
      _current_time = cosmological_time(All.Time, _Constants);
      break;
    default:
      std::cerr << __FILE__ ": " << __LINE__ << ": "
                << "ERROR: unknown value of ComovingIntegrationOn:" << All.ComovingIntegrationOn << std::endl;
      return;
  }

  /*Timing Variables*/

  _TimerArray = (double *)malloc(nCodeTimes * sizeof(double));
  _StartTime = getTimeInSeconds();
  _TotalTime = 0.0;
  for (size_t i = 0; i < nCodeTimes; ++i) {
    _TimerArray[i] = 0.0;
  }

  /* Report GPU(s) being used. */
  int devCount = 0;
  cudaGetDeviceCount(&devCount);
  int nDev = devCount;
  if (nDev > _Parameters.nSources) {
    nDev = _Parameters.nSources;
  }
  std::cout << "Using " << devCount << " CUDA device(s)";
  for (int i=0; i<devCount; ++i) {
   RT_CUDA_Util_PrintDeviceProperties(i);
  }

  _Constants_dev = _Constants;
  RT_CUDA_CopyConstantsToDevice(_Constants, &_Constants_dev);

  /* Increase cudaLimitStackSize on the device. */
  size_t pValue;
  bool const TerminateOnCudaError = true;
  cudaError_t cudaError = cudaDeviceGetLimit(&pValue, cudaLimitStackSize);
  checkCudaAPICall(cudaError, __FILE__, __LINE__, TerminateOnCudaError);
  printf("Current LimitStackSize=%lu. Setting to 2048 bytes\n", pValue);
  cudaError = cudaDeviceSetLimit(cudaLimitStackSize, 2048);
  checkCudaAPICall(cudaError, __FILE__, __LINE__, TerminateOnCudaError);

   size_t const nParticles = (size_t)All.TotN_gas;

  _RecalculateColumnDensityFlag = new unsigned[nParticles];
  std::fill_n(_RecalculateColumnDensityFlag, nParticles, _Parameters.RecalculateColumnDensityIterationCount);

  if (RestartFlag == 0) {  // Not restarting
#pragma omp parallel for shared(nParticles,SphP,_Parameters)
    for (size_t i = 0; i < nParticles; i++) {
      SphP[i].HI = _Parameters.InitialFractions[0];
      SphP[i].HII = _Parameters.InitialFractions[1];
      SphP[i].HeI = _Parameters.InitialFractions[2];
      SphP[i].HeII = _Parameters.InitialFractions[3];
      SphP[i].HeIII = _Parameters.InitialFractions[4];
    }
  }

  // set up sources
  printf("Setting up sources...");
  _Source.resize(_Parameters.nSources);
  Manitou_SetSources(_Parameters, All.BoxSize, _Constants, _Source.data());

  /* Initialize the source functions */
  for (auto &Src : _Source) {
    RT_SetSourceFunction(&Src);
  }
  if (_Parameters.nSources > 0) {
    RT_InitialiseSpectrum(0, _Constants, _Source.data());
  }
  printf("done\n");

  printf("Allocating Rates...");
  _Rates_dev = RT_CUDA_RatesBlock_Allocate(nParticles);
  _Rates_dev.NumCells = nParticles;
  printf("done.\n");

#ifdef MANITOU_FOF_SOURCES
  char fname[] = "Geneva_0.002_burst_1e6msol";
  size_t count = 7 * 100;
  FILE *fp = fopen(fname, "rb");
  if (!fp) {
    perror("Manitou: could not open starburst file");
    gizmo_terminate("Failed to open starburst file");
  }
  _source_table = new float[count];
  size_t nread = fread(_source_table, sizeof(float), count, fp);
  if (nread != count) {
    if (feof(fp)) {
      printf(
          "Manitou: starburst file invalid, expected %zu floats, hit EOF "
          "after %zu",
          count,
          nread);
    } else {
      perror("Manitou: error reading starburst file");
    }

    fclose(fp);
    gizmo_terminate("Failed to read starburst file");
  }

  fclose(fp);
#endif

  printf("Leaving ert_startup_routine.\n");
}
