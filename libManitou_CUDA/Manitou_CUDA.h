#pragma once

/* CUDA-unaware header of functions that are CUDA-aware */

#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include "BVH.h"
#include "DeviceMemoryPointers.cuh"
#include "Parameters.h"
#include "Rays.h"
#include "cuda_runtime.h"
#include "generateRays.h"

// fwd:
struct LinearBVH;

extern "C" {
/* RT includes */
#include "RT_Data.h"
#include "RT_IntegralInvolatilesT.h"
#include "RT_Precision.h"
#include "RT_RatesT.h"
#include "RT_SourceT.h"
}

void calculateRecombinationRatesInclCooling(rt_float const ExpansionFactor,
                                            RT_ConstantsT const Constants_dev,
                                            RT_Data const Data_dev,
                                            /* updated */ RT_RatesBlockT Rates_dev);

void calculateSourceParticlePairRates_CUDA(RT_Data const Data_dev,
                                           ParametersT const Parameters,
                                           RT_ConstantsT const Constants_dev,
                                           rt_float const ExpansionFactor,
                                           RT_SourceT Source,
                                           RT_IntegralInvolatilesT const Involatiles_dev,
                                           /* Incremented */ RT_RatesBlockT Rates_dev,
                                           /*    Updated */ unsigned int *const RecalculateColumnDensityFlag);

#ifdef SAH_BVH
int columnDensities_CUDA(size_t const *const ParticlesToUpdateColumnDensities,
                         double const NumberOfHydrogenAtomsPerParticle,
                         double const NumberOfHeliumAtomsPerParticle,
                         rt_float const UnitLength,
                         size_t nRays,
                         size_t nParticles,
                         DeviceMemoryPointers const &DevMemP,
                         RT_Data const Data);
#else
int columnDensities_CUDA(size_t const *const ParticlesToUpdateColumnDensities,
                         double const NumberOfHydrogenAtomsPerParticle,
                         double const NumberOfHeliumAtomsPerParticle,
                         rt_float const UnitLength,
                         size_t nRays,
                         size_t nParticles,
                         DeviceMemoryPointers const &DevMemP,
                         const LinearBVH &tree,
                         RT_Data const Data);
#endif

void copyColumnDensitiesToDevice(RT_Data const Data_host, RT_Data const Data_dev);

void copyColumnDensitiesFromDevice(RT_Data const Data_host, RT_Data const Data_dev);

void copyGammaFromRatesToData_CUDA(RT_RatesBlockT const Rates_dev,
                                   /* updated */ RT_Data Data_dev);

void copyRTDataFromDeviceToHost(RT_Data const Data_dev, RT_Data Data);
void copyRTDataFromHostToDevice(RT_Data const Data_src, RT_Data *const Data_dest);

#ifdef SAH_BVH
DeviceMemoryPointers initializeRtsphCuda(size_t const nParticles, size_t const nRays, BVH *tree);
#else
DeviceMemoryPointers initializeRtsphCuda(size_t const nParticles, size_t const nRays, LinearBVH *tree);
#endif

void resetEntropyAndTemperature_CUDA(rt_float const Temperature, RT_ConstantsT const Constants, RT_Data const Data_dev);

void setSourceToParticleDistance(TreeType const &DevMemP,
                                 uint32_t const *tree_map,
                                 RT_SourceT const &Source,
                                 /* Same units as h_particles. */
                                 rt_float const BoxSize,
                                 rt_float const UnitLength,
                                 RT_Data Data_dev);

#if defined(GIZMO_BINDINGS) || 1  // TODO: no macro is actually defined
int GIZMO_IntegralInvolatiles_setLuminosity(int Source_id,
                                            RT_ConstantsT const Constants,
                                            rt_float redshift,
                                            RT_IntegralInvolatilesT Involatiles);
#endif
