#pragma once
#include <memory>

#include "cudasupport.h"

struct cuda_deleter {
  template <typename T>
  void operator()(T* p) {
    auto cerr = cudaFree(p);
    checkCudaAPICall(cerr, __FILE__, __LINE__, true);
  }
};

template <typename T>
using cuda_ptr = std::unique_ptr<T, cuda_deleter>;

template <typename T>
cuda_ptr<T> cuda_malloc(size_t size) {
  void* mem = nullptr;
  auto cerr = cudaMalloc(&mem, size * sizeof(T));
  checkCudaAPICall(cerr, __FILE__, __LINE__, true);
  return cuda_ptr<T>((T*)mem);
}

/** Pin existing host memory within a scope (RAII)
 *
 * Pinning host memory significantly improves cudaMemcpy throughput.
 */
class PinMemoryGuard {
  void* ptr_;

 public:
  PinMemoryGuard(void* ptr, size_t nbytes) : ptr_(ptr) { ERT_CUDA_CHECK(cudaHostRegister(ptr, nbytes, /*flags=*/0)); }

  ~PinMemoryGuard() { ERT_CUDA_CHECK(cudaHostUnregister(ptr_)); }
};
