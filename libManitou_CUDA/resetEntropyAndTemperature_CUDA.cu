#include "Manidoowag.h"
#include "Manitou_CUDA.h"
#include "RT_CUDA.h"
#include "cudasupport.h"

__global__ void resetEntropy_kernel(rt_float const Temperature, RT_ConstantsT const Constants, RT_Data Data) {
  size_t const istart = blockIdx.x * blockDim.x + threadIdx.x;
  size_t const span = gridDim.x * blockDim.x;  // nBlocks * nThreadsPerBlock

  for (size_t cellindex = istart; cellindex < Data.NumCells; cellindex += span) {
    rt_float const Entropy = RT_CUDA_EntropyFromTemperature(Temperature, cellindex, &Data, &Constants);
    Data.Entropy[cellindex] = Entropy;
  }
}

void resetEntropyAndTemperature_CUDA(rt_float const Temperature,
                                     RT_ConstantsT const Constants,
                                     RT_Data const Data_dev) {
  int gridSize;   // The actual grid size needed, based on input
                  // size
  int blockSize;  // The launch configurator returned block size
  setGridAndBlockSize(Data_dev.NumCells, (void *)resetEntropy_kernel, &gridSize, &blockSize);

  resetEntropy_kernel<<<gridSize, blockSize>>>(Temperature, Constants, Data_dev);

  bool const TerminateOnCudaError = true;
  checkCudaKernelLaunch(__FILE__, __LINE__, TerminateOnCudaError);

  /* Temperature is easier; just a constant */
  setVectorToValue(Data_dev.T, Temperature, Data_dev.NumCells);
}
