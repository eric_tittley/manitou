#include <cuda_runtime.h>

#include "Manitou_CUDA.h"
#include "cudasupport.h"

void copyRTDataFromDeviceToHost(RT_Data const Data_dev, RT_Data Data_hst) {
  cudaDeviceSynchronize();
  cudaError_t const cudaError =
      cudaMemcpy(Data_hst.MemSpace, Data_dev.MemSpace, Data_hst.NBytes, cudaMemcpyDeviceToHost);
  cudaDeviceSynchronize();
  bool const TerminateOnCudaError = true;
  checkCudaAPICall(cudaError, __FILE__, __LINE__, TerminateOnCudaError);
}
