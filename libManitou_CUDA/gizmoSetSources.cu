#include <allvars.h>

#include "config.h"

#ifdef MANITOU_FOF_SOURCES
extern "C" {
#  include <proto.h>
#  include <structure/fof.h>

#  include "RT_Luminosity.h"
}

#  include <algorithm>
#  include <cmath>
#  include <cstdio>
#  include <cstring>
#  include <limits>
#  include <numeric>
#  include <vector>

#  include "ERT_gizmo.h"
#  include "manitou_vars.h"

#  if !defined(GALSF) || !defined(FOF)
#    error GIZMO must be built with FOF and GALSF enabled
#  elif ((FOF_PRIMARY_LINK_TYPES | FOF_SECONDARY_LINK_TYPES) & 16) == 0
#    error Star particles must be linked to FOF groups
#  endif

#  ifndef MANITOU_MAX_SOURCES
#    define MANITOU_MAX_SOURCES 8
#  endif

constexpr int SOURCE_AGE_BINS = 30;
constexpr double STELLAR_AGE_CUTOFF_MA = 30.;
constexpr double ESCAPE_FRACTION = 0.01;
constexpr double BIN_WIDTH_MA = STELLAR_AGE_CUTOFF_MA / SOURCE_AGE_BINS;

/** Lookup value in starburst stellar spectra lookup table
 *
 * For each age bin, the spectra is given as 3 power laws
 * age alpha L0 alpha L0 alpha L0
 */
static float get_table_element(int table_idx, int k) { return _source_table[k * 100 + table_idx]; }

static float get_table_alpha(int table_idx, int frequency_bin) {
  return get_table_element(table_idx, 1 + 2 * frequency_bin);
}

static float get_table_L_0(int table_idx, int frequency_bin) {
  return get_table_element(table_idx, 2 + 2 * frequency_bin);
}

static rt_float RT_Luminosity_starburst(RT_LUMINOSITY_ARGS) {
  int frequency_bin;
  if (nu < Constants->nu_1) {
    frequency_bin = 0;
  } else if (nu < Constants->nu_2) {
    frequency_bin = 1;
  } else {
    frequency_bin = 2;
  }

  const rt_float mass_unit_1e6msol = (All.UnitMass_in_g * 1e-6) / (All.HubbleParam * SOLAR_MASS);

  rt_float L_nu = 0;
  for (int i = 0; i < SOURCE_AGE_BINS; ++i) {
    rt_float mass = Source->source_table[i] * mass_unit_1e6msol;
    rt_float alpha = get_table_alpha(i * 2, frequency_bin);
    rt_float L_0 = get_table_L_0(i * 2, frequency_bin);
    if (L_0 <= 0) {
      continue;
    }
    rt_float lum = L_0 * std::pow(nu / Constants->nu_0, alpha);
    L_nu += lum * (mass * ESCAPE_FRACTION);
  }
  return L_nu;
}

float stellarMassInGroup[MANITOU_MAX_SOURCES][SOURCE_AGE_BINS];

struct group_stellar_properties {
  double young_mass_bins[SOURCE_AGE_BINS] = {0};
  double old_mass = 0.;                     // Mass of old stars
  double luminosity = 0.;                   // Integrated ionising radiation
  const group_properties *group = nullptr;  // Associated group
};

static std::vector<group_stellar_properties> calc_stellar_properties(
    const fof_particle_list *PList,
    const std::vector<const group_properties *> &groups) {
  std::vector<group_stellar_properties> group_masses(groups.size());

  // For each star in a group, assign stellar mass bins
  for (size_t i = 0; i < groups.size(); i++) {
    const auto &group = *(groups[i]);
    const auto *particle =
        std::lower_bound(PList, PList + NumPart, group.MinID, [](const fof_particle_list &part, const MyIDType MinID) {
          return part.MinID < MinID;
        });

    for (; particle < PList + NumPart && particle->MinID == group.MinID; ++particle) {
      if (P[particle->Pindex].Type != 4) { /* Stars only */
        continue;
      }

      auto idx = particle->Pindex;
      const double age_Ma = evaluate_stellar_age_Gyr(P[idx].StellarAge) * 1000.;
      if (age_Ma > STELLAR_AGE_CUTOFF_MA) {
        group_masses[i].old_mass += P[idx].Mass;
      } else {
        const long age_bin = std::lround(age_Ma / BIN_WIDTH_MA);
        group_masses[i].young_mass_bins[age_bin] += P[idx].Mass;
      }
    }

    group_masses[i].group = groups[i];
  }

  // Limits to the different bands of the frequency integral
  const double nu[4] = {_Constants.nu_0, _Constants.nu_1, _Constants.nu_2, std::numeric_limits<double>::infinity()};

  // Find integrated ionising luminosity for each group
  double weights[SOURCE_AGE_BINS] = {0};
  for (size_t iMass = 0; iMass < SOURCE_AGE_BINS; ++iMass) {
    for (size_t iFreq = 0; iFreq < 3; ++iFreq) {
      double alpha = get_table_alpha(iMass * 2, iFreq);
      double L_0 = get_table_L_0(iMass * 2, iFreq);

      // For very old stars, there is so little emission in the HeII-ionising
      // band that the fit goes bad and doesn't vanish at infinity. Just ignore
      // these cases, the luminosity is essentially 0 anyway.
      if (iFreq == 2 && alpha >= -1.0) {
        assert(L_0 < 1.);  // Luminosity is negligible
        weights[iMass] = 0.;
        continue;
      }

      assert(std::abs(alpha + 1.) > 1e-8);  // integral is not valid for alpha == -1
      double w = L_0 / (std::pow(_Constants.nu_0, alpha) * (alpha + 1.));
      w *= std::pow(nu[iFreq + 1], alpha + 1.) - std::pow(nu[iFreq], alpha + 1.);
      w *= ESCAPE_FRACTION;
      assert(w >= 0. && std::isfinite(w));

      weights[iMass] += w;
    }
  }

  const double mass_unit_1e6msol = (All.UnitMass_in_g * 1e-6) / (All.HubbleParam * SOLAR_MASS);
  for (auto &group : group_masses) {
    group.luminosity = 0.;
    for (size_t iMass = 0; iMass < SOURCE_AGE_BINS; ++iMass) {
      group.luminosity += group.young_mass_bins[iMass] * weights[iMass];
    }
    group.luminosity *= mass_unit_1e6msol;
  }

  return group_masses;
}

void ert_set_sources_from_fof(const fof_particle_list *PList) {
  std::vector<const group_properties *> groups(Ngroups);
  std::iota(begin(groups), end(groups), Group);

  // Remove any groups without stars
  groups.erase(
      std::remove_if(begin(groups), end(groups), [](const group_properties *a) { return a->MassType[4] == 0.; }),
      end(groups));

  // Find the highest luminosity groups
  auto stellar_prop = calc_stellar_properties(PList, groups);
  auto num_sources = std::min(groups.size(), size_t{MANITOU_MAX_SOURCES});
  std::nth_element(begin(stellar_prop),
                   begin(stellar_prop) + num_sources - 1,
                   end(stellar_prop),
                   [](const group_stellar_properties &a, const group_stellar_properties &b) {
                     // Lexicographic sort by 1. highest luminosity 2. most old stars and 3. least gas
                     if (a.luminosity != b.luminosity) {
                       return a.luminosity > b.luminosity;
                     }
                     if (a.old_mass != b.old_mass) {
                       return a.old_mass > b.old_mass;
                     }
                     return a.group->MassType[0] < b.group->MassType[0];
                   });

  for (size_t iGroup = 0; iGroup < num_sources; ++iGroup) {
    groups[iGroup] = stellar_prop[iGroup].group;
    std::copy_n(&stellar_prop[iGroup].young_mass_bins[0], SOURCE_AGE_BINS, &stellarMassInGroup[iGroup][0]);
  }

  // Collect some stats on fraction of luminosity we capture
  double missing_luminosity = 0.;
  for (size_t i = num_sources; i < stellar_prop.size(); ++i) {
    missing_luminosity += stellar_prop[i].luminosity;
  }
  double accounted_luminosity = 0.;
  for (size_t i = 0; i < num_sources; ++i) {
    accounted_luminosity += stellar_prop[i].luminosity;
  }
  const double total_luminosity = missing_luminosity + accounted_luminosity;

  printf(
      "Manitou: creating %zu sources from %d groups (%zu with stars) with L=%6g of L_tot=%6g (%.2f %%)\n",
      num_sources,
      Ngroups,
      groups.size(),
      accounted_luminosity,
      total_luminosity,
      100. * (accounted_luminosity / total_luminosity));

  stellar_prop.resize(num_sources);
  groups.resize(num_sources);

  // Update dynamic sources
  _DynamicSources.epoch = All.Ti_Current;
  _DynamicSources.particles.resize(num_sources);
  for (size_t i = 0; i < num_sources; ++i) {
    auto &particle = _DynamicSources.particles[i];
    particle.initial_position = vecFromArray(groups[i]->CM);
    particle.velocity = vecFromArray(groups[i]->Vel);
  }

  // Set RT_SourceT's
  _Source.resize(num_sources);
  for (size_t i = 0; i < num_sources; ++i) {
    for (int j = 0; j < 3; ++j) {
      _Source[i].r[j] = groups[i]->CM[j];
    }
    _Source[i].AttenuationGeometry = SphericalWave;
    _Source[i].SourceType = SourceUser;
    _Source[i].LuminosityFunction = (SourceFunctionT)&RT_Luminosity_starburst;
    _Source[i].source_table = &stellarMassInGroup[i][0];
    // Luminosity used in the RTSources output file only
    _Source[i].L_0 = stellar_prop[i].luminosity;
  }
}

#endif  // MANITOU_FOF_SOURCES
