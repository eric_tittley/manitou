#pragma once
#include <RT_Precision.h>
#include <cudasupport.h>

#include <cstdlib>

// fwd:
struct D_BVH;
struct D_Rays;
struct LinearBVH;

struct RT_SourceT_struct;
using RT_SourceT = RT_SourceT_struct;

struct PointSource {
  float3 origin;
};

struct PlaneSource {
  float3 origin;
  float3 direction;
};

#ifdef SAH_BVH
using TreeType = D_BVH;
#else
using TreeType = LinearBVH;
#endif

#ifdef __CUDACC__
__global__ void genPlaneRays(PlaneSource source,
                             const TreeType &tree,
                             const size_t *ids,
                             size_t num_rays,
                             D_Rays &rays);

__global__ void genPointRays(PointSource source,
                             const TreeType &tree,
                             const size_t *ids,
                             size_t num_rays,
                             D_Rays &rays);
#endif

ERT_HOST int genRtSourceRays(const RT_SourceT &source,
                             const TreeType &tree,
                             const size_t *ids,
                             size_t num_rays,
                             D_Rays &rays,
                             rt_float box_size);
