#pragma once

#ifdef HAVE_CONFIG
#  include "config.h"
#endif
#include "D_BVH.cuh"
#include "D_Rays.cuh"
#include "cudaMemory.h"

#include <memory>

struct dev_bvh_deleter {
  void operator()(D_BVH * tree) {
    freeTree(tree);
    checkCUDAErrors("Free tree failed", __FILE__, __LINE__);
  }
};
using dev_bvh_ptr = std::unique_ptr<D_BVH, dev_bvh_deleter>;

struct dev_rays_deleter {
  void operator()(D_Rays * rays) {
    freeRays(rays);
    checkCUDAErrors("Free rays failed", __FILE__, __LINE__);
  }
};
using dev_rays_ptr = std::unique_ptr<D_Rays, dev_rays_deleter>;

/* All these pointers point to device memory.
 * The structure itself can (and usually?) exists in Host memory.
 */
struct DeviceMemoryPointers {
  cuda_ptr<float3> q; /* quantity to integrate (vector or 3xN array) */
  cuda_ptr<float3> weights; /* integrated value along the ray */
#ifdef SAH_BVH
  cuda_ptr<uint32_t> tree_map; /* Map from BVH tree index to original particle order */
  dev_bvh_ptr tree; /* The Tree */
#endif
  dev_rays_ptr rays; /* The Rays */
};
