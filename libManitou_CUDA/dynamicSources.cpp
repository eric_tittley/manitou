#include "dynamicSources.h"

extern "C" {
#include <proto.h>

#include "RT/RT.h"
}

#include <cassert>

static float time_diff(integertime time0, integertime time1) {
  if (time1 < time0) {
    printf(
        "ERROR: predicting source movement back in time. time0=%lld, time1=%lld\n", (long long)time0, (long long)time1);
    return 0.f;
  }

  if (time1 == time0) {
    return 0.f;
  }

  if (All.ComovingIntegrationOn) {
    return get_drift_factor(time0, time1);
  }

  return (time1 - time0) * All.Timebase_interval;
}

void updateSourcePositions(const DynamicSources& dyn_sources, std::vector<RT_SourceT>& rt_sources, integertime time) {
  const float dt = time_diff(dyn_sources.epoch, time);
  assert(dyn_sources.particles.size() == rt_sources.size());

  for (size_t i = 0; i < dyn_sources.particles.size(); ++i) {
    auto& part = dyn_sources.particles[i];
    auto pos = part.initial_position + part.velocity * dt;

    rt_sources[i].r[0] = pos.x;
    rt_sources[i].r[1] = pos.y;
    rt_sources[i].r[2] = pos.z;
  }
}
