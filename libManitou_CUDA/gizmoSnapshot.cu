#include <string>

#include "Manitou.h"
#include "ERT_gizmo.h"
#include "RT.h"
#include "RT_CUDA.h"
#include "allvars.h"
#include "cudasupport.h"
#include "manitou_vars.h"

char ert_DumpFlag = 0;
int ert_snapshot_num = 0;
namespace {

int CreateSnapshotGizmo(RT_Data &Data_src,
                        RT_RatesBlockT Rates_dev,
                        rt_float Redshift,
                        rt_float t,
                        rt_float outputTimesUnit,
                        const char *Dir) {
  printf("Writing Snapshot...\n");
  rt_float const ExpansionFactor = 1. / (Redshift + 1.);
  size_t const LABEL_STR_SIZE = 32;
  char RTFile[LABEL_STR_SIZE];
  rt_float const t_outputTimeUnits = t / outputTimesUnit;

  int ierr;
  ierr = snprintf(RTFile, LABEL_STR_SIZE - 1, "%s/RTData_t=%07.3f", Dir, t_outputTimeUnits);
  if (ierr > (int)LABEL_STR_SIZE) {
    printf("ERROR: %s: %i: Path too long %s\n", __FILE__, __LINE__, RTFile);
    return EXIT_FAILURE;
  }

  RT_FileHeaderT FileHeader;
  FileHeader.ExpansionFactor = (float)ExpansionFactor;
  FileHeader.Redshift = (float)header.redshift;
  FileHeader.Time = (float)t;
  RT_Data *Data_tmp_p = &Data_src;
  RT_SaveData(RTFile, &Data_tmp_p, 1, FileHeader);

#if 0 /* I don't think this works for multiple GPUs, but not sure why */
  /* * Save Rates to file * */
  /*   Allocate host memory for Rates   */
  RT_RatesBlockT Rates_host;
  Rates_host.NumCells = Data_src.NumCells;
  ierr = RT_RatesBlock_Allocate(Data_src.NumCells, &Rates_host);
  if (ierr != EXIT_SUCCESS) {
    printf("ERROR: %s: %i: Failed to allocate host memory for Rates\n", __FILE__, __LINE__);
    return EXIT_FAILURE;
  }
  /*   Copy Rates from device to host   */
  RT_CUDA_CopyRatesDeviceToHost(Rates_dev, Rates_host);

  /*   Write Rates to file              */
  ierr = snprintf(RTFile, LABEL_STR_SIZE - 1, "%s/Rates_t=%07.3f", Dir, t_outputTimeUnits);
  if (ierr > (int)LABEL_STR_SIZE) {
    printf("ERROR: %s: %i: Path too long %s\n", __FILE__, __LINE__, RTFile);
    return EXIT_FAILURE;
  }
  FILE *fid = fopen(RTFile, "w");
  if (fid == NULL) {
    printf("ERROR: %s: %i: Unable to open file %s\n", __FILE__, __LINE__, RTFile);
    ReportFileError(RTFile);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
  ierr = RT_RatesBlock_Write(Rates_host, fid);
  if (ierr != EXIT_SUCCESS) {
    printf("ERROR: %s: %i: Failed to write Rates to file %s\n", __FILE__, __LINE__, RTFile);
    return EXIT_FAILURE;
  }
  /* Close the file */
  ierr = fclose(fid);
  if (ierr != 0) {
    printf("ERROR: %s: Unable to close file %s\n", __FILE__, RTFile);
    ReportFileError(RTFile);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }

  /*   Free local Rates memory          */
  RT_RatesBlock_Free(Rates_host);
#endif /* 0 */

  /* Print Time Array */
  double SumKnownTimes = 0.0;
  double TotalTime = _TimerArray[CumulativeTime];
  for (size_t i = 1; i < nCodeTimes; ++i) {
    SumKnownTimes += _TimerArray[i];
  }
  printf("#########################################\n");
  printf("##### Manitou Profile for t=%07.3f #####\n", t_outputTimeUnits);
  printf("#########################################\n");
  printf("# Total Time in Manitou = %9.1fs    #\n", TotalTime);
  printf("# Creating Spheres      = %7.4f %%     #\n", 100.0 * _TimerArray[CreatingSpheres] / TotalTime);
  printf("# Tree Creation time    = %7.4f %%     #\n", 100.0 * _TimerArray[CreatingTree] / TotalTime);
  printf("# Updating From Gizmo   = %7.4f %%     #\n", 100.0 * _TimerArray[UpdatingFromGizmo] / TotalTime);
  printf("# Ray Creation time     = %7.4f %%     #\n", 100.0 * _TimerArray[CreatingRays] / TotalTime);
  printf("# Column Densities time = %7.4f %%     #\n", 100.0 * _TimerArray[ColumnDensities] / TotalTime);
  printf("# Ionization Rates      = %7.4f %%     #\n", 100.0 * _TimerArray[IonizationRates] / TotalTime);
  printf("# Recombinations        = %7.4f %%     #\n", 100.0 * _TimerArray[Recombinations] / TotalTime);
  printf("# Extra Cooling         = %7.4f %%     #\n", 100.0 * _TimerArray[ExtraCooling] / TotalTime);
  printf("# Update Heating Rates  = %7.4f %%     #\n", 100.0 * _TimerArray[UpdateHeatingRates] / TotalTime);
  printf("# TimeStep              = %7.4f %%     #\n", 100.0 * _TimerArray[TimeStep] / TotalTime);
  printf("# Updating              = %7.4f %%     #\n", 100.0 * _TimerArray[Updating] / TotalTime);
  printf("# Setting Equil. values = %7.4f %%     #\n", 100.0 * _TimerArray[EquilibriumValues] / TotalTime);
  printf("# Device Memory IO      = %7.4f %%     #\n", 100.0 * _TimerArray[MemCopy] / TotalTime);
  printf("# Updating To Gizmo     = %7.4f %%     #\n", 100.0 * _TimerArray[UpdatingToGizmo] / TotalTime);
  printf("# Other                 = %7.4f %%     #\n", 100.0 * (TotalTime - SumKnownTimes) / TotalTime);
  printf("#########################################\n");

  printf("Finished writing Snapshot\n");
  return EXIT_SUCCESS;
}

template <typename... Args>
std::string safe_sprintf(const char *format, const Args &...args) {
  int size = snprintf(nullptr, 0, format, args...);
  std::string res(size + 1, ' ');
  size = snprintf(&res[0], res.size(), format, args...);
  res.resize(size);
  return res;
}

void write_sources_file(const std::vector<RT_SourceT> &Sources, int FileNum, const char *Dir) {
  auto Filename = safe_sprintf("%s/RTSources_%03d.csv", Dir, FileNum);
  FILE *File = fopen(Filename.c_str(), "w");
  if (!File) {
    printf("ERROR: Failed to open RTSources file:\n\t%s", Filename.c_str());
    return;
  }

  for (auto &Src : Sources) {
    fprintf(File, "%0.9g,%0.9g,%0.9g,%0.9g,%0.9g\n", Src.r[0], Src.r[1], Src.r[2], Src.z_on, Src.L_0);
  }
  fclose(File);
}

}  // namespace

void ert_write_snapshot(int snapshot_num) {
  rt_float Redshift = 1.0 / (All.Time) - 1;
  rt_float t = UNIT_TIME_IN_CGS;
  const char Dir[] = "./save";
  rt_float outputTimesUnit = 3.15576e13;

  int ierr = CreateSnapshotGizmo(_Data, _Rates_dev, Redshift, t, outputTimesUnit, Dir);
  write_sources_file(_Source, snapshot_num, Dir);
}
