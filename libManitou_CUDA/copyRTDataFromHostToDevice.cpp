#include <cuda_runtime.h>

#include "Manitou_CUDA.h"
#include "cudasupport.h"

void copyRTDataFromHostToDevice(RT_Data const Data_src, RT_Data* const Data_dest) {
  bool const TerminateOnCudaError = true;

  cudaError_t cudaError;
  size_t const sizeOfHostMemSpace = Data_src.NBytes; /* bytes */
  cudaError = cudaMalloc(&(Data_dest->MemSpace), sizeOfHostMemSpace);
  checkCudaAPICall(cudaError, __FILE__, __LINE__, TerminateOnCudaError);

  cudaError = cudaMemcpy(Data_dest->MemSpace, Data_src.MemSpace, sizeOfHostMemSpace, cudaMemcpyHostToDevice);
  checkCudaAPICall(cudaError, __FILE__, __LINE__, TerminateOnCudaError);
  Data_dest->NBytes = Data_src.NBytes;
  Data_dest->NumCells = Data_src.NumCells; /* == nParticles */
  /* Set the pointers in Data_dev_RT */
  RT_Data_Assign(Data_dest);
  cudaDeviceSynchronize();
}
