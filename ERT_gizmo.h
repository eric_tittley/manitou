#pragma once

#ifdef __cplusplus
#  define EXTERNC extern "C"
#else
#  define EXTERNC
#endif

// forward declaration from structure/fof.h
struct fof_particle_list;

EXTERNC void ert_parent_routine(void);

EXTERNC void ert_startup_routine(void);

EXTERNC void ert_write_snapshot(int snapshot_num);

EXTERNC void ert_set_sources_from_fof(const struct fof_particle_list* PList);

EXTERNC void ert_set_sources_from_stars(void);