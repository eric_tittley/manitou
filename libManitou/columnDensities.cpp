/*
 * ARGUMENTS
 *  ParticlesToUpdateColumnDensities List of particles to updated column
 *                                   densities. In original (Gadget) order.
 */

/* Compile-time configuration */
#include <stdio.h>
#include <stdlib.h>

#include "config.h"
#ifdef DEBUG
#  include <cmath>
#endif

#include "BVH.h"
#include "Epsilon.h"
#include "RT_Data.h"
#include "Ray.h"
#include "columnDensities.h"

#define OMP_CHUNK_SIZE 2048

#ifndef CUDA_RTSPH
#  define RTSPH_FLOAT double
#endif

int cD_ProcessSpecies(size_t const nParticlesTotal,
                      size_t const *const ParticlesToUpdateColumnDensities,
                      double const NumberOfAtomsPerParticle,
                      rt_float const UnitLength,
                      BVH *tree,
                      Rays const *rays,
                      rt_float const *const QuantityToIntegrate,
                      rt_float *const Destination) {
  /* rays.count should equal NumParticlesToUpdateColumnDensities */
  size_t const nParticlesToUpdate = (size_t)rays->count();

  double *TemporaryStorageOfUnsortedColumnDensities = new double[nParticlesToUpdate];

  /* if q is set to a mass (kg), a mass column density is returned (kg/(units of h)^2) */
  RTSPH_FLOAT *q = new RTSPH_FLOAT[nParticlesTotal];

#pragma omp parallel for
  for (size_t iParticle = 0; iParticle < nParticlesTotal; ++iParticle) {
    q[iParticle] = (RTSPH_FLOAT)QuantityToIntegrate[tree->map[iParticle]];
  }

#pragma omp parallel for default(none) shared(rays, tree, q, TemporaryStorageOfUnsortedColumnDensities) \
    schedule(dynamic, OMP_CHUNK_SIZE)
  /* A note about OpenMP scheduling:
   *  Because the particles for which RecalculateColumnDensityFlag is true
   *  are correlated, static scheduling can be a bad idea if one or a few threads
   *  get all the particles that need column densities.
   *  Better solutions might be dynamic with a large enough chunk size to
   *  prevent the thread overhead from dominating but small enough that a single
   *  thread won't get too many corelated groups of particles.
   */
  for (size_t iray = 0; iray < rays->count(); ++iray) {
    // fetch ray
    Ray ray;
    rays->get(iray, ray);
    // calculate column density
    TemporaryStorageOfUnsortedColumnDensities[iray] = tree->weightRay(ray, q);
#ifdef DEBUG
    if (!std::isfinite(TemporaryStorageOfUnsortedColumnDensities[iray]) ||
        TemporaryStorageOfUnsortedColumnDensities[iray] < -FLOAT_MIN) {
      printf("ERROR: %s: %i: TemporaryStorageOfUnsortedColumnDensities[%i]=%5.3e\n",
             __FILE__,
             __LINE__,
             (int)iray,
             TemporaryStorageOfUnsortedColumnDensities[iray]);
    }
#endif
  }
#ifdef DEBUG
  for (size_t iParticle = 0; iParticle < nParticlesToUpdate; ++iParticle) {
    if (!std::isfinite(TemporaryStorageOfUnsortedColumnDensities[iParticle]) ||
        TemporaryStorageOfUnsortedColumnDensities[iParticle] < -FLOAT_MIN) {
      printf("ERROR: %s: %i: TemporaryStorageOfUnsortedColumnDensities[%i]=%5.3e\n",
             __FILE__,
             __LINE__,
             (int)iParticle,
             TemporaryStorageOfUnsortedColumnDensities[iParticle]);
      return EXIT_FAILURE;
    }
  }
#endif
  double const ScaleFactor = NumberOfAtomsPerParticle / (double)UnitLength / (double)UnitLength;
#pragma omp parallel for default(none) schedule(dynamic, OMP_CHUNK_SIZE) \
    shared(tree,                                                         \
           TemporaryStorageOfUnsortedColumnDensities,                    \
           nParticlesToUpdate,                                           \
           ScaleFactor,                                                  \
           Destination,                                                  \
           ParticlesToUpdateColumnDensities)
  for (size_t iParticle = 0; iParticle < nParticlesToUpdate; ++iParticle) {
    Destination[ParticlesToUpdateColumnDensities[iParticle]] =
        (rt_float)((double)TemporaryStorageOfUnsortedColumnDensities[iParticle] * ScaleFactor);
  }
#ifdef DEBUG
  for (size_t iParticle = 0; iParticle < nParticlesToUpdate; ++iParticle) {
    size_t const particleID = ParticlesToUpdateColumnDensities[iParticle];
    if (!std::isfinite(Destination[particleID]) || Destination[particleID] < -FLOAT_MIN) {
      printf("ERROR: %s: %i: Destination[%i]=%5.3e", __FILE__, __LINE__, (int)iParticle, Destination[particleID]);
      printf(" TemporaryStorageOfUnsortedColumnDensities=%5.3e; NumberOfAtomsPerParticle=%5.3e; UnitLength=%5.3e\n",
             TemporaryStorageOfUnsortedColumnDensities[iParticle],
             NumberOfAtomsPerParticle,
             UnitLength);
      return EXIT_FAILURE;
    }
  }
#endif
  delete q;
  delete TemporaryStorageOfUnsortedColumnDensities;

  return EXIT_SUCCESS;
}

int columnDensities(size_t const *const ParticlesToUpdateColumnDensities,
                    double const NumberOfHydrogenAtomsPerParticle,
                    double const NumberOfHeliumAtomsPerParticle,
                    rt_float const UnitLength,
                    BVH *tree,
                    Rays const *rays,
                    RT_Data const Data) {
  size_t const nParticlesTotal = Data.NumCells;
  int ierr;

  /* H1 */
  ierr = cD_ProcessSpecies(nParticlesTotal,
                           ParticlesToUpdateColumnDensities,
                           NumberOfHydrogenAtomsPerParticle,
                           UnitLength,
                           tree,
                           rays,
                           Data.f_H1,
                           Data.column_H1);
  if (ierr != EXIT_SUCCESS) {
    printf("ERROR: %s: %i: Failed to calculate H1 column densities\n", __FILE__, __LINE__);
    return EXIT_FAILURE;
  }

  if (NumberOfHeliumAtomsPerParticle > 0.0) {
    /* If there is any Helium... */

    /* He1 */
    ierr = cD_ProcessSpecies(nParticlesTotal,
                             ParticlesToUpdateColumnDensities,
                             NumberOfHeliumAtomsPerParticle,
                             UnitLength,
                             tree,
                             rays,
                             Data.f_He1,
                             Data.column_He1);
    if (ierr != EXIT_SUCCESS) {
      printf("ERROR: %s: %i: Failed to calculate H1 column densities\n", __FILE__, __LINE__);
      return EXIT_FAILURE;
    }

    /* He2 */
    ierr = cD_ProcessSpecies(nParticlesTotal,
                             ParticlesToUpdateColumnDensities,
                             NumberOfHeliumAtomsPerParticle,
                             UnitLength,
                             tree,
                             rays,
                             Data.f_He2,
                             Data.column_He2);
    if (ierr != EXIT_SUCCESS) {
      printf("ERROR: %s: %i: Failed to calculate H1 column densities\n", __FILE__, __LINE__);
      return EXIT_FAILURE;
    }
  } else {
    /* No Helium */
    printf("NO HELIUM. Skipping helium column densities\n");
  }

  return EXIT_SUCCESS;
}
