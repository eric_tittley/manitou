#include "Manitou.h"
#include "RT.h"

void resetEntropyAndTemperature(rt_float const Temperature, RT_ConstantsT const Constants, RT_Data const Data) {
  size_t const nParticles = Data.NumCells;

  for (size_t cellindex = 0; cellindex < nParticles; ++cellindex) {
    rt_float const Entropy = RT_EntropyFromTemperature(Constants, Temperature, cellindex, Data);
    Data.Entropy[cellindex] = Entropy;
  }
  for (size_t cellindex = 0; cellindex < nParticles; ++cellindex) {
    Data.T[cellindex] = Temperature;
  }
}
