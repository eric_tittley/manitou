#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "RT_Precision.h"

extern "C" {
#include "libPF/PF.h"
}

#include "Parameters.h"

int readParameters(char const *const ParametersFile, ParametersT *const Parameters) {
  int ierr;
  PF_ParameterEntry *ParameterEntries = NULL;
  FILE *File;

  /* Set defaults */
  Parameters->ProblemType = 0;
  Parameters->GadgetFile = (char *)malloc(FILENAME_MAX);
  Parameters->GadgetFile[0] = '\0'; /* Initialize to blank */
  Parameters->InitialTemperature = 100.; /* K */
  Parameters->t_start = 0.;              /* s */
  Parameters->t_stop = 3600. * 24. * 365.25 * 1e6;
  Parameters->Nsph = 64;
  Parameters->nSources = 1;
  Parameters->columnDensityThreshold_H1 = 1e25;  /* m^2 */
  Parameters->columnDensityThreshold_He1 = 1e25; /* m^2 */
  Parameters->columnDensityThreshold_He2 = 1e25; /* m^2 */
  Parameters->RecalculateColumnDensityIterationCount = 10;
  Parameters->totalSourceLuminosity = 1e23; /* W / Hz @ nu_0 */
  Parameters->HaloCatalogueFile = (char *)malloc(FILENAME_MAX);
  Parameters->HaloCatalogueFile[0] = '\0'; /* Initialize to blank */
  Parameters->NElements_InitialFractions = 5;
  Parameters->InitialFractions = (double *)malloc(sizeof(double) * Parameters->NElements_InitialFractions);
  Parameters->InitialFractions[0] = 1.0;
  Parameters->InitialFractions[1] = 0.0;
  Parameters->InitialFractions[2] = 1.0;
  Parameters->InitialFractions[3] = 0.0;
  Parameters->InitialFractions[4] = 0.0;
  Parameters->WallTimeLimit = 0;
  Parameters->RestartFlag = 0;
  Parameters->RestartCommand = (char *)malloc(FILENAME_MAX);
  Parameters->RestartCommand[0] = '\0'; /* Initialize to blank */
  /* Non-zero OutputIterationCount overrules any OutputTimesFile. */
  Parameters->OutputIterationCount = 0;
  Parameters->OutputTimesFile = (char *)malloc(FILENAME_MAX);
  Parameters->OutputTimesFile[0] = '\0'; /* Initialize to blank */

  /* Allocate memory */
  ParameterEntries = (PF_ParameterEntry *)malloc(sizeof(PF_ParameterEntry) * nParameters);

  /* Compile ParameterEntries array of structures */

  strncpy(ParameterEntries[iProblemType].Parameter, "ProblemType", MAX_PARAMETER_NAME_LENGTH);
  ParameterEntries[iProblemType].Type = UNSIGNED_INTEGER;
  ParameterEntries[iProblemType].Pointer = &(Parameters->ProblemType);
  ParameterEntries[iProblemType].IsBoolean = 0;
  ParameterEntries[iProblemType].IsArray = 0;

  strncpy(ParameterEntries[iGadgetFile].Parameter, "GadgetFile", MAX_PARAMETER_NAME_LENGTH);
  ParameterEntries[iGadgetFile].Type = STRING;
  ParameterEntries[iGadgetFile].Pointer = Parameters->GadgetFile;
  ParameterEntries[iGadgetFile].IsBoolean = 0;
  ParameterEntries[iGadgetFile].IsArray = 0;

  strncpy(ParameterEntries[iInitialTemperature].Parameter, "InitialTemperature", MAX_PARAMETER_NAME_LENGTH);
  ParameterEntries[iInitialTemperature].Type = RT_FLOAT;
  ParameterEntries[iInitialTemperature].Pointer = &(Parameters->InitialTemperature);
  ParameterEntries[iInitialTemperature].IsBoolean = 0;
  ParameterEntries[iInitialTemperature].IsArray = 0;

  strncpy(ParameterEntries[it_start].Parameter, "t_start", MAX_PARAMETER_NAME_LENGTH);
  ParameterEntries[it_start].Type = RT_FLOAT;
  ParameterEntries[it_start].Pointer = &(Parameters->t_start);
  ParameterEntries[it_start].IsBoolean = 0;
  ParameterEntries[it_start].IsArray = 0;

  strncpy(ParameterEntries[it_stop].Parameter, "t_stop", MAX_PARAMETER_NAME_LENGTH);
  ParameterEntries[it_stop].Type = RT_FLOAT;
  ParameterEntries[it_stop].Pointer = &(Parameters->t_stop);
  ParameterEntries[it_stop].IsBoolean = 0;
  ParameterEntries[it_stop].IsArray = 0;

  strncpy(ParameterEntries[iNsph].Parameter, "Nsph", MAX_PARAMETER_NAME_LENGTH);
  ParameterEntries[iNsph].Type = INTEGER;
  ParameterEntries[iNsph].Pointer = &(Parameters->Nsph);
  ParameterEntries[iNsph].IsBoolean = 0;
  ParameterEntries[iNsph].IsArray = 0;

  strncpy(ParameterEntries[inSources].Parameter, "nSources", MAX_PARAMETER_NAME_LENGTH);
  ParameterEntries[inSources].Type = UNSIGNED_LONG_INTEGER;
  ParameterEntries[inSources].Pointer = &(Parameters->nSources);
  ParameterEntries[inSources].IsBoolean = 0;
  ParameterEntries[inSources].IsArray = 0;

  strncpy(
      ParameterEntries[icolumnDensityThreshold_H1].Parameter, "columnDensityThreshold_H1", MAX_PARAMETER_NAME_LENGTH);
  ParameterEntries[icolumnDensityThreshold_H1].Type = RT_FLOAT;
  ParameterEntries[icolumnDensityThreshold_H1].Pointer = &(Parameters->columnDensityThreshold_H1);
  ParameterEntries[icolumnDensityThreshold_H1].IsBoolean = 0;
  ParameterEntries[icolumnDensityThreshold_H1].IsArray = 0;

  strncpy(
      ParameterEntries[icolumnDensityThreshold_He1].Parameter, "columnDensityThreshold_He1", MAX_PARAMETER_NAME_LENGTH);
  ParameterEntries[icolumnDensityThreshold_He1].Type = RT_FLOAT;
  ParameterEntries[icolumnDensityThreshold_He1].Pointer = &(Parameters->columnDensityThreshold_He1);
  ParameterEntries[icolumnDensityThreshold_He1].IsBoolean = 0;
  ParameterEntries[icolumnDensityThreshold_He1].IsArray = 0;

  strncpy(
      ParameterEntries[icolumnDensityThreshold_He2].Parameter, "columnDensityThreshold_He2", MAX_PARAMETER_NAME_LENGTH);
  ParameterEntries[icolumnDensityThreshold_He2].Type = RT_FLOAT;
  ParameterEntries[icolumnDensityThreshold_He2].Pointer = &(Parameters->columnDensityThreshold_He2);
  ParameterEntries[icolumnDensityThreshold_He2].IsBoolean = 0;
  ParameterEntries[icolumnDensityThreshold_He2].IsArray = 0;

  strncpy(ParameterEntries[iRecalculateColumnDensityIterationCount].Parameter,
          "RecalculateColumnDensityIterationCount",
          MAX_PARAMETER_NAME_LENGTH);
  ParameterEntries[iRecalculateColumnDensityIterationCount].Type = UNSIGNED_INTEGER;
  ParameterEntries[iRecalculateColumnDensityIterationCount].Pointer =
      &(Parameters->RecalculateColumnDensityIterationCount);
  ParameterEntries[iRecalculateColumnDensityIterationCount].IsBoolean = 0;
  ParameterEntries[iRecalculateColumnDensityIterationCount].IsArray = 0;

  strncpy(ParameterEntries[itotalSourceLuminosity].Parameter, "totalSourceLuminosity", MAX_PARAMETER_NAME_LENGTH);
  ParameterEntries[itotalSourceLuminosity].Type = RT_FLOAT;
  ParameterEntries[itotalSourceLuminosity].Pointer = &(Parameters->totalSourceLuminosity);
  ParameterEntries[itotalSourceLuminosity].IsBoolean = 0;
  ParameterEntries[itotalSourceLuminosity].IsArray = 0;

  strncpy(ParameterEntries[iHaloCatalogueFile].Parameter, "HaloCatalogueFile", MAX_PARAMETER_NAME_LENGTH);
  ParameterEntries[iHaloCatalogueFile].Type = STRING;
  ParameterEntries[iHaloCatalogueFile].Pointer = Parameters->HaloCatalogueFile;
  ParameterEntries[iHaloCatalogueFile].IsBoolean = 0;
  ParameterEntries[iHaloCatalogueFile].IsArray = 0;

  strncpy(ParameterEntries[iInitialFractions].Parameter, "InitialFractions", MAX_PARAMETER_NAME_LENGTH);
  ParameterEntries[iInitialFractions].Type = DOUBLE;
  ParameterEntries[iInitialFractions].Pointer = &(Parameters->InitialFractions);
  ParameterEntries[iInitialFractions].IsBoolean = 0;
  ParameterEntries[iInitialFractions].IsArray = 1;
  ParameterEntries[iInitialFractions].NArrayElements = &(Parameters->NElements_InitialFractions);

  strncpy(ParameterEntries[iWallTimeLimit].Parameter, "WallTimeLimit", MAX_PARAMETER_NAME_LENGTH);
  ParameterEntries[iWallTimeLimit].Type = UNSIGNED_INTEGER;
  ParameterEntries[iWallTimeLimit].Pointer = &(Parameters->WallTimeLimit);
  ParameterEntries[iWallTimeLimit].IsBoolean = 0;
  ParameterEntries[iWallTimeLimit].IsArray = 0;

  strncpy(ParameterEntries[iRestartFlag].Parameter, "RestartFlag", MAX_PARAMETER_NAME_LENGTH);
  ParameterEntries[iRestartFlag].Type = UNSIGNED_INTEGER;
  ParameterEntries[iRestartFlag].Pointer = &(Parameters->RestartFlag);
  ParameterEntries[iRestartFlag].IsBoolean = 0;
  ParameterEntries[iRestartFlag].IsArray = 0;

  strncpy(ParameterEntries[iRestartCommand].Parameter, "RestartCommand", MAX_PARAMETER_NAME_LENGTH);
  ParameterEntries[iRestartCommand].Type = STRING;
  ParameterEntries[iRestartCommand].Pointer = Parameters->RestartCommand;
  ParameterEntries[iRestartCommand].IsBoolean = 0;
  ParameterEntries[iRestartCommand].IsArray = 0;

  strncpy(ParameterEntries[iOutputIterationCount].Parameter, "OutputIterationCount", MAX_PARAMETER_NAME_LENGTH);
  ParameterEntries[iOutputIterationCount].Type = UNSIGNED_LONG_INTEGER;
  ParameterEntries[iOutputIterationCount].Pointer = &(Parameters->OutputIterationCount);
  ParameterEntries[iOutputIterationCount].IsBoolean = 0;
  ParameterEntries[iOutputIterationCount].IsArray = 0;

  strncpy(ParameterEntries[iOutputTimesFile].Parameter, "OutputTimesFile", MAX_PARAMETER_NAME_LENGTH);
  ParameterEntries[iOutputTimesFile].Type = STRING;
  ParameterEntries[iOutputTimesFile].Pointer = Parameters->OutputTimesFile;
  ParameterEntries[iOutputTimesFile].IsBoolean = 0;
  ParameterEntries[iOutputTimesFile].IsArray = 0;

  /* Open Parameters file for reading */
  File = fopen(ParametersFile, "r");
  if (File == NULL) {
    printf("%s: %i: ERROR: failed to load file %s\n", __FILE__, __LINE__, ParametersFile);
    return EXIT_FAILURE;
  }

  /* Read the Parameters */
  ierr = PF_ReadParameterFile(File, ParameterEntries, nParameters);
  if (ierr != EXIT_SUCCESS) {
    printf("%s: %i: ERROR: PF_ReadParameterFile failed\n", __FILE__, __LINE__);
    return EXIT_FAILURE;
  }

  /* Print the Parameters using PF_WriteParameters */
  ierr = PF_WriteParameters(ParameterEntries, nParameters);
  if (ierr != EXIT_SUCCESS) {
    printf("%s: %i: ERROR: PF_WriteParameters failed\n", __FILE__, __LINE__);
    return EXIT_FAILURE;
  }

  /* Free the structure */
  free(ParameterEntries);

  /* Close the file */
  fclose(File);

  return EXIT_SUCCESS;
}
