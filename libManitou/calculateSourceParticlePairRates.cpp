#include "Manitou.h"

int calculateSourceParticlePairRates(RT_Data const Data,
                                     ParametersT const Parameters,
                                     RT_ConstantsT const Constants,
                                     rt_float const z_source,
                                     RT_SourceT const Source,
                                     RT_IntegralInvolatilesT const Involatiles,
                                     /* Incremented */ RT_RatesBlockT const Rates,
                                     /*     Updated */ unsigned int* const RecalculateColumnDensityFlag) {
  size_t const nParticles = Data.NumCells;

  bool ErrorFlag = false;

#pragma omp parallel for default(none) reduction(||                               \
                                                 : ErrorFlag) shared(stdout,      \
                                                                     nParticles,  \
                                                                     Data,        \
                                                                     Parameters,  \
                                                                     Constants,   \
                                                                     Involatiles, \
                                                                     Source,      \
                                                                     z_source,    \
                                                                     Rates,       \
                                                                     RecalculateColumnDensityFlag)
  for (size_t iParticle = 0; iParticle < nParticles; ++iParticle) {
    if (Data.column_H1[iParticle] < Parameters.columnDensityThreshold_H1 &&
        Data.column_He1[iParticle] < Parameters.columnDensityThreshold_He1 &&
        Data.column_He2[iParticle] < Parameters.columnDensityThreshold_He2) {
      /* The species number densities. m^-3 */
      rt_float n_H1 = Data.n_H[iParticle] * Data.f_H1[iParticle];
      rt_float n_He1 = Data.n_He[iParticle] * Data.f_He1[iParticle];
      rt_float n_He2 = Data.n_He[iParticle] * Data.f_He2[iParticle];

      /* The column density through this particle (number density)*(size)  m^-2 */
      rt_float const meanPathLengthThroughParticle = 4. / 3. * Data.dR[iParticle];
      rt_float N_H1 = n_H1 * meanPathLengthThroughParticle;
      rt_float N_He1 = n_He1 * meanPathLengthThroughParticle;
      rt_float N_He2 = n_He2 * meanPathLengthThroughParticle;

      rt_float I_H1_from_source;
      rt_float I_He1_from_source;
      rt_float I_He2_from_source;
      rt_float G_H1_from_source;
      rt_float G_He1_from_source;
      rt_float G_He2_from_source;
      rt_float Gamma_HI_from_source;

      rt_float const DistanceFromSourceMpc = Data.R[iParticle] / Constants.Mpc;
      rt_float const ParticleRadiusMpc = Data.dR[iParticle] / Constants.Mpc; /* Not used */
      rt_float const DistanceThroughElementMpc = meanPathLengthThroughParticle / Constants.Mpc;

      /* The contributions to the photoionization rates for this source */
      int ierr = RT_PhotoionizationRates(N_H1,
                                         N_He1,
                                         N_He2,
                                         Data.column_H1[iParticle],
                                         Data.column_He1[iParticle],
                                         Data.column_He2[iParticle],
                                         z_source,
                                         Source,
                                         Constants,
                                         Involatiles,
                                         DistanceFromSourceMpc,
                                         ParticleRadiusMpc, /* Not used */
                                         DistanceThroughElementMpc,
                                         &I_H1_from_source,
                                         &I_He1_from_source,
                                         &I_He2_from_source,
                                         &G_H1_from_source,
                                         &G_He1_from_source,
                                         &G_He2_from_source,
                                         &Gamma_HI_from_source);
      if (ierr == EXIT_FAILURE) {
        printf("ERROR: %s: %i: Failed to calculate rates.\n", __FILE__, __LINE__);
        (void)fflush(stdout);
        ErrorFlag = true;
      }
      /* Add the contributions from this source */
      Rates.I_H1[iParticle] += I_H1_from_source;
      Rates.I_He1[iParticle] += I_He1_from_source;
      Rates.I_He2[iParticle] += I_He2_from_source;
      Rates.G_H1[iParticle] += G_H1_from_source;
      Rates.G_He1[iParticle] += G_He1_from_source;
      Rates.G_He2[iParticle] += G_He2_from_source;
      Rates.Gamma_HI[iParticle] += Gamma_HI_from_source;

      /* Always recalculate column densities in this regime.
       * The 1 is a marker that it must be held. */
      RecalculateColumnDensityFlag[iParticle] = Parameters.RecalculateColumnDensityIterationCount + 1;
    } /* end if column densities are low enough to bother */

  } /* End of loop over all particles */

  for (size_t iParticle = 0; iParticle < 5; ++iParticle) {
    printf("Gamma_HI[%lu]=%5.3e;\n", iParticle, Rates.Gamma_HI[iParticle]);
  }

  if (ErrorFlag) {
    return EXIT_FAILURE;
  } else {
    return EXIT_SUCCESS;
  }
}
