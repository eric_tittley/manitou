#include "Manidoowag.h"
#include "Manitou.h"

/* Forward declaration. Not used elsewhere, so not in header. */
void setSourcePositions(ParametersT const Parameters, rt_float const BoxSize, RT_SourceT *const Source);

void Manitou_SetSources(ParametersT &Parameters,
                        double const BoxSize,
                        RT_ConstantsT const Constants,
                        RT_SourceT *const Source) {
  /* T = 1E5 K blackbody flux in units of _ionizing_ photons s^-1 m^-2.
   * (Derive by integrating planck's law / (h_p * nu) * cos(theta) over
   * frequency and solid angle. cos(theta) term due to Lambert's cosine
   * law. Frequency integral is from nu = nu_0 -> infinity, where n_0 is
   * the ionization threshold of hydrogen; solid angle integral over
   * hemipsphere.
   * The frequency integral from nu_0 -> infinity can be found by
   * computing the integral from 0 -> infinity, which has an analytic
   * solution (~ zeta(3)) and subtracting the integral from 0 -> nu_0,
   * which can be expressed in terms of polylogarithms, but must
   * ultimately be computed numerically.)
   */
  const double Flux1E5BB = 1.067678205e+30;

  switch (Parameters.ProblemType) {
    case 0: /* N sources placed in halos in mass order */
    {
      if (Parameters.nSources <= 0) {
        break;
      }

#if 0
    rt_float luminosityPerSource =   Parameters.totalSourceLuminosity
                                   / ((rt_float)Parameters.nSources);
#endif

      std::cout << "Reading halo catalogue " << Parameters.HaloCatalogueFile << "..." << std::endl;
      /* Read in the halo centres.
       * The file should be a list of halo centres, in order of most
       * massive/dense/whatever first, the first line being the number of centres
       * and the following lines the x y z sets normalized [0,1) */
      rt_float *haloCentres;
      rt_float *Ls;
      size_t const nHalos = readHaloCatalogue(Parameters.HaloCatalogueFile, haloCentres, Ls);
      if (nHalos == 0) {
        std::cerr << "WARNING: " << __FILE__ << ": " << __LINE__ << ": "
                  << " Trouble reading Halo Catalogue." << std::endl
                  << " Either file does not exist or has no sources." << std::endl;
      }
      std::cout << "...done" << std::endl;

      if (Parameters.nSources != nHalos) {
        if (Parameters.nSources > nHalos) {
          std::cerr << "WARNING: expected " << Parameters.nSources << " halos but found " << nHalos
                    << ". Setting Parameters.nSources to " << nHalos << std::endl;
          Parameters.nSources = nHalos;
        } else {
          std::cerr << "ERROR: expected " << Parameters.nSources << " halos but found " << nHalos
                    << ". Please change nSources in Manitou.params." << std::endl;
          exit(-1);
          // At this point, we should free(Source) then malloc(nSources*sizeof(RT_SourceT)) and return this as Source
        }
      }

      /* Set up sources */
      for (size_t iSource = 0; iSource < Parameters.nSources; ++iSource) {
        /*  Power law: nu^-1, at the centre, on between z=8 & z=3, with L=1e23 */
        Source[iSource].r[0] = haloCentres[3 * iSource + 0] * (rt_float)BoxSize; /* Gadget internal units */
        Source[iSource].r[1] = haloCentres[3 * iSource + 1] * (rt_float)BoxSize; /* Gadget internal units */
        Source[iSource].r[2] = haloCentres[3 * iSource + 2] * (rt_float)BoxSize; /* Gadget internal units */
        Source[iSource].SourceType = SourcePowerLawMinusOne;
        Source[iSource].z_on = 8000.0;
        Source[iSource].z_off = 0.0;
#if 0
     Source[iSource].L_0   = luminosityPerSource; /* W / Hz @ nu_0 */
#else
        Source[iSource].L_0 = Ls[iSource];
#endif
        Source[iSource].AttenuationGeometry = SphericalWave;
      }
      free(haloCentres);
      free(Ls);
    } break;

    case 1:                                     /* Test 1 */
      Source[0].r[0] = 0.5 * (rt_float)BoxSize; /* Gadget internal units */
      Source[0].r[1] = 0.5 * (rt_float)BoxSize; /* Gadget internal units */
      Source[0].r[2] = 0.5 * (rt_float)BoxSize; /* Gadget internal units */
      /* Source always on! */
      Source[0].z_on = 1e30;
      Source[0].z_off = 0.;
      Source[0].SourceType = SourceMonochromatic;
      Source[0].nu = Constants.nu_0;
      Source[0].L_0 = 1.0895e+31; /* J/s / ray */
      Source[0].AttenuationGeometry = SphericalWave;
      break;

    case 2: /* Test 2 */
    case 5: /* Test 5 - Dynamical expansion of an HII region into a constant density medium.*/
      Source[0].r[0] = 0.5 * (rt_float)BoxSize; /* Gadget internal units */
      Source[0].r[1] = 0.5 * (rt_float)BoxSize; /* Gadget internal units */
      Source[0].r[2] = 0.5 * (rt_float)BoxSize; /* Gadget internal units */
      Source[0].SourceType = SourceBlackBody;
      /* Source always on! */
      Source[0].z_on = 1e30;
      Source[0].z_off = 0.;
      Source[0].nu_end = 0.0; /* Needs to be initialized */
      /* 5E48 photons/sec, 1E5 K black body */
      Source[0].T_BB = 1E5; /* K */
      /* Ndot = 5E48 photons/sec equivalent:
       * Ndot/A = (4pi * zeta(3) / c^2) * (k_B * T / h_p)^3 photons s^-1 m^-2
       * (Derive by integrating planck's law / (h_p * nu) over frequency and
       * solid angle.)
       */
      Source[0].SurfaceArea = (rt_float)(5E48 / Flux1E5BB); /* m^2 per ray */
      Source[0].AttenuationGeometry = SphericalWave;
      break;

    case 7:
    case 3: /* Test 3 */
      /* Plane wave, in the x-direction (here r is the direction) */
      Source[0].r[0] = 1.0;
      Source[0].r[1] = 0.0;
      Source[0].r[2] = 0.0;
      Source[0].SourceType = SourceBlackBody;
      /* Source always on! */
      Source[0].z_on = 1e30;
      Source[0].z_off = 0.;
      /* Need 1e6 s^-1 cm^-2 (1e10 s^-1 m^-2) ionizing photon flux at R_0  */
      /* RT expects R_0 to be in Mpc */
      Source[0].R_0 = (rt_float)(sqrt(5.0e48 / (4.0 * M_PI * 1.0e10)) / ((double)Constants.Mpc));
      /* 5E48 photons/sec, 1E5 K black body */
      Source[0].T_BB = 1E5; /* K */
      /* Ndot = 5E48 photons/sec equivalent:
       * Ndot/A = (4pi * zeta(3) / c^2) * (k_B * T / h_p)^3 photons s^-1 m^-2
       * (Derive by integrating planck's law / (h_p * nu) over frequency and
       * solid angle.)
       */
      Source[0].SurfaceArea = (rt_float)(5E48 / Flux1E5BB); /* m^2 per ray */
      /* Source[0].SurfaceArea = 4.677811703730327e+18; */  /* m^2 */
      Source[0].AttenuationGeometry = PlaneWave;
      break;

    case 4: /* Test 4 */
    {
      if (Parameters.nSources != 16) {
        std::cerr << "ERROR: " << __FILE__ << ": " << __LINE__ << " Test 4 has 16 sources. Parameters file states "
                  << Parameters.nSources << " sources." << std::endl;
      }

      /* Read source positions and unscaled luminosities from halo catalogue */
      setSourcePositions(Parameters, BoxSize, Source);

      for (size_t iSource = 0; iSource < Parameters.nSources; ++iSource) {
        Source[iSource].SourceType = SourceBlackBody;
        /* Source always on! */
        Source[iSource].z_on = 1e30;
        Source[iSource].z_off = 0.;
        Source[iSource].nu_end = 0.0; /* Needs to be initialized */
        if (Parameters.ProblemType == 5) {
          Source[iSource].T_BB = 3E4; /* SOFT SPECTRA, K */
        } else {
          Source[iSource].T_BB = 1E5; /* HARD SPECTRA, K */
        }
        /* Ndot = L[iSource] * 1E52 ionizing photons / sec */
        /* Source.L_0 will have source luminosities in units of 1E52 ionizing photons/sec. */
        Source[iSource].SurfaceArea = (rt_float)(((double)Source[iSource].L_0 * (double)1E52)); /* m^2 per ray */
        Source[iSource].AttenuationGeometry = SphericalWave;
      }
    } break;

      /* case 5: See case 2 */

    case 6: /* Test 6 - Dynamical expansion of an HII region into a flat-topped r^-2 density density field. */
      Source[0].r[0] = 0.5 * BoxSize; /* Gadget internal units */
      Source[0].r[1] = 0.5 * BoxSize; /* Gadget internal units */
      Source[0].r[2] = 0.5 * BoxSize; /* Gadget internal units */
      Source[0].SourceType = SourceBlackBody;
      /* Source always on! */
      Source[0].z_on = 1e30;
      Source[0].z_off = 0.;
      Source[0].nu_end = 0.0; /* Needs to be initialized */
      /* 5E50 photons/sec, 1E5 K black body */
      Source[0].T_BB = 1E5; /* K */
      /* Ndot = 5E50 ionizing photons/sec equivalent */
      Source[0].SurfaceArea = (rt_float)(1E50 / Flux1E5BB); /* m^2 per ray */
      Source[0].AttenuationGeometry = SphericalWave;
      break;

    /* Test 100 (Multiple Sources)*/
    case 100:
      for (size_t iSource = 0; iSource < Parameters.nSources; ++iSource) {
        Source[iSource].r[0] = 0.5 * (rt_float)BoxSize; /* Gadget internal units */
        Source[iSource].r[1] = 0.5 * (rt_float)BoxSize; /* Gadget internal units */
        Source[iSource].r[2] = 0.5 * (rt_float)BoxSize; /* Gadget internal units */
        Source[iSource].SourceType = SourceBlackBody;
        /* Source always on! */
        Source[iSource].z_on = 1e30;
        Source[iSource].z_off = 0.;
        Source[iSource].nu_end = 0.0; /* Needs to be initialized */
        /* 5E48 photons/sec, 1E5 K black body */
        Source[iSource].T_BB = 1E5; /* K */
        /* Ndot = 5E48 photons/sec equivalent:
         * Ndot/A = (4pi * zeta(3) / c^2) * (k_B * T / h_p)^3 photons s^-1 m^-2
         * (Derive by integrating planck's law / (h_p * nu) over frequency and
         * solid angle.)
         */
        Source[iSource].SurfaceArea = (rt_float)(5E48 / Flux1E5BB) / Parameters.nSources; /* m^2 per ray */
        Source[iSource].AttenuationGeometry = SphericalWave;
      }
      break;

    default:
      printf("%s: %i: ERROR: Unknown ProblemType %i\n", __FILE__, __LINE__, (int)Parameters.ProblemType);
  }
}

void setSourcePositions(ParametersT const Parameters, rt_float const BoxSize, RT_SourceT *const Source) {
  std::cout << "Reading halo catalogue " << Parameters.HaloCatalogueFile << "..." << std::endl;
  /* Read in the halo centres.
   * The file should be a list of halo centres, in order of most
   * massive/dense/whatever first, the first line being the number of centres
   * and the following lines the x y z sets normalized [0,1) */
  rt_float *haloCentres;
  rt_float *Ls;
  size_t const nHalos = readHaloCatalogue(Parameters.HaloCatalogueFile, haloCentres, Ls);
  if (nHalos == 0) {
    std::cerr << "ERROR: " << __FILE__ << ": " << __LINE__ << " Trouble reading Halo Catalogue." << std::endl;
  }
  if (nHalos < Parameters.nSources) {
    std::cerr << "ERROR: " << __FILE__ << ": " << __LINE__ << " Fewer halos (" << nHalos << ") than sources ("
              << Parameters.nSources << ")" << std::endl;
  }
  std::cout << "...done" << std::endl;

  /* Set up positions */
  for (size_t iSource = 0; iSource < Parameters.nSources; ++iSource) {
    Source[iSource].r[0] = haloCentres[3 * iSource + 0] * BoxSize; /* Gadget internal units */
    Source[iSource].r[1] = haloCentres[3 * iSource + 1] * BoxSize; /* Gadget internal units */
    Source[iSource].r[2] = haloCentres[3 * iSource + 2] * BoxSize; /* Gadget internal units */
    Source[iSource].L_0 = Ls[iSource];
  }
  free(haloCentres);
  free(Ls);
}
