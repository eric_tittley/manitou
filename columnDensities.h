#ifndef _COLUMNDENSITIES_H_
#define _COLUMNDENSITIES_H_

#include "BVH.h"
#include "RT_Data.h"
#include "Ray.h"

int columnDensities(const double NumberOfHydrogenAtomsPerParticle,
                    const double NumberOfHeliumAtomsPerParticle,
                    const double UnitLength,
                    BVH *tree,
                    const Rays *rays,
                    RT_Cell *Data_cell);

#endif
