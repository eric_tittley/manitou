#ifndef _PARAMETERS_H_
#define _PARAMETERS_H_

#include "RT_Precision.h"

struct ParametersT_struct {
  unsigned int ProblemType;
  char *GadgetFile;
  rt_float InitialTemperature; /* K */
  rt_float t_start;            /* s */
  rt_float t_stop;             /* s */
  int Nsph;                    /* Number of particles in an SPH search radius */
  size_t nSources;
  rt_float columnDensityThreshold_H1;
  rt_float columnDensityThreshold_He1;
  rt_float columnDensityThreshold_He2;
  unsigned int RecalculateColumnDensityIterationCount;
  rt_float totalSourceLuminosity;
  char *HaloCatalogueFile;
  double *InitialFractions;
  size_t NElements_InitialFractions;
  unsigned int WallTimeLimit;
  unsigned int RestartFlag;
  char *RestartCommand;
  size_t OutputIterationCount;
  char *OutputTimesFile;
};

typedef struct ParametersT_struct ParametersT;

enum ParametersEntries {
  iProblemType,
  iGadgetFile,
  iInitialTemperature,
  it_start,
  it_stop,
  iNsph,
  inSources,
  icolumnDensityThreshold_H1,
  icolumnDensityThreshold_He1,
  icolumnDensityThreshold_He2,
  iRecalculateColumnDensityIterationCount,
  itotalSourceLuminosity,
  iHaloCatalogueFile,
  iInitialFractions,
  iWallTimeLimit,
  iRestartFlag,
  iRestartCommand,
  iOutputIterationCount,
  iOutputTimesFile,
  nParameters
};

#endif
