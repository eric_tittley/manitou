#ifndef _MANITOU_H_
#define _MANITOU_H_

#include "RT_Precision.h"

extern "C" {
/* RT includes */
#include "Logic.h"
#include "RT.h"
#include "RT_Data.h"
#include "RT_RatesT.h"
#include "RT_TimeScale.h"
}
#include "BVH.h"
#include "Parameters.h"
#include "RT_ConstantsT.h"
#include "Ray.h"

int calculateSourceParticlePairRates(RT_Data const Data,
                                     ParametersT const Parameters,
                                     RT_ConstantsT const Constants,
                                     rt_float const z_source,
                                     RT_SourceT const Source,
                                     RT_IntegralInvolatilesT const Involatiles,
                                     /* Incremented */ RT_RatesBlockT const Rates,
                                     /*     Updated */ unsigned int *const RecalculateColumnDensityFlag);

int columnDensities(size_t const *const ParticlesToUpdateColumnDensities,
                    double const NumberOfHydrogenAtomsPerParticle,
                    double const NumberOfHeliumAtomsPerParticle,
                    rt_float const UnitLength,
                    BVH *tree,
                    Rays const *rays,
                    RT_Data const Data_cell);

int readParameters(char const *const ParametersFile, ParametersT *const Parameters);

size_t readHaloCatalogue(char *const HaloCatalogueFile, rt_float *&haloCentres, rt_float *&Ls);

void resetEntropyAndTemperature(rt_float const Temperature, RT_ConstantsT const Constants, RT_Data const Data);

void Manitou_SetSources(ParametersT &Parameters,
                        double const BoxSize,
                        RT_ConstantsT const Constants,
                        RT_SourceT *const Source);

int saveDeviceRatesToFile(RT_Data const &Data_host,
                          RT_RatesBlockT Rates_dev,
                          rt_float t,
                          rt_float outputTimesUnit,
                          char *Dir);

#endif
