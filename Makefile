# The following allows one to print a variable's value with:
#  make print-VARIABLE
# which prints to stdout
#  VARIABLE = the_value_of_the_variable
print-% : ; @echo $* = $($*)

ROOT_DIR := $(CURDIR)
MAKE_CONFIG := $(ROOT_DIR)/make.config

include $(MAKE_CONFIG)

INC_GADGET2          := -I$(ROOT_DIR)/Gadget2
INC_RT               := -I$(ROOT_DIR)/RT
INC_LIBMANIDOOWAG    := -I$(ROOT_DIR)/libManidoowag
ifeq ($(CUDA),0)
 INC_RTSPH           := -I$(ROOT_DIR)/rtsph/include
else
 INC_CUDA            := -I$(CUDA_DIR)/include
 INC_RT_CUDA         := -I$(ROOT_DIR)/RT_CUDA
 INC_RTSPH           := -I$(ROOT_DIR)/rtsph_cuda
 INC_LIBMANITOU_CUDA := -I$(ROOT_DIR)/libManitou_CUDA
 INC_LIBCUDASUPPORT  := -I$(ROOT_DIR)/libcudasupport
endif

INCS = -I$(ROOT_DIR) \
       $(INC_RT) \
       $(INC_RT_CUDA) \
       $(INC_RTSPH) \
       $(INC_LIBMANITOU_CUDA) \
       $(INC_LIBMANIDOOWAG) \
       $(INC_GSL) \
       $(INC_NCCL) \
       $(INC_CUDA) \
       $(INC_LIBCUDASUPPORT)

LOCAL_LIBS = libManitou/libManitou.a \
             RT/libRT.a \
             libPF/libPF.a

ifeq ($(CUDA),0)
 LOCAL_LIBS += rtsph/librtsph.a \
               libManidoowag/libManidoowag.a
else
 LOCAL_LIBS += rtsph_cuda/librtsph.a \
               libManitou_CUDA/libManitou_CUDA.a \
               libManidoowag/libManidoowag.a \
               RT_CUDA/libRT_CUDA.a \
               libcudasupport/libcudasupport.a
endif

LIBS = $(LOCAL_LIBS) $(LIB_ARCH) -lm $(LIB_GSL) $(LIB_NCCL)
ifeq ($(CUDA),1)
 LIBS += -lgomp
endif

ifeq ($(GIZMO_BINDINGS),1)
 INCS += $(INC_GIZMO)
endif

SUBDIRS = libManitou libManidoowag RT libPF
GADGET_SRC =

ifeq ($(GADGET_BINDINGS),1)
 INCS += $(INC_GADGET2) $(INC_MPI)
 ExtraPackages += Gadget2
 LOCAL_LIBS += libGadget2/libGadget2.a
 LIBS += -lmpi
 SUBDIRS +=  libGadget2
 GADGET_SRC = Gadget2
 DEFS += -DPMGRID
endif

SUBMODULES = RT libPF libManidoowag rtsph
ifeq ($(CUDA),1)
 SUBDIRS += libcudasupport rtsph_cuda RT_CUDA libManitou_CUDA
 SUBMODULES += libcudasupport rtsph_cuda RT_CUDA
endif

DEPS = dep.mak

# Targets for which no dependencies should be generated
NODEPS = clean distclean update status diff push revision

EXEC = Manitou
EXEC_SRC = Manitou.cu
EXEC_OBJ = $(EXEC_SRC:.cu=.o)

all: $(LOCAL_LIBS)

$(EXEC): $(SUBDIRS) $(GADGET_SRC) $(EXEC_OBJ) $(MANITOU_OBJS) $(LOCAL_LIBS) revision
	$(LD) -o $(EXEC) $(LD_FLAGS) $(EXEC_OBJ) $(MANITOU_OBJS) $(LIBS)
	echo "md5sum $(EXEC)" >> Revisions; md5sum $(EXEC) >> Revisions

install: $(EXEC)
	install $(EXEC) $(INSTALL_DIR)
	install Revisions $(INSTALL_DIR)/Revisions

uninstall:
	-rm $(INSTALL_DIR)/$(EXEC)
	-rm $(INSTALL_DIR)/Revisions

# Dependency tips:
# If you need a library to produce a CUDA link.o file, then depend on the
# library.
# If you need a directory to access a header file and that directory is not
# part of the repository, then depend on the directory unless the library in
# the directory is already a dependency from the first tip.

libManitou/libManitou.a: RT rtsph libPF
	$(MAKE) ROOT_DIR=${ROOT_DIR} INC_RT=$(INC_RT) INC_RTSPH=$(INC_RTSPH) \
	INC_LIBMANIDOOWAG=${INC_LIBMANIDOOWAG} -C libManitou

libManitou_CUDA/libManitou_CUDA.a: RT rtsph rtsph_cuda RT_CUDA/libRT_CUDA.a libManidoowag/libManidoowag.a
	$(MAKE) ROOT_DIR=${ROOT_DIR} INC_RT=$(INC_RT) INC_RT_CUDA=$(INC_RT_CUDA) \
			INC_RTSPH=$(INC_RTSPH) INC_CUDA=$(INC_CUDA) \
			INC_LIBCUDASUPPORT=$(INC_LIBCUDASUPPORT) \
			INC_LIBMANIDOOWAG=${INC_LIBMANIDOOWAG} \
			GIZMO_DIR=$(GIZMO_DIR) \
			-C libManitou_CUDA

libManidoowag/libManidoowag.a: libManidoowag RT RT_CUDA libcudasupport
	$(MAKE) ROOT_DIR=${ROOT_DIR} INC_RT=$(INC_RT) INC_RT_CUDA=$(INC_RT_CUDA) \
			INC_RTSPH=$(INC_RTSPH) INC_CUDA=$(INC_CUDA) \
			INC_LIBCUDASUPPORT=$(INC_LIBCUDASUPPORT) \
			-C libManidoowag

RT/libRT.a: RT
	$(MAKE) ROOT_DIR=${ROOT_DIR} MAKE_CONFIG=${MAKE_CONFIG} -C RT

RT_CUDA/libRT_CUDA.a: RT_CUDA RT
	$(MAKE) ROOT_DIR=${ROOT_DIR} MAKE_CONFIG=${MAKE_CONFIG} AR=$(AR) \
			INC_RT=$(INC_RT) INC_RT_CUDA=$(INC_RT_CUDA) \
			INC_LIBCUDASUPPORT=$(INC_LIBCUDASUPPORT) \
			-C RT_CUDA

rtsph/librtsph.a: rtsph
	$(MAKE) CXX=$(CXX) CXXFLAGS="$(CXXFLAGS)" LDFLAGS="$(LD_FLAGS)" AR=$(AR) -C rtsph lib

rtsph_cuda/librtsph.a: rtsph_cuda
	$(MAKE) CXX=$(CXX) CXXFLAGS="$(CXXFLAGS)" \
			NVCC="$(NVCC)" NVCCFLAGS="$(NVCCFLAGS)" \
			INC_LIBCUDASUPPORT=$(INC_LIBCUDASUPPORT) \
			-C rtsph_cuda lib

libGadget2/libGadget2.a: Gadget2
	$(MAKE) MAKE_CONFIG=${MAKE_CONFIG} -C libGadget2 lib

libPF/libPF.a: libPF
	$(MAKE) CC=$(CC) CFLAGS="$(CFLAGS)" AR=$(AR) -C libPF

libcudasupport/libcudasupport.a: libcudasupport
	$(MAKE) CC=$(CC) CFLAGS="$(CFLAGS)"  \
			CXX=$(CXX) CXXFLAGS="$(CXXFLAGS)" \
			NVCC="$(NVCC)" NVCCFLAGS="$(NVCCFLAGS)" NVCC_LIBS="$(NVCC_LIBS)" \
			ROOT_DIR=$(ROOT_DIR) INC_CUDA=$(INC_CUDA) \
			AR=$(AR) ARFLAGS="$(ARFLAGS)" \
			-C libcudasupport

rtsph/bin/testKernel:
	$(MAKE) CXX=$(CXX) CXXFLAGS="$(CXXFLAGS)" LDFLAGS="$(LD_FLAGS)" -C rtsph tests

$(SUBMODULES): % : %/.git
$(addsuffix /.%, $(SUBMODULES)):
	git submodule update --init $(SUBMODULES)

Gadget2:
	-rm Gadget2
	wget -q http://www.mpa-garching.mpg.de/gadget/gadget-2.0.7.tar.gz
	tar xzf gadget-2.0.7.tar.gz
	ln -s Gadget-2.0.7/Gadget2
	rm gadget-2.0.7.tar.gz

.PHONY: clean
clean:
	-rm -f *.o
	-rm -f *~
	-rm -f Make.log
	-rm -f splint.log
	for dir in $(SUBDIRS); do \
	 ( $(MAKE) -C $$dir clean ) \
	done

.PHONY: distclean
distclean: clean
	-rm -f $(DEPS)
	-rm -f $(EXEC)
	-rm -f Revisions
	for dir in $(SUBDIRS); do \
	 ( $(MAKE) -C $$dir distclean ) \
	done

.PHONY: rebuild
rebuild: distclean
	$(MAKE) all

$(DEPS): $(SUBDIRS) $(GADGET_SRC)
	#for dir in $(SUBDIRS); do \
	# ( $(MAKE) -C $$dir dep ) \
	#done
	-rm -f $(DEPS)
	for file in $(EXEC_SRC) ; do \
	 ( $(DEPEND) $(DEPEND_FLAGS) $(INCS) $(DEFS) $$file >> $(DEPS) ) \
	done

.PHONY: check
check:
	for dir in $(SUBDIRS); do \
	 ( $(MAKE) -C $$dir check ) \
	done
	splint +posixlib -nullderef -nullpass $(INCS) $(DEFS) $(EXEC_SRC) >> splint.log

.PHONY: update
update:
	rm -f $(DEPS)
	git pull
	git submodule sync
	git submodule update $(SUBMODULES)

.PHONY: status
status:
	rm -f $(DEPS)
	git status
	git submodule foreach "git status"

.PHONY: diff
diff:
	rm -f $(DEPS)
	git diff --stat
	git submodule foreach "git diff --stat"

.PHONY: push
push:
	rm -f $(DEPS)
	git push
	git submodule foreach "git push"

.PHONY: revision
revision:
	$(ROOT_DIR)/bin/gitReportBranchVersion.sh > Revisions
	for dir in . $(SUBMODULES); do \
	 (cd $$dir && \
	 $(ROOT_DIR)/bin/gitReportBranchVersion.csh >> $(ROOT_DIR)/Revisions && \
	 echo >> $(ROOT_DIR)/Revisions); \
	done
	echo "\n------------- config.h ------------------" >> Revisions
	cat config.h >> Revisions
	echo "-------------- end ----------------------" >> Revisions
	echo "\nCFLAGS:\n $(CFLAGS)" >> Revisions
	echo "\nCXXFLAGS:\n $(CXXFLAGS)" >> Revisions
	echo "\nNVCCFLAGS:\n $(NVCCFLAGS)" >> Revisions

.PHONY: submodules
submodules: $(SUBMODULES)

#Don't create dependencies when we're cleaning, for instance
ifeq (0, $(words $(findstring $(MAKECMDGOALS), $(NODEPS))))
    #Chances are, these files don't exist.  GMake will create them and
    #clean up automatically afterwards
    -include $(DEPS)
endif

.SUFFIXES: .o .cpp .cu

.cpp.o:
	$(CXX) $(CXXFLAGS) $(DEFS) $(INCS) -c $<

.cu.o:
	$(NVCC) $(NVCCFLAGS) $(DEFS) $(INCS) -dc $<
